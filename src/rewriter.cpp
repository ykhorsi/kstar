#include <llvm/Support/raw_ostream.h>

#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Rewrite/Frontend/Rewriters.h>
#include <clang/Rewrite/Frontend/FrontendActions.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Tooling.h>

#include <fstream>

#include "OpenMPRuntime.h"
#include "RuntimeVisitor.h"
#include "config.h"
#include "context.h"

#include "frontend/FrontendActions.h"

using namespace clang;

// Contains context information shared between FrontendActions
ActionContext Acontext;

std::string get_include(bool kaapi, bool starpu) {
  if (kaapi)
    return "kstar_kaapi.h";
  else if (starpu)
    return "kstar_starpu.h";
  assert(!"Unknow runtime!");
  return std::string();
}

static llvm::cl::OptionCategory KStarToolOption("KStar tool Option");

int main(int argc, const char **argv) {
  if (argc < 2) {
    llvm::errs() << "Usage: " << argv[0] << " <options> <filename>\n";
    return 1;
  }

  llvm::cl::opt<bool> optStarpu("starpu", llvm::cl::NotHidden,
                                llvm::cl::desc("Select starpu runtime"),
                                llvm::cl::ValueDisallowed);
  llvm::cl::opt<bool> optKaapi("kaapi", llvm::cl::NotHidden,
                               llvm::cl::desc("Select kaapi runtime"),
                               llvm::cl::ValueDisallowed);
  llvm::cl::opt<std::string> optOutput(
      "o", llvm::cl::NotHidden, llvm::cl::desc("Select output file"),
      llvm::cl::Optional, llvm::cl::init("a.out"));

  tooling::CommonOptionsParser op(argc, argv, KStarToolOption);
  tooling::ClangTool Tool(op.getCompilations(), op.getSourcePathList());

  // Initialize the selected runtime.
  if (optKaapi) {
    OpenMPRuntimeFactory::build(OpenMPRuntimeFactory::KAAPI);
  } else if (optStarpu) {
    OpenMPRuntimeFactory::build(OpenMPRuntimeFactory::STARPU);
  }

  // Initialize rewriter
  Rewriter rewrite;
  Acontext.rewriter_ = &rewrite;

  Acontext.IncludeFiles.insert(get_include(optKaapi, optStarpu));

  Tool.run(
    tooling::newFrontendActionFactory<kstar::IncludeExpanderAction>().get());
  // Change content for input file by preprocessed input so that Action can be
  // chained
  Tool.mapVirtualFile(op.getSourcePathList()[0].c_str(),
                      Acontext.resultStream2.str());

  Tool.run(
    tooling::newFrontendActionFactory<kstar::PrintPreprocessedAction>().get());

  // Change content for input file by preprocessed input so that Action can be
  // chained
  Tool.mapVirtualFile(op.getSourcePathList()[0].c_str(),
                      Acontext.resultStream.str());

  int result = Tool.run(
      tooling::newFrontendActionFactory<RewriterFrontendAction>().get());

  // Now output rewritten source code
  FileID mainFileId = rewrite.getSourceMgr().getMainFileID();
  RewriteBuffer const *RewriteBuf = rewrite.getRewriteBufferFor(mainFileId);
  if (RewriteBuf) {
    // Inject required header includes.
    for (std::string Name : Acontext.IncludeFiles) {
      std::string IncludeFile = "\n#include <" + Name + ">\n";
      rewrite.getEditBuffer(mainFileId).InsertText(0, IncludeFile);
    }

    std::error_code EC;
    llvm::raw_fd_ostream outFile(optOutput.c_str(), EC,
                                 llvm::sys::fs::F_None);
    outFile << std::string(RewriteBuf->begin(), RewriteBuf->end());
  } else {
    std::ifstream src(op.getSourcePathList()[0].c_str());
    std::ofstream dst(optOutput.c_str());
    dst << src.rdbuf();
  }

  // Free memory allocated by the runtime.
  delete OpenMPRuntimeFactory::getInstance();

  return result;
}
