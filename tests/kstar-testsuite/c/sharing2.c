#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    do  {
      randomvalue = rand()%10000;
    } while (randomvalue == 0);

    int a = randomvalue;
    #pragma omp parallel firstprivate(a)
    {
      assert(a == randomvalue);
      a = 12;
    }
    assert( a == randomvalue );
    return 0;
}
