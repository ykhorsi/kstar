#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    int a = 0;
    #pragma omp parallel
    #pragma omp for lastprivate(a)
      for (int i=0; i<1000; ++i)
      {
        a =  i;
      }
    assert( a == 999 );
    return 0;
}
