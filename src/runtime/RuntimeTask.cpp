#include <llvm/Support/raw_ostream.h>

#include "RuntimePragma.h"
#include "GenUtils.h"

using namespace clang;

RuntimeTask::RuntimeTask(OMPTaskDirective *t, ASTContext &c,
                         DataSharingInfo &dsa)
    : RuntimeScopeDirective(t, c, dsa) {
  newFunction_ = "wrapper_" + std::to_string(getId());

  for (auto &Dep : outDeps_) {
    const VarDecl *VD = Dep.first;
    const std::vector<Expr const *> Exprs = getDepends(VD);
    for (Expr const *E : Exprs) {
      if (isa<OMPArraySectionExpr>(E))
        by_ref.push_back(VD->getName());
    }
  }
  for (auto &Dep : inDeps_) {
    const VarDecl *VD = Dep.first;
    const std::vector<Expr const *> Exprs = getDepends(VD);
    for (Expr const *E : Exprs) {
      if (isa<OMPArraySectionExpr>(E))
        by_ref.push_back(VD->getName());
    }
  }
  for (auto &Dep : cwDeps_) {
    const VarDecl *VD = Dep.first;
    const std::vector<Expr const *> Exprs = getDepends(VD);
    for (Expr const *E : Exprs) {
      if (isa<OMPArraySectionExpr>(E))
        by_ref.push_back(VD->getName());
    }
  }
  for (auto &Dep : commuteDeps_) {
    const VarDecl *VD = Dep.first;
    const std::vector<Expr const *> Exprs = getDepends(VD);
    for (Expr const *E : Exprs) {
      if (isa<OMPArraySectionExpr>(E))
        by_ref.push_back(VD->getName());
    }
  }
}

const int RuntimeTask::getNumberOfDepends(VarDecl const *VD) {
  return inDeps_[VD].size() + outDeps_[VD].size() + cwDeps_[VD].size()
         + commuteDeps_[VD].size();
}

const std::vector<Expr const *> RuntimeTask::getDepends(VarDecl const *VD) {
  std::vector<Expr const *> Depends;
  for(Expr const * e: inDeps_[VD])
    Depends.push_back(e);
  for(Expr const * e: outDeps_[VD])
    Depends.push_back(e);
  for(Expr const * e: cwDeps_[VD])
    Depends.push_back(e);
  for(Expr const * e: commuteDeps_[VD])
    Depends.push_back(e);
  return Depends;
}

std::string RuntimeTask::freeFirstPrivateVLA() const {
  std::string str;
  llvm::raw_string_ostream rso(str);

  for (const VarDecl *VD : dsi.getCapturedVars(getOMPDirective(), true))
    if (dsi.isFirstPrivate(getOMPDirective(), VD)
        && VD->getType()->isVariableArrayType())
      rso << getRuntime()->EmitFree(VD->getNameAsString()) << ";";
  return rso.str();
}
