#Authors

Klang has been written by the members of the KSTAR project, namely :

* Olivier Aumage
* François Broquedis
* Pierrick Brunet
* Nathalie Furmento
* Thierry Gautier
* Samuel Pitoiset
* Samuel Thibault
* Philippe Virouleau

