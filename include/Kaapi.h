//===----------------------------------------------------------------------===//
//
// This provides a class for OpenMP code generation with KAAPI runtime.
//
//===----------------------------------------------------------------------===//

#ifndef KSTAR_KAAPI_H
#define KSTAR_KAAPI_H

#include "OpenMPRuntime.h"

class Kaapi : public OpenMPRuntime {
public:
  Kaapi() : OpenMPRuntime() {}

  RuntimeDirective *createDirective(clang::OMPExecutableDirective *S,
                                    clang::ASTContext &C,
                                    DataSharingInfo &DSI) override;

  std::string EmitBeginCritical(const std::string &Name,
                                bool IsCPlusPlus11) override;
  std::string EmitEndCritical(const std::string &Name,
                              bool IsCPlusPlus11) override;
  std::string EmitBarrier() override;
  std::string EmitTaskwait() override;
  std::string EmitSingleCond() override;
  std::string EmitBeginCopyprivate(const std::string &Data) override;
  std::string EmitEndCopyprivate(const std::string &Data) override;
  std::string EmitCopyprivateBarrier() override;
  std::string EmitMasterCond() override;
  std::string EmitBeginOrdered() override;
  std::string EmitEndOrdered() override;
  std::string EmitBeginTaskgroup() override;
  std::string EmitEndTaskgroup() override;
  std::string EmitFlush() override;
  std::string EmitScheduleType(clang::OpenMPScheduleClauseKind &K) override;
  std::string EmitDependType(clang::OpenMPDependClauseKind &K,
                             bool IsCPlusPlus) override;
  std::string EmitMapType(clang::OpenMPMapClauseKind &K) override;
  std::string EmitProcBind(clang::OpenMPProcBindClauseKind &K) override;
  std::string EmitAlloc(std::string const &Size) override;
  std::string EmitFree(std::string const &Name) override;
  std::string EmitForInit(const std::string &Begin, const std::string &End,
                          const std::string &Chunk, const std::string &Pargrain,
                          const std::string &Schedule,
                          const std::string &Ordered) override;
  std::string EmitForNext(const std::string &Len, const std::string &Chunk,
                          const std::string &Schedule,
                          const std::string &Ordered, const std::string &Begin,
                          const std::string &End) override;
  std::string EmitForEnd(const std::string &Nowait) override;
  std::string EmitParallelRegion(const std::string &Name,
                                 const std::string &NumThreads,
                                 clang::OpenMPProcBindClauseKind &,
                                 const std::string &Func,
                                 const std::string &Arg) override;

  clang::QualType getScheduleType(clang::ASTContext &C) override;

  /// \brief Emit a call to komp_self_team().
  std::string EmitSelfTeamCall() const;

  /// \brief Emit a call to komp_self_thread().
  std::string EmitSelfThreadCall() const;

  /// \brief Emit a call which declares a mutex.
  std::string EmitDeclMutex(const std::string &Name) const;

  /// \brief Emit a call which inits a mutex.
  std::string EmitInitMutex(const std::string &Name) const;

  /// \brief Emit a call which destroys a mutex.
  std::string EmitDestroyMutex(const std::string &Name) const;

  /// \brief Emit constructor which is used to init mutexes.
  std::string EmitConstructor(clang::ASTContext &Context,
                              const std::set<std::string> &MutexNames) const;

  /// \brief Emit destructor which is used to destroy mutexes.
  std::string EmitDestructor(clang::ASTContext &Context,
                             const std::set<std::string> &MutexNames) const;

  /***/
  bool isKaapi() override { return true; }
  std::string genTaskSignature(clang::ASTContext &Context,
                               std::string funcName,
                               std::string templateArgs) override;
  std::string genRegister(clang::Expr const *b, clang::VarDecl const *,
                          DataSharingInfo &, clang::OMPExecutableDirective *,
                          const int &bufInd, clang::ASTContext &,
                          bool alloc = false) override;
  clang::FunctionDecl *
    CreateWrapperFunction(clang::ASTContext &Context,
                          const std::string &Name) override;
};

#endif
