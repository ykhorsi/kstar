#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <omp.h>

#define N 4096

void gpu_scal_func(unsigned, int *, int);

static void
cpu_scal_func(unsigned n, int *vec, int factor)
{
  unsigned i;

  for (i = 0; i < n; i++)
    vec[i] *= factor;
}

static int
check_result(unsigned n, int *vec, int factor)
{
  unsigned i;

  for (i = 0; i < n; i++) {
    if (vec[i] != i * factor) {
      fprintf(stderr, "Failure at %d, expected (%d), observed (%d)\n",
              i, i * factor, vec[i]);
      return 1;
    }
  }
  return 0;
}

static int
test_cpu()
{
  const int factor = 2;
  int *vec = calloc(N, sizeof(*vec));
  unsigned i;
  int ret;

  for (i = 0; i < N; i++)
    vec[i] = i;

#pragma omp parallel
#pragma omp master
#pragma omp task depend(in: N, factor) depend(inout: vec[0:N]) where(cpu)
  {
    // omp_is_initial_device() should return 1 for CPU workers.
    assert(omp_is_initial_device());
    cpu_scal_func(N, vec, factor);
  }

  ret = check_result(N, vec, factor);

  free(vec);
  return ret;
}

static int
test_gpu()
{
  const int factor = 4;
  int *vec = calloc(N, sizeof(*vec));
  unsigned i;
  int ret;

  for (i = 0; i < N; i++)
    vec[i] = i;

#pragma omp parallel
#pragma omp master
#pragma omp task depend(in: N, factor) depend(inout: vec[0:N]) where(gpu)
  {
    // omp_is_initial_device() should return 0 for GPU workers.
    assert(!omp_is_initial_device());
    gpu_scal_func(N, vec, factor);
  }

  ret = check_result(N, vec, factor);

  free(vec);
  return ret;
}

static int
test_all()
{
  const int factor = 8;
  int *vec = calloc(N, sizeof(*vec));
  unsigned i;
  int ret;

  for (i = 0; i < N; i++)
    vec[i] = i;

#pragma omp parallel
#pragma omp master
#pragma omp task depend(in: N, factor) depend(inout: vec[0:N]) where(all)
  {
    if (omp_is_initial_device())
      cpu_scal_func(N, vec, factor);
    else
      gpu_scal_func(N, vec, factor);
  }

  ret = check_result(N, vec, factor);

  free(vec);
  return ret;
}

int
main()
{
  assert(!test_cpu());
  assert(!test_gpu());
  assert(!test_all());
  return 0;
}
