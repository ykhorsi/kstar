#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void foo(int *a)
{
    *a = 12;
}

void bar(int *a)
{
    sleep(1);
    *a = 14;
}

void foobar(int *a)
{
    sleep(1);
    *a = 14 + *a;
}

int main()
{
    int a = 8;
#pragma omp parallel
#pragma omp single
    {
#pragma omp task depend(out: a) shared(a)
        foo(&a);
#pragma omp task depend(out: a) shared(a)
        bar(&a);
#pragma omp task depend(in: a) shared(a)
        if (a != 14)
            abort();
    }

#pragma omp parallel
#pragma omp single
    {
#pragma omp task depend(out: a) shared(a)
        foo(&a);
#pragma omp task depend(inout: a) shared(a)
        foobar(&a);
#pragma omp task depend(in: a) shared(a)
        if (a != 26)
            abort();
    }

    return 0;
}
