/*
** xkaapi
**
**
** Copyright 2009,2010,2011,2012,2013,2014 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** olivier.aumage@inria.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
**
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
**
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
**
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
**
*/
#ifndef OMP_H
#define OMP_H 1

/*
 * Define the lock data types.
 */
typedef void *omp_lock_t;
typedef void *omp_nest_lock_t;

/*
 * Define the lock hints.
 */
typedef enum omp_lock_hint_t {
  omp_lock_hint_none = 0,
  omp_lock_hint_uncontended = 1,
  omp_lock_hint_contended = 2,
  omp_lock_hint_nonspeculative = 4,
  omp_lock_hint_speculative = 8
  /* Add vendor specific constants for lock hints here, starting from the
   * most-significant bit. */
} omp_lock_hint_t;

/*
 * Define the schedule kinds.
 */
typedef enum omp_sched_t {
  omp_sched_static = 1,
  omp_sched_dynamic = 2,
  omp_sched_guided = 3,
  omp_sched_auto = 4,
  /* Add vendor specific schedule constants here. */
  omp_sched_adaptive = 5,
  omp_sched_steal = 6
} omp_sched_t;

/*
 * Define the proc bind values.
 */
typedef enum omp_proc_bind_t {
  omp_proc_bind_false = 0,
  omp_proc_bind_true = 1,
  omp_proc_bind_master = 2,
  omp_proc_bind_close = 3,
  omp_proc_bind_spread = 4
} omp_proc_bind_t;

#ifdef __cplusplus
extern "C" {
#define __KSTAR_NOTHROW throw()
#else
#define __KSTAR_NOTHROW __attribute__((__nothrow__))
#endif

/*
 * Execution environment routines.
 */
extern void omp_set_num_threads(int) __KSTAR_NOTHROW;
extern int omp_get_num_threads(void) __KSTAR_NOTHROW;
extern int omp_get_max_threads(void) __KSTAR_NOTHROW;
extern int omp_get_thread_num(void) __KSTAR_NOTHROW;
extern int omp_get_num_procs(void) __KSTAR_NOTHROW;
extern int omp_in_parallel(void) __KSTAR_NOTHROW;
extern void omp_set_dynamic(int) __KSTAR_NOTHROW;
extern int omp_get_dynamic(void) __KSTAR_NOTHROW;
extern int omp_get_cancellation(void) __KSTAR_NOTHROW;
extern void omp_set_nested(int) __KSTAR_NOTHROW;
extern int omp_get_nested(void) __KSTAR_NOTHROW;
extern void omp_set_schedule(omp_sched_t, int) __KSTAR_NOTHROW;
extern void omp_get_schedule(omp_sched_t *, int *) __KSTAR_NOTHROW;
extern int omp_get_thread_limit(void) __KSTAR_NOTHROW;
extern void omp_set_max_active_levels(int) __KSTAR_NOTHROW;
extern int omp_get_max_active_levels(void) __KSTAR_NOTHROW;
extern int omp_get_level(void) __KSTAR_NOTHROW;
extern int omp_get_ancestor_thread_num(int) __KSTAR_NOTHROW;
extern int omp_get_team_size(int) __KSTAR_NOTHROW;
extern int omp_get_active_level(void) __KSTAR_NOTHROW;
extern int omp_in_final(void);
extern omp_proc_bind_t omp_get_proc_bind(void) __KSTAR_NOTHROW;
extern int omp_get_num_places(void) __KSTAR_NOTHROW;
extern int omp_get_place_num_procs(int place_num) __KSTAR_NOTHROW;
extern void omp_get_place_proc_ids(int place_num, int *ids) __KSTAR_NOTHROW;
extern int omp_get_place_num(void) __KSTAR_NOTHROW;
extern int omp_get_partition_num_places(void) __KSTAR_NOTHROW;
extern void omp_get_partition_place_nums(int *place_nums) __KSTAR_NOTHROW;
extern void omp_set_default_device(int device_num) __KSTAR_NOTHROW;
extern int omp_get_default_device(void) __KSTAR_NOTHROW;
extern int omp_get_num_devices(void) __KSTAR_NOTHROW;
extern int omp_get_num_teams(void) __KSTAR_NOTHROW;
extern int omp_get_team_num(void) __KSTAR_NOTHROW;
extern int omp_is_initial_device(void) __KSTAR_NOTHROW;
extern int omp_get_initial_device(void) __KSTAR_NOTHROW;
extern int omp_get_max_task_priority(void) __KSTAR_NOTHROW;

/*
 * Lock routines.
 */
extern void omp_init_lock(omp_lock_t *) __KSTAR_NOTHROW;
extern void omp_destroy_lock(omp_lock_t *) __KSTAR_NOTHROW;
extern void omp_set_lock(omp_lock_t *) __KSTAR_NOTHROW;
extern void omp_unset_lock(omp_lock_t *) __KSTAR_NOTHROW;
extern int omp_test_lock(omp_lock_t *) __KSTAR_NOTHROW;
extern void omp_init_nest_lock(omp_nest_lock_t *) __KSTAR_NOTHROW;
extern void omp_destroy_nest_lock(omp_nest_lock_t *) __KSTAR_NOTHROW;
extern void omp_set_nest_lock(omp_nest_lock_t *) __KSTAR_NOTHROW;
extern void omp_unset_nest_lock(omp_nest_lock_t *) __KSTAR_NOTHROW;
extern int omp_test_nest_lock(omp_nest_lock_t *) __KSTAR_NOTHROW;

/*
 * Timing routines.
 */
extern double omp_get_wtime(void) __KSTAR_NOTHROW;
extern double omp_get_wtick(void) __KSTAR_NOTHROW;

#ifdef __cplusplus
}
#endif

#endif
