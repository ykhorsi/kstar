#include <llvm/Support/raw_ostream.h>

#include "GenUtils.h"
#include "KaapiPragma.h"

using namespace clang;

KaapiFor::KaapiFor(OMPForDirective *S, ASTContext &C,
                   DataSharingInfo &DSI)
    : RuntimeFor(S, C, DSI) {}

std::string KaapiFor::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // XXX: Wrapper functions generated for Kaapi only use one parameter...
  rso << RuntimeLoop::genReplacePragma(q);
  rso << wrapperFunction_ << addInjectedTemplateArgs()
      << "((void *)&params);";

  return rso.str();
}
