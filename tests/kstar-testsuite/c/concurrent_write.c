#include <stdio.h>
#include <assert.h>
#include <unistd.h>

int main()
{
    int val[4] = {5,5,5,5};
#pragma omp parallel
#pragma omp master
    {
        for (int i = 0; i < 4; i++) {
#pragma omp task firstprivate(i) shared(val) depend(cw:val)
            {
                sleep(2);
                val[i] = i;
            }
        }
    }

    for (int i = 0; i < 4; i++)
        assert(val[i] == i);
    return 0;
}
