#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    do  {
        randomvalue = rand()%10000;
    } while (randomvalue == 0);
    int a = randomvalue;
    int b = 4;
#pragma omp parallel
#pragma omp single
    {
        for (int i = 0; i < b; i++) {
#pragma omp task private(a) firstprivate(b)
            {
                assert( a != randomvalue);
                assert( b == 4);
                b++;
            }
        }
    }
    assert( a == randomvalue );
    return 0;
}
