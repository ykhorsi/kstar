#ifndef KSTAR_KAAPI_H
#define KSTAR_KAAPI_H

#include <libkomp.h>
#ifdef __cplusplus
#  include <new>
extern "C" {
#endif
extern int __kstar_extern_link_forced(void);
static int (*__force_link)() = &__kstar_extern_link_forced;
#ifdef __cplusplus
}
#endif

#endif /* __KSTAR_KAAPI_H */
