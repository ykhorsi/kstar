import numpy as np
import sys

p = ['dget', 'dgeq', 'dpot']
i = 0
values = list()
threads = list()
for f_ in sys.argv[1:]:
    #print "open : ", f_
    with file(f_) as f:
        value = dict()
        for line in f:
            if line.strip():
                if 'M' in line:
                    value['size'] = line.strip().split()[-1]
                elif "NB" in line:
                    value['subsize'] = line.strip().split()[-1]
                elif "Gflops" in line:
                    value['speed'] = line.strip().split()[-1]
                elif "Threads" in line:
                    value['threads'] = line.strip().split()[-1]
                    if value['threads'] not in threads:
                        threads.append(value['threads'])
                elif "Program" in line:
                    value['bin'] = line.strip().split()[-1]
                    #print value['bin']
                elif "Time" in line:
                    if "bin" not in value:
                        value['bin'] = f_ + "-" + p[i%3]
                        print value['bin']
                        i += 1
                    values.append(value)
                    #print value
                    value = dict()
            elif value:
                values.append(value)
                value = dict()

import glob
import os.path
aa = glob.glob(os.path.join("iwomp_hpac", "gcc-quark-*-*"))
aa = [a + '-dget' for a in aa] + [a + '-dgeq' for a in aa] + [a + '-dpot' for a in aa]

all_prog = ["time_dgeqrf", "time_dpotrf"]
labels = ["DGEQRF", "DPOTRF"]
prog_label = dict(zip(all_prog, labels))
all_kind = ["gcc-", "clang-omp-", "gcc-plasma-", "gcc-quarkdyn-"]
#all_prog = ["/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dpotrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dpotrf-task"] + aa

#print values
#print ['name', "threads", "size", "bloc"] + all_prog
#print threads
header = "Label Threads "
for version in all_kind:
    header += " " + version
header += "\n"
#print header
for TH in threads:
    for M in ["2048", "4096", "8192"]:
        for B in ["128", "224", "256"]:
            #print "\t".join(("-".join((M, B)), "48", M, B)),
            writefile = False
            dataprog = ""
            for prog_name,label in prog_label.items():
                dataprog += label
                #print "#" + prog_name + " (gcc-omp, clang-omp, plasma, quark)"
                for version in all_kind:
                    P = version + prog_name

                    speeds = np.array([float(value['speed']) for value in values if P in value["bin"] and value["size"] == M and value["subsize"] == B and value["threads"] == TH])
                    if len(speeds):
                        writefile = True
                        prog_mean = np.mean(speeds)
                        #print P + " (" + M + "-" + B + ") : " + str(prog_mean)
                        #print TH + " " + M + " " + B + " " + str(prog_mean)
                        dataprog += " " + str(prog_mean)
                dataprog += "\n"
            if writefile:
                with open("gnuplot-%s-%s.dat"%(M,B), "w") as monfichier:
                    #monfichier.write(header)
                    monfichier.write(dataprog)
                    #for n, f in [("mea", np.mean), ("med", np.median), ("std", np.std), ("max", np.max), ("min", np.min)]:
                        #if len(vv):
                            #v = f(vv)
                            #if not np.isnan(v):
                                #print "\t" + str(v),
print ""
