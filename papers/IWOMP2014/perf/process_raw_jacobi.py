import numpy as np
import os.path
import sys

p = ['dget', 'dgeq', 'dpot']
values = list()
threads = list()
for f_ in sys.argv[1:]:
    #print "open : ", f_
    with file(f_) as f:
        value = dict()
        for line in f:
            if line.strip():
                if "Threads" in line:
                    value['threads'] = line.strip().split()[-1]
                    if value['threads'] not in threads:
                        threads.append(value['threads'])
                elif "Program" in line:
                    value['bin'] = line.strip().split()[-1]
                elif "Time" in line and "rhs" not in line and "init" not in line:
                    value['speed'] = line.strip().split()[-2]
                    #value['size'] = os.path.basename(f_).split('-')[-2]
                    values.append(value)
                    #print value
                    value = dict()
                elif "Size" in line:
                    value['size'] = line.strip().split()[-1]
                elif "Blocksize" in line:
                    value['blocsize'] = line.strip().split()[-1]
            elif value:
                values.append(value)
                value = dict()

all_kind = ["gcc-seq", "clang-seq", "gcc-block-task", "clang-block-task", "gcc-block-task-dep", "clang-block-task-dep", "gcc-for", "clang-for"]
#all_prog = ["/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dpotrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dpotrf-task"] + aa

#print values
#print ['name', "threads", "size", "bloc"] + all_prog
#print threads
header = "Blocsize Threads "
for version in all_kind:
    header += " " + version
header += "\n"
#print header
writefile = False
dataprog = ""
for M in ["8192", "16384"]:
    for B in ["1024"]:
        dataseq = ""
        for TH in ["1", "32", "48"]:
            #print "\t".join(("-".join((M, B)), "48", M, B)),
            datathread = M  + " " + TH
            writethread = False
            if len(dataseq) > 0:
                datathread += dataseq
            #print "#" + prog_name + " (gcc-omp, clang-omp, plasma, quark)"
            for version in all_kind:
                speeds = np.array([float(value['speed']) for value in values if value["bin"].endswith(version) and M == value["size"] and B == value["blocsize"] and TH == value["threads"]])
                if TH == "1" and len(speeds):
                    dataseq += " " + str(np.mean(speeds))
                elif len(speeds):
                    writethread = True
                    prog_mean = np.mean(speeds)
                    datathread += " " + str(prog_mean)
            #if TH == "1" and len(dataseq) > 0:
                ##dirty hack to have point at [1,1]
                #datathread += dataseq + dataseq
            datathread += "\n"
            if writethread:
                dataprog += datathread
                writefile = True
if writefile:
    with open("gnuplot-jacobi.dat", "w") as monfichier:
        #monfichier.write(header)
        monfichier.write(dataprog)
