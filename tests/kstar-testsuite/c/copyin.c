#include <omp.h>
#include <stdlib.h>
#include <stdio.h>

int thr[2] = { 32, 64 };
#pragma omp threadprivate (thr)

int
main (void)
{
  int l = 0;

  omp_set_dynamic (0);
  omp_set_num_threads (6);

#pragma omp parallel copyin (thr) reduction (||:l)
  {
    l = thr[0] != 32 || thr[1] != 64;
    thr[0] = omp_get_thread_num () + 11;
  }

  if (l || thr[0] != 11)
    abort ();

#pragma omp parallel reduction (||:l)
{
  l = thr[0] != omp_get_thread_num () + 11;
}

  if (l)
    abort ();
  return 0;
}
