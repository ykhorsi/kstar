#ifndef KSTAR_CODEGEN_STRUCTEXPAND_H
#define KSTAR_CODEGEN_STRUCTEXPAND_H

#include <tuple>
#include <vector>
#include <algorithm>

#include <llvm/Support/raw_ostream.h>

namespace CodeGen {

class StructExpand {

public:
  StructExpand() = default;

  StructExpand(const std::string &structName, const std::string &structType,
               bool is_cpp, const std::string &templateArgs = "");

  // Add a new variable to expand later
  void add(const std::string &name, const std::string &type,
           const std::string &paramType, const std::string &default_ = "",
           bool private_ = false, bool copy = false, bool copyin = false,
           bool ref = false);

  // Add a new record declaration.
  void addRecordDecl(const std::string &record);

  // Add a new array firstprivate variable to expand as they are very specific.
  void addArray(const std::string &name, const std::string &type,
                const std::string &len, const std::string &paramType,
                bool is_struct, bool copy, bool isvla, bool copyin);

  // Remove all variable which are no in to_handle.
  void except(std::vector<std::string> const &to_handle);

  // Free stack allocated ressources.
  std::string free();

  // Generate expansion source
  std::string str();

private:
  std::string structName_;
  std::string structType_;
  // variable, type, default, private?, copy?, paramType, copyIn?, ref?
  std::vector<std::tuple<std::string, std::string, std::string, bool, bool,
                         std::string, bool, bool>> Variables_;
  // variable, type, len, paramType, is_struct?, copy?, isvla
  std::vector<std::tuple<std::string, std::string, std::string, std::string,
                         bool, bool, bool, bool>> ArrayVariables_;
  std::vector<std::string> Records_;
  bool IsCXX;
  std::string templateArgs_;
};
}

#endif
