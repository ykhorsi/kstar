#include <iostream>
#include <cstdlib>

class MyClass {
    public:
        MyClass() : val_(0) {
            val_++;
        }
        void setVal(int value) {
            val_ = value;
        }
        int getVal() {
            return val_;
        }
    private:
        int val_;
};

void alter(MyClass &mc, int newVal)
{
    mc.setVal(newVal);
}

int main( int argc, char** argv)
{
    MyClass maVar;
#pragma omp parallel
#pragma omp single
    {
#pragma omp task shared(maVar) depend(in:maVar)
        if (maVar.getVal() != 1)
            abort();
#pragma omp task shared(maVar) depend(out:maVar)
        alter(maVar, 5);
#pragma omp task shared(maVar) depend(in:maVar)
        if (maVar.getVal() != 5)
            abort();
#pragma omp task shared(maVar) depend(out:maVar)
        alter(maVar, 6);
#pragma omp task shared(maVar) depend(out:maVar)
        alter(maVar, 7);
#pragma omp task shared(maVar) depend(in:maVar)
        if (maVar.getVal() != 7)
            abort();
#pragma omp taskwait
    }
    return 0;
}
