import numpy as np
import os.path
import sys

p = ['dget', 'dgeq', 'dpot']
values = list()
threads = list()
for f_ in sys.argv[1:]:
    #print "open : ", f_
    with file(f_) as f:
        value = dict()
        for line in f:
            if line.strip():
                if "Threads" in line:
                    value['threads'] = line.strip().split()[-1]
                    if value['threads'] not in threads:
                        threads.append(value['threads'])
                elif "Program" in line:
                    value['bin'] = line.strip().split()[-1]
                    #print value['bin']
                elif "bin/" in line:
                    value['bin'] = line.strip()
                elif "Time" in line:
                    value['speed'] = line.strip().split()[-2]
                    value['size'] = os.path.basename(f_).split('-')[-2]
                    values.append(value)
                    #print value
                    value = dict()
            elif value:
                values.append(value)
                value = dict()
#sys.exit(0)
all_kind = ["gcc-task", "clang-task", "gcc-task-dep", "clang-task-dep", "gcc-seq", "clang-seq"]
#all_prog = ["/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/gcc-time_dpotrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgetrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dgeqrf-task", "/scratch/pvirouleau/kstar/benchmarks/KASTORS/plasma/bin/clang-omp-time_dpotrf-task"] + aa

#print values
#print ['name', "threads", "size", "bloc"] + all_prog
#print threads
header = "Label Threads "
for version in all_kind:
    header += " " + version
header += "\n"
#print header
dataprog = ""
for M in ["64x128", "128x64"]:
    #for B in ["128"]:
    #print "\t".join(("-".join((M, B)), "48", M, B)),
    writefile = False
    #Experiment were logged using SUBMATRIXxMATRIX, the standard is to present it
    #the other way, so we invert before displaying
    if M == "128x64":
        dataprog += "64x128"
    else:
        dataprog += "128x64"
    dataprog += " 48"
    #print "#" + prog_name + " (gcc-omp, clang-omp, plasma, quark)"
    for version in all_kind:
        speeds = np.array([float(value['speed']) for value in values if value["bin"].endswith(version) and value["size"] == M])
        if len(speeds):
            writefile = True
            prog_mean = np.mean(speeds)
            dataprog += " " + str(prog_mean)
    dataprog += "\n"
if writefile:
    with open("gnuplot-sparselu.dat", "w") as monfichier:
        #monfichier.write(header)
        monfichier.write(dataprog)
