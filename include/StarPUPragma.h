#ifndef KSTAR_STARPUPRAGMA_H
#define KSTAR_STARPUPRAGMA_H

#include <clang/AST/ASTContext.h>
#include <clang/AST/StmtOpenMP.h>
#include <clang/Basic/SourceManager.h>

#include <map>
#include <string>

#include "RuntimePragma.h"

#define STARPU_DIRECTIVE(Name)                                                 \
  class StarPU##Name : public Runtime##Name {                                  \
  public:                                                                      \
    StarPU##Name(clang::OMP##Name##Directive *OMPD, clang::ASTContext &C,      \
                 DataSharingInfo &DSI)                                         \
        : Runtime##Name(OMPD, C, DSI) {}                                       \
  }

class StarPUTask : public RuntimeTask {
public:
  StarPUTask(clang::OMPTaskDirective *t, clang::ASTContext &c,
             DataSharingInfo &dsi);
  std::string genBeforeFunction(std::queue<std::string> &q) override;
  std::string genAfterFunction(std::queue<std::string> &q) override;
  std::string genReplacePragma(std::queue<std::string> &q) override;
};

class StarPUTarget : public RuntimeTarget {
public:
  StarPUTarget(clang::OMPTargetDirective *t, clang::ASTContext &c,
               DataSharingInfo &dsi);
  std::string genReplacePragma(std::queue<std::string> &) override;
  std::string genBeforeFunction(std::queue<std::string> &) override;
  std::string genAfterFunction(std::queue<std::string> &) override;

private:
  std::string genExpandDMA(std::unordered_map<clang::VarDecl const *,
                           std::set<clang::Expr const *>> &,
                           clang::VarDecl const *, size_t &);
  std::string genFuncWrapper(std::queue<std::string> &,
                             std::string prefix = "");
};

STARPU_DIRECTIVE(Barrier);
STARPU_DIRECTIVE(Critical);
STARPU_DIRECTIVE(For);
STARPU_DIRECTIVE(Sections);
STARPU_DIRECTIVE(Master);
STARPU_DIRECTIVE(Ordered);
STARPU_DIRECTIVE(Taskwait);
STARPU_DIRECTIVE(Flush);
STARPU_DIRECTIVE(Taskgroup);
STARPU_DIRECTIVE(Single);
STARPU_DIRECTIVE(Taskyield);
STARPU_DIRECTIVE(Parallel);
STARPU_DIRECTIVE(ParallelFor);
STARPU_DIRECTIVE(ParallelSections);

#endif
