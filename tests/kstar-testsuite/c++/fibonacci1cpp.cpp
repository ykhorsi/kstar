#include <iostream>
#include <cstdlib>

static void fibonacci(long *result,  const long n)
{
    if (n < 2) {
        *result = n;
    } else  {
        long r1,r2;
#pragma omp task shared(r1)
        fibonacci(&r1, n - 1);
/*#pragma omp task shared(r2)*/
        fibonacci(&r2, n - 2);
#pragma omp taskwait
        *result = r1 + r2;
    }
}

int main( int argc, char** argv)
{
    long result;
    int m = 10;

#pragma omp parallel 
#pragma omp single
#pragma omp task shared(result)
        fibonacci(&result, m);
#pragma omp taskwait

    if (result != 55)
        abort();

    return 0;
}
