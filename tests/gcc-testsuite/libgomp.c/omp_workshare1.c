/******************************************************************************
* FILE: omp_workshare1.c
* DESCRIPTION:
*   OpenMP Example - Loop Work-sharing - C/C++ Version
*   In this example, the iterations of a loop are scheduled dynamically
*   across the team of threads.  A thread will perform CHUNK iterations
*   at a time before being scheduled for the next CHUNK of work.
* AUTHOR: Blaise Barney  5/99
* LAST REVISED: 04/06/05
******************************************************************************/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define CHUNKSIZE   10
#define N       100

int main (int argc, char *argv[]) {

int i;
float a[N], b[N], c[N];

/* Some initializations */
for (i=0; i < N; i++)
  a[i] = b[i] = i * 1.0;

#pragma omp parallel shared(a,b,c) private(i)
  {
  #pragma omp for schedule(dynamic)
  for (i=0; i<N; i++)
    {
    c[i] = a[i] + b[i];
    }
  }  /* end of parallel section */

  for (i=0; i<N; i++)
    {
        assert(c[i] == 2 * i);
    }

  return 0;
}
