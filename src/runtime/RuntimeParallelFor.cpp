#include "RuntimePragma.h"

using namespace clang;

RuntimeParallelFor::RuntimeParallelFor(OMPParallelForDirective *S,
                                       ASTContext &C, DataSharingInfo &DSI)
    : RuntimeLoop(S, C, DSI) {}

std::string RuntimeParallelFor::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << RuntimeLoop::genReplacePragma(q);

  std::string Name  = "_kstar_parallel_region_" + std::to_string(getId());
  std::string NumThreads = getNumThreads(numThreads_, ifClause_);
  std::string Func = wrapperFunction_ + addInjectedTemplateArgs();
  std::string Arg = "&params";

  rso << getRuntime()->EmitParallelRegion(Name, NumThreads,
                                          procBind_, Func, Arg);

  return rso.str();
}
