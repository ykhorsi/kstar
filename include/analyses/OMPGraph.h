#ifndef KSTAR_ANALYSES_OMPGRAPH_H
#define KSTAR_ANALYSES_OMPGRAPH_H

#include <clang/AST/RecursiveASTVisitor.h>

#include <unordered_set>
#include <unordered_map>
#include <stack>
#include <vector>

class OMPGraph : public clang::RecursiveASTVisitor<OMPGraph> {
public:
  OMPGraph(clang::Decl const *s);

#define WRAP(PRAGMA)                                                           \
  bool VisitOMP##PRAGMA##Directive(clang::OMP##PRAGMA##Directive *S) {         \
    ProcessOMPExecutableDirective(S);                                          \
    return true;                                                               \
  }
#include "pragma.def"

  clang::OMPExecutableDirective *
    getFirstNode(clang::OMPExecutableDirective *OMPD) {
    return result[OMPD].first;
  }

private:
  std::stack<clang::OMPExecutableDirective *> parents_;
  std::unordered_set<clang::Stmt *> visited_;
  std::unordered_map<clang::OMPExecutableDirective *,
                     std::pair<clang::OMPExecutableDirective *,
                               std::vector<clang::OMPExecutableDirective *>>>
      result;

  void ProcessOMPExecutableDirective(clang::OMPExecutableDirective *S);
};

#endif
