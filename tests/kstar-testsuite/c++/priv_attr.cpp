#include <iostream>
#include <stdlib.h>
#include <omp.h>

class Average {
private:
    const int nb_threads;
    int priv_n, priv_sum;

public:
    Average(int n) : nb_threads(omp_get_max_threads()), priv_n(n) {
        priv_sum = 0;
    }

    double compute(int array[])
    {
        int & sum = this->priv_sum;

#pragma omp parallel for num_threads(nb_threads)
        for (int i = 0; i < priv_n; ++i) {
#pragma omp critical
            sum += array[i];
        }
        return double(sum) / priv_n;
    }

    const int get_priv_sum() const {
        return priv_sum;
    }
};

int main(int argc, char **argv)
{
    int array[5] = {100, 200, 400, 500, 1000};
    Average avg(sizeof(array) / sizeof(array[0]));

    if (avg.compute(array) != 440)
        abort();
    if (avg.get_priv_sum() != 2200)
        abort();
    return 0;
}
