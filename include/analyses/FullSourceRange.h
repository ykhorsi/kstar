#ifndef KSTAR_ANALYSES_FULLSOURCERANGE_H
#define KSTAR_ANALYSES_FULLSOURCERANGE_H

#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Basic/SourceLocation.h>
#include <clang/Rewrite/Core/Rewriter.h>

class FullSourceRange : public clang::RecursiveASTVisitor<FullSourceRange> {
public:
  FullSourceRange(clang::Stmt const *S, clang::Rewriter *R);
  FullSourceRange(clang::Decl const *D, clang::Rewriter *R);
  bool VisitStmt(clang::Stmt *S);
  bool VisitDecl(clang::Decl *D);
  clang::SourceRange result;

private:
  /* Private functions */
  void setBeginLocation(const clang::SourceLocation &LHS);
  void setEndLocation(const clang::SourceLocation &RHS);
  void findBeginOfOMPDirective(clang::SourceRange &SR);
  void findEndOfOMPThreadPrivateDecl(clang::SourceRange &SR);

  /* Private attributes */
  clang::Rewriter *rewriter_;
};

#endif
