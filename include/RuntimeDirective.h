#ifndef KSTAR_RUNTIMEDIRECTIVE_H
#define KSTAR_RUNTIMEDIRECTIVE_H

#include <clang/AST/StmtOpenMP.h>
#include <clang/AST/ASTContext.h>

#include <string>
#include <queue>

#include "OpenMPRuntimeFactory.h"

class RuntimeDirective {
public:
  RuntimeDirective(clang::OMPExecutableDirective *S, clang::ASTContext &C);
  virtual ~RuntimeDirective() {}

  virtual std::string genReplacePragma(std::queue<std::string> &) = 0;

  virtual std::string genBeforeFunction(std::queue<std::string> &) {
    return "";
  }

  virtual std::string genAfterFunction(std::queue<std::string> &) { return ""; }

  /// \brief Return the underlying OpenMP executable directive.
  clang::OMPExecutableDirective *getOMPDirective() const {
    return OMPDirective;
  }

  int getId() const { return pragmaId_; }
  static int numDirective;

  /// \brief Return the current AST context.
  clang::ASTContext &getContext() const {
    return Context;
  }

  /// \brief Return an instance to the current OpenMP runtime.
  OpenMPRuntime *getRuntime() const {
    return OpenMPRuntimeFactory::getInstance();
  }

protected:
  /// \brief The underlying OpenMP executable directive.
  clang::OMPExecutableDirective *OMPDirective;

  clang::ASTContext &Context;
  const int pragmaId_;
};

#endif
