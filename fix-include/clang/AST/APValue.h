/* avoid numerous strict-aliasing in clang/AST/APValue.h
 * Perhaps, we are only hiding a bug but it is not ours
 * and we can expect that llvm guys will fix it if it is real
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#include_next "clang/AST/APValue.h"
#pragma GCC diagnostic pop
