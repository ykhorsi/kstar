#include <stdio.h>
#include <stdlib.h>

static int compare_double(const double f1, const double f2)
{
    double precision = 0.00001;
    return ((f1 - precision < f2) && (f1 + precision) > f2);
}

int main()
{
    float a[100];
    for (int i = 0; i < 100; i++)
        a[i] = 1;

    float scale_factor = 3.4;

#pragma omp parallel
#pragma omp single
    for (int i = 0; i < 100; i++)
#pragma omp task
        a[i] = a[i] * scale_factor;

    if (!compare_double(a[9], 3.4))
        abort();

    return 0;
}
