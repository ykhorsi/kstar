#include <cassert>

int main()
{
    int b[10];
    #pragma omp parallel firstprivate(b)
    {
        assert(sizeof(b) > 8);
    }
    return 0;
}
