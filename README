
KSTAR
======
1. Intro
2. Supported languages
3. OpenMP extensions
   3.1 Commute
   3.2 Priority
4. Setup
    4.1 Dependencies
    4.2 Clang/LLVM
    4.3 Git submodules
    4.4 Build
5. Usage
6. License
7. Contact

1. Intro
========

KSTAR is a source-to-source OpenMP C/C++ compiler based on Clang for portable
and efficient application programming on task-based runtime systems. KSTAR can
be used for translating OpenMP constructs into runtime calls like StarPU and
KAAPI. Though, KAAPI is currently unmaintained and disabled by default in the
configure script.

KSTAR fully supports OpenMP 4.0, as well as some parts of OpenMP 4.5 and it
also supports some OpenMP extensions which are not part of the actual
specification, they are described in the OpenMP extensions section.

The main advantage of using KSTAR is to be able to test an OpenMP application
without changing any lines in the original source code. That way, users can try
the StarPU task-based runtime easily. The usage is pretty straightforward,
basically you just have to replace gcc by kstar from the command line.

2. Supported languages
======================

Because KSTAR is based on Clang frontend, it supports C/C++, but unfortunately
don't expect anything about Fortran soon because Clang has no plan for it.

3. OpenMP extensions
====================
3.1. Commute
------------

Commute is a new depend clause used for expressing the commutativity and for
allowing the runtime to improve the scheduling of tasks. It's not supported by
the official specification.

3.2 Priority
------------

Priority is a new clause added to the task constructs. It allows to set up
different priorities for tasks. This extension is now part of OpenMP 4.5,
although KSTAR supports it for a long time ago.

4. Setup
========
4.1. Dependencies
-----------------

KSTAR has various dependencies. however, there are four main dependencies:
* StarPU:
  This software is provided as a Git submodule into the Git source tree of
  KSTAR. However, the configure script allows you to choose between:
  - using the internal version of StarPU (recommended)
  - using an external (already installed) version of StarPU. In this case, the
    developer must take care to check that the installed StarPU version is
    recent (starpu-1.3 trunk).
  - disabling StarPU (in this case, the StarPU target wont be available)

* KAAPI:
  This software is provided as a Git submodule into the Git source tree of
  KSTAR. However, the configure option allows you to choose between:
  - using the internal version of KAAPI (recommended)
  - using an external (already installed) version of KAAPI. In this case, the
    developer must take care to check that the installed KAAPI version is
    recent.
  - disabling KAAPI (in this case, the KAAPI target wont be available)

* LLVM:
  This software is provided as a Git submodule into the Git source tree of
  KSTAR. This is the upstream version of the official project hosted on Github
  (https://github.com/llvm-mirror/llvm.git). This dependency is required for
  building Clang.

* Clang:
  This software is provided as a Git submodule into the Git source tree of
  KSTAR. It is based on the upstream version of the official project hosted on
  Github but a few patches for specific needs have been applied
  (https://scm.gforge.inria.fr/anonscm/git/kstar/clang-omp.git). You need to
  use that specific version for KSTAR.
* Python >= 2.7

4.2. Clang/LLVM
---------------

The compilation of Clang/LLVM is long. So, once compiled, Clang/LLVM wont be
recompiled in KSTAR unless a "make distclean" is run (i.e. "make clean" is not
enough to force a Clang/LLVM rebuild).

WARNING
*******
It means that after an source update that modifies the Clang/LLVM source files,
you *must* run "make distclean".

Alternatively, you can only remove the third_party/llvm-build.stamp file to
force "make" to be run again in the Clang/LLVM tree.

If you want to build Clang/LLVM with GCC installed in a non-standard path, you
need to set the --with-gcc-toolchain option to the path where GCC is installed
and to set accordingly CC/CXX envvars, for example:

    $ export CC=/usr/local/gcc/4.9.0/bin/gcc
    $ export CXX=/usr/local/gcc/4.9.0/bin/g++
    $ ./configure --with-gcc-toolchain=/usr/local/gcc/4.9.0

WARNING
*******
The Clang/LLVM version used in this project doesn't compile with GCC >= 5.0, if
you want to use such version, please look at this similar report, providing a 2
lines patch to fix the issue : https://sft.its.cern.ch/jira/browse/ROOT-7047.

4.3. Git submodules
-------------------

KSTAR upstream sources are managed with Git and Git submodules. As said
previously, all dependences are provided as submodules.

WARNING
*******
Submodules are *not* automatically updated when a "git pull" is done. It means
that if submodule references are updated, you must run "git submodule update"
after "git pull".

Building the internal copy of StarPU will create files into the
third_party/xkaapi directory in the sources tree (even if an external build
tree is used). It is not a problem but it means that "git status" will never
report a clean working directory. It will always say that third_party/xkaapi
contains "untracked content". The same thing applies for StarPU.

4.4. Build
----------

KAAPI runtime is disabled by default, add --with-kaapi=yes if you want it.

$ ./autogen.sh
$ ./configure
$ make -j<jobs>
$ make install

5. Usage
========

Make sure that everything is set up correctly, and run:

    $ kstar --runtime [starpu|kaapi] -fopenmp file.c

All options are directly passed to the target compiler, for example:

    $ kstar --runtime starpu -fopenmp -g -Wall -std=c11 file.c

For MPI, you need to overwrite the "native" C/C++ compiler with
KSTAR_C/KSTAR_CXX. For example:

KSTAR_CC=mpicc KSTAR_CXX=mpic++ kstar --runtime [starpu|kaapi] -fopenmp file.c

6. License
==========

See COPYING file

5. Contact
==========

Mailing list: kstar-public@lists.gforge.inria.fr
