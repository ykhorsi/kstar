#Status of KSTAR regarding the OpenMP specification

###Requirements

* Backend compiler have to support the `_Thread_local_` keyword from C11 for threadprivate clauses (GCC 4.9.0+)
* libnuma (if using kaapi)
* libhwloc (if using starpu)
* python 2.6.9
* glibc 2.17

##OpenMP support

| | Kaapi | StarPU |
|-|-|-|
||||
| <b>`#pragma omp parallel ...`</b>| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... if(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... num_threads(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... default(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... private(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... firstprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... shared(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... copyin(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... reduction(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... proc_bind(master)`| <span class="kstar_ok">YES</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... proc_bind(close)`| <span class="kstar_ok">YES</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... proc_bind(spread)`| <span class="kstar_ok">YES</span> | <span class="kstar_ko">NO</span>|
||||
| <b>`#pragma omp for ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... private(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... firstprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... lastprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... reduction(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... schedule(static, ...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... schedule(dynamic, ...)`| <span class="kstar_ko">NO</span> | <span class="kstar_ok">YES</span>|
| `... schedule(guided, ...)`| <span class="kstar_ko">NO</span> | <span class="kstar_ok">YES</span>|
| `... schedule(runtime, ...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [KSTAR extension] `... schedule(adaptive, ...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ko">NO</span>|
| `... collapse(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... ordered`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... nowait`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp sections ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... private(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... firstprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... lastprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... reduction(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... nowait`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp single ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... private(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... firstprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... copyprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... nowait`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp parallel for`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp parallel sections`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp task ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... if(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... final(...)`| <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| `... untied(...)`| <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| `... default(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... mergeable(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... private(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... firstprivate(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... shared(...)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... depend(in: ...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... depend(out: ...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... depend(inout: ...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp taskyield`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp taskgroup`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp master`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp critical`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp barrier`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp taskwait`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp atomic ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... atomic (read)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... atomic (write)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... atomic (update)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| `... atomic (capture)`| <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp flush`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp ordered`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| <b>`#pragma omp threadprivate`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp simd ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... safelen(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... linear(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... aligned(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... private(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... lastprivate(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... reduction(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... collapse(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp declare simd ...`</b> | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... simdlen(...)`  | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... linear(...)`  | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... aligned(...)`  | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... uniform(...)`  | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... inbranch(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
| [OpenMP 4.0] `... notinbranch(...)` | <span class="kstar_ok">YES</span> | <span class="kstar_ok">YES</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp loop simd`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target ...`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... device(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... map(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... if(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target data`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target update ...`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... device(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... if(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... from(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... to(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp declare target`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp teams ...`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... num_teams(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... thread_limit(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... default(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... private(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... firstprivate(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... shared(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
| [OpenMP 4.0] `... reduction(...)` | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp distribute simd`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp parallel loop simd`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target teams`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp teams distribute simd`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target teams distribute`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp teams distribute parallel for`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp target teams distribute parallel for simd`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp cancel`</b> | <span class="kstar_ok">OK</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp cancellation point`</b> | <span class="kstar_ok">OK</span> | <span class="kstar_ko">NO</span>|
||||
| [OpenMP 4.0]<b>`#pragma omp declare reduction(...)`</b> | <span class="kstar_ko">NO</span> | <span class="kstar_ko">NO</span>|

##Specifications

Default maximum number of threads for StarPU is the number of cpu on the computer - 1.
