#include <assert.h>
#include <cstdlib>
#include <omp.h>

class A {
public:
    template <class T>
    T a(const T & a, const T & b) {
        T res = 0;
        #pragma omp parallel
        {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((a - b) != -4);
        }
        return res;
    }

    template <class T>
    T unused(const T & a, const T & b) {
        T res = 0;
        #pragma omp parallel
        {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((a - b) != -4);
        }
        return res;
    }
};

class B {
public:
    template <class T>
    static T b(const T & a, const T & b) {
        T res = 0;
        #pragma omp parallel
        {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((a - b) != -4);
        }
        return res;
    }
};

template <class T>
class C {
public:
    template <class U>
    static U c(const U & a, const U & b) {
        U res = 0;
        #pragma omp parallel
        {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((a - b) != -4);
        }
        return res;
    }
};

template <class T>
class D {
public:
    static int d(const T & a, const int & b) {
        int res = 0;
        #pragma omp parallel
        {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((a - b) != -4);
        }
        return res;
    }
};

template <typename T>
class E {
private:
    int m_a, m_b, m_res;

public:
    E(const int & a, const int & b)
        : m_a(a), m_b(b), m_res(0) {

        #pragma omp parallel
        {
            m_res |= (omp_get_thread_num() >= omp_get_num_threads());
            m_res |= ((m_a - m_b) != -4);
        }
    }

    const int & getResult() const {
        return m_res;
    }
};

// An useless class template which no specializations
// (do not need to be parsed)
template <typename T>
class Useless {
private:
    int m_a, m_b, m_res;

public:
    Useless(const int & a, const int & b)
        : m_a(a), m_b(b), m_res(0) {

        #pragma omp parallel
        {
            m_res |= (omp_get_thread_num() >= omp_get_num_threads());
            m_res |= ((m_a - m_b) != -4);
        }
    }

    const int & getResult() const {
        return m_res;
    }
};

template <typename T>
class F {
private:
    int m_a, m_b, *m_res;

public:
    F(const int & a, const int & b)
        : m_a(a), m_b(b), m_res(NULL) {

        #pragma omp parallel num_threads(1)
        {
            /* Used to check C++ casts and new operator. */
            m_res = (int *)new int[1];
            const_cast<int&>(m_res[0]) = 4;
            int *p1 = reinterpret_cast<int *>(m_res[0]);
            (void)p1;
            F *p2 = dynamic_cast<F *>(this);
            (void)p2;

            m_res[0] = ((m_a - m_b) != -4);
        }
    }

    ~F() {
        delete m_res;
    }
    const int & getResult() const {
        return m_res[0];
    }
};

int main()
{
    E<int> e(12, 16);
    A a;
    F<int> f(12, 16);

    if (a.a<int>(12, 16) != 0)
        abort();
    if (B::b<int>(12, 16) != 0)
        abort();
    if (C<int>::c<int>(12, 16) != 0)
        abort();
    if (D<int>::d(12, 16) != 0)
        abort();
    if (e.getResult() != 0)
        abort();
    if (f.getResult() != 0)
        abort();
    return 0;
}
