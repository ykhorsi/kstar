#include <stdio.h>
#include <stdlib.h>

void fibonacci(long* result, const long n)
{
    if (n<2)
        *result = n;
    else 
    {
        long r1,r2;
        fibonacci( &r1, n-1 );
        fibonacci( &r2, n-2 );
        *result = r1 + r2;
    }
}

static void check_result(const int n, const long *result)
{
#pragma omp task
    if (n != 10 || *result != 55)
        abort();
#pragma omp taskwait
}

int main( int argc, char** argv)
{
    long result = 0;
    int n;
    if (argc >1) n = atoi(argv[1]);
    else n = 10;
#pragma omp parallel 
#pragma omp single
    {
#pragma omp task shared(result)
      fibonacci(&result, n);
#pragma omp taskwait
#pragma omp task shared(result)
      check_result(n, &result);
    }
    return 0;
}
