#ifndef KSTAR_FRONTEND_UTILS_H
#define KSTAR_FRONTEND_UTILS_H

#include <clang/Lex/Preprocessor.h>

namespace kstar {

/// DoPrintPreprocessedInput - Implements -E mode with -P option, and
/// without inclusion directives.
///
/// According to the spec of GCC:
///
/// -P: Inhibit generation of linemarkers in the output from the preprocessor.
/// This might be useful when running the preprocessor on something that is not
/// C code, and will be sent to a program which might be confused by the
/// linemarkers. out inclusion directives.
///
/// Rewriting inclusion directives is done with a previous frontend action,
/// that's why we don't processes them here. Instead, inclusion directives
/// are just print in a form that will be properly accepted back as a
/// definition, and all processed tokens come from the main source file.
void DoPrintPreprocessedInput(clang::Preprocessor &PP, llvm::raw_ostream *OS,
                              const clang::PreprocessorOutputOptions &Opts);

/// DoRewriteIncludesInInput - Implement -frewrite-includes mode.
void DoRewriteIncludesInInput(clang::Preprocessor &PP, llvm::raw_ostream *OS,
                              const clang::PreprocessorOutputOptions &Opts);

} // end kstar namespace

#endif
