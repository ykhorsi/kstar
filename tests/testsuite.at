dnl TODO: Make use of kstar++ for c++ tests.
m4_define([RUNTIME_CHECK], [
    AT_CHECK([echo opt=$kstar_options], 0, [stdout], [stderr])
    AT_CHECK([kstar $kstar_options --runtime=$2 --debug -g ${abs_top_srcdir}/tests/$1 $3 -fopenmp], 0, [stdout], [stderr])
    AT_CHECK([timeout 5s ./a.out], 0, [stdout], [stderr])
])

dnl Macro for StarPU runtime.
dnl <test> <filename> <cflags> <envvars> <if_fail> <if_skip>
m4_define([STARPU_CHECK], [
    AT_SETUP([$1 "starpu"])
    AT_KEYWORDS([starpu])
    AT_SKIP_IF([m4_ifnblank([$6], [true], [$STARPU_XFAIL])])
    AT_XFAIL_IF([m4_ifnblank([$5], [true], [false])])
    AS_IF([test x"$4" != x], [export STARPU_NCPU=$4])
    RUNTIME_CHECK([$2], ["starpu"], [$3])
    AS_IF([test x"$4" != x], [unset STARPU_NCPU])
    AT_CLEANUP()
])

dnl Macro for KAAPI runtime.
dnl <test> <filename> <cflags> <envvars> <if_fail> <if_skip>
m4_define([XKAAPI_CHECK], [
    AT_SETUP([$1 "kaapi"])
    AT_KEYWORDS([kaapi])
    AT_SKIP_IF([m4_ifnblank([$6], [true], [$KAAPI_XFAIL])])
    AT_XFAIL_IF([m4_ifnblank([$5], [true], [false])])
    RUNTIME_CHECK([$2], ["kaapi"], [$3])
    AT_CLEANUP()
])

m4_define([KSTAR_TEST], [
    STARPU_CHECK([$1], [$2], [$3], [$4], [$5], [$7])
    XKAAPI_CHECK([$1], [$2], [$3], [$4], [$6], [$8])
])

m4_define([KF_KSTAR_TEST],[KSTAR_TEST([$1],[$2],[$3],[$4],[],[kaapi-fail])])
m4_define([SF_KSTAR_TEST],[KSTAR_TEST([$1],[$2],[$3],[$4],[starpu-fail],[])])
m4_define([SKF_KSTAR_TEST],[KSTAR_TEST([$1],[$2],[$3],[$4],[starpu-fail],[kaapi-fail])])

AT_COLOR_TESTS()

AT_INIT("")

AT_ARG_OPTION([installed],
  [AS_HELP_STRING([--installed],
               [use installed binaries to perform tests])],
  [AS_IF([test x"$at_arg_installed" = xfalse ],
    [kstar_options="--in-build-tree"],
    [kstar_options=""])
  ],[kstar_options="--in-build-tree"])

AT_BANNER("KSTAR testsuite - loop")

KSTAR_TEST("for private C", "kstar-testsuite/c/for1.c")
KSTAR_TEST("for firstprivate C", "kstar-testsuite/c/for2.c")
KSTAR_TEST("for lastprivate C", "kstar-testsuite/c/for3.c")
KSTAR_TEST("for lastprivate 2 C", "kstar-testsuite/c/for4.c")
KSTAR_TEST("for implicit private C", "kstar-testsuite/c/for5.c")
KSTAR_TEST("for first/lastprivate C", "kstar-testsuite/c/for6.c")
KSTAR_TEST("parallel for first/lastprivate C", "kstar-testsuite/c/for7.c")

AT_BANNER("KSTAR testsuite - data sharing")

KSTAR_TEST("private C", "kstar-testsuite/c/sharing1.c")
KSTAR_TEST("firstprivate C", "kstar-testsuite/c/sharing2.c")
KSTAR_TEST("firstprivate array C", "kstar-testsuite/c/sharing-array.c")
KSTAR_TEST("firstprivate array C++", "kstar-testsuite/c++/sharing-array.cpp")
KSTAR_TEST("firstprivate array 2 C++", "kstar-testsuite/c++/size.cpp")
KSTAR_TEST("task firstprivate array C++", "kstar-testsuite/c++/task-firstprivate.cpp")
KSTAR_TEST("share C", "kstar-testsuite/c/sharing3.c")
KSTAR_TEST("lastprivate C", "kstar-testsuite/c/sharing4.c")
KSTAR_TEST("last/firstprivate C", "kstar-testsuite/c/sharing5.c")
KSTAR_TEST("task private C", "kstar-testsuite/c/sharing6.c")
KSTAR_TEST("threadprivate C", "kstar-testsuite/c/static_tp.c")
KSTAR_TEST("schedule chunksize C", "kstar-testsuite/c/schedule_chunksize.c")

AT_BANNER("KSTAR testsuite - misc")

KSTAR_TEST("local typedef C", "kstar-testsuite/c/parallel-typedef.c")
KSTAR_TEST("local static var C", "kstar-testsuite/c/local-static-var.c")
KSTAR_TEST("local struct decl C", "kstar-testsuite/c/local-struct.c")
KSTAR_TEST("local struct decl C++", "kstar-testsuite/c++/local-struct.cpp")
KSTAR_TEST("private attributes C++", "kstar-testsuite/c++/priv_attr.cpp")
KSTAR_TEST("macro C", "kstar-testsuite/c/macro.c")

AT_BANNER("KSTAR testsuite - inclusion")

KSTAR_TEST("nested include headers C", "kstar-testsuite/c/nested_include_omp/test.c", [], 3)

AT_BANNER("KSTAR testsuite - scale vector")

KSTAR_TEST("static array C", "kstar-testsuite/c/scale_vector.c")
KSTAR_TEST("variable length array C", "kstar-testsuite/c/scale_vector2.c")

AT_BANNER("KSTAR testsuite - template")

KSTAR_TEST("simple template C++", kstar-testsuite/c++/simple_template.cpp)
KSTAR_TEST("function template C++", "kstar-testsuite/c++/function_template.cpp", [-std=c++11])
KSTAR_TEST("class template C++", "kstar-testsuite/c++/class_template.cpp")

AT_BANNER("KSTAR testsuite - task")

KSTAR_TEST("simple depend C", "kstar-testsuite/c/simple_depend.c")
KSTAR_TEST("matmul depend C", "kstar-testsuite/c/task_matmul_depend.c")
KSTAR_TEST("simple depend C++", "kstar-testsuite/c++/simple_depend.cpp")
KSTAR_TEST("mergeable C", "kstar-testsuite/c/task_mergeable.c")
KSTAR_TEST("task commute C", "kstar-testsuite/c/task_commute.c")
dnl task_where.c not present in Git's master branch
dnl SKF_KSTAR_TEST("task where C", "kstar-testsuite/c/task_where.c")

AT_BANNER("KSTAR testsuite - fibonnacci")

KSTAR_TEST("Easy fibo C", "kstar-testsuite/c/fibonacci1.c")
KSTAR_TEST("Nested C++, "kstar-testsuite/c++/fibonacci1cpp.cpp")
KSTAR_TEST("Nested C", "kstar-testsuite/c/fibonacci.c")
KSTAR_TEST("with depends C++", "kstar-testsuite/c++/fibo_depends.cpp")

AT_BANNER("KSTAR testsuite - concurrent write")

SF_KSTAR_TEST("concurrent write C", "kstar-testsuite/c/concurrent_write.c")

AT_BANNER("KSTAR testsuite - critical")

KSTAR_TEST("multi C", "kstar-testsuite/c/critical-multi.c")

AT_BANNER("KSTAR testsuite - single")

KSTAR_TEST("single 1 C", "kstar-testsuite/c/single-1.c")

dnl target support needs additional work
dnl AT_BANNER("KSTAR testsuite - target")
dnl SKF_KSTAR_TEST("target C", "kstar-testsuite/c/omp_target.c")

AT_BANNER("KSTAR testsuite - copyin")

KSTAR_TEST("copyin C", "kstar-testsuite/c/copyin.c")

AT_BANNER("GCC testsuite - hello")

KSTAR_TEST("omp C", "gcc-testsuite/libgomp.c/omp_hello.c")

AT_BANNER("GCC testsuite - atomic")

KSTAR_TEST("atomic 1 C", "gcc-testsuite/libgomp.c/atomic-1.c")
KSTAR_TEST("atomic 2 C", "gcc-testsuite/libgomp.c/atomic-2.c")
KSTAR_TEST("atomic 3 C", "gcc-testsuite/libgomp.c/atomic-3.c")
KSTAR_TEST("atomic 4 C", "gcc-testsuite/libgomp.c/atomic-4.c")
KSTAR_TEST("atomic 5 C", "gcc-testsuite/libgomp.c/atomic-5.c")
KSTAR_TEST("atomic 6 C", "gcc-testsuite/libgomp.c/atomic-6.c")
KSTAR_TEST("atomic 10 C", "gcc-testsuite/libgomp.c/atomic-10.c")
KSTAR_TEST("atomic 11 C", "gcc-testsuite/libgomp.c/atomic-11.c")
KSTAR_TEST("atomic 12 C", "gcc-testsuite/libgomp.c/atomic-12.c")
KSTAR_TEST("atomic 13 C", "gcc-testsuite/libgomp.c/atomic-13.c")
KSTAR_TEST("atomic 14 C", "gcc-testsuite/libgomp.c/atomic-14.c")
KSTAR_TEST("atomic 15 C", "gcc-testsuite/libgomp.c/atomic-15.c")
KSTAR_TEST("atomic 16 C", "gcc-testsuite/libgomp.c/atomic-16.c")
KSTAR_TEST("atomic 17 C", "gcc-testsuite/libgomp.c/atomic-17.c")
KSTAR_TEST("atomic 18 C", "gcc-testsuite/libgomp.c/atomic-18.c")
KSTAR_TEST("atomic 1 C++", "gcc-testsuite/libgomp.c++/atomic-1.C")
KSTAR_TEST("atomic 2 C++", "gcc-testsuite/libgomp.c++/atomic-2.C")
KSTAR_TEST("atomic 3 C++", "gcc-testsuite/libgomp.c++/atomic-3.C")
KSTAR_TEST("atomic 12 C++", "gcc-testsuite/libgomp.c++/atomic-12.C")

AT_BANNER("GCC testsuite - collapse")

KSTAR_TEST("collapse 1 C", "gcc-testsuite/libgomp.c/collapse-1.c")
KSTAR_TEST("collapse 2 C", "gcc-testsuite/libgomp.c/collapse-2.c")
KSTAR_TEST("collapse 3 C", "gcc-testsuite/libgomp.c/collapse-3.c")
KSTAR_TEST("collapse 1 C++", "gcc-testsuite/libgomp.c++/collapse-1.C")

AT_BANNER("GCC testsuite - critical")

KSTAR_TEST("critical 2 C", "gcc-testsuite/libgomp.c/critical-2.c")

AT_BANNER("GCC testsuite - flush")

KSTAR_TEST("flush 1 C", "gcc-testsuite/libgomp.c/flush-1.c")

AT_BANNER("GCC testsuite - loop")

KSTAR_TEST("loop 3 C", "gcc-testsuite/libgomp.c/loop-3.c")
KSTAR_TEST("loop 4 C", "gcc-testsuite/libgomp.c/loop-4.c")
KSTAR_TEST("loop 5 C", "gcc-testsuite/libgomp.c/loop-5.c")
KSTAR_TEST("loop 6 C", "gcc-testsuite/libgomp.c/loop-6.c")
KSTAR_TEST("loop 7 C", "gcc-testsuite/libgomp.c/loop-7.c")
KSTAR_TEST("loop 8 C", "gcc-testsuite/libgomp.c/loop-8.c")
KSTAR_TEST("loop 9 C", "gcc-testsuite/libgomp.c/loop-9.c")
KSTAR_TEST("loop 10 C", "gcc-testsuite/libgomp.c/loop-10.c")
KSTAR_TEST("loop 11 C", "gcc-testsuite/libgomp.c/loop-11.c")
KSTAR_TEST("loop 12 C", "gcc-testsuite/libgomp.c/loop-12.c")
KSTAR_TEST("omp loop 01 C", "gcc-testsuite/libgomp.c/omp-loop01.c")
KSTAR_TEST("omp loop 02 C", "gcc-testsuite/libgomp.c/omp-loop02.c")
KSTAR_TEST("omp loop 03 C", "gcc-testsuite/libgomp.c/omp-loop03.c")
KSTAR_TEST("parafor C", "gcc-testsuite/libgomp.c/omp-parallel-for.c")
KSTAR_TEST("loop 1 C++", "gcc-testsuite/libgomp.c++/loop-1.C")
KSTAR_TEST("loop 2 C++", "gcc-testsuite/libgomp.c++/loop-2.C")
KSTAR_TEST("loop 3 C++", "gcc-testsuite/libgomp.c++/loop-3.C")
KSTAR_TEST("loop 4 C++", "gcc-testsuite/libgomp.c++/loop-4.C")
KSTAR_TEST("loop 5 C++", "gcc-testsuite/libgomp.c++/loop-5.C")
KSTAR_TEST("loop 6 C++", "gcc-testsuite/libgomp.c++/loop-6.C")
KSTAR_TEST("loop 7 C++", "gcc-testsuite/libgomp.c++/loop-7.C")
KSTAR_TEST("loop 8 C++", "gcc-testsuite/libgomp.c++/loop-8.C")
KSTAR_TEST("loop 9 C++", "gcc-testsuite/libgomp.c++/loop-9.C")
KSTAR_TEST("loop 10 C++", "gcc-testsuite/libgomp.c++/loop-10.C")
KSTAR_TEST("loop 11 C++", "gcc-testsuite/libgomp.c++/loop-11.C")
KSTAR_TEST("loop 12 C++", "gcc-testsuite/libgomp.c++/loop-12.C")
SKF_KSTAR_TEST("for 2 C++", "gcc-testsuite/libgomp.c++/for-2.C")
KSTAR_TEST("for 6 C++", "gcc-testsuite/libgomp.c++/for-6.C")
KSTAR_TEST("for 7 C++", "gcc-testsuite/libgomp.c++/for-7.C", [-std=c++11])

AT_BANNER("GCC testsuite - nested")

SF_KSTAR_TEST("nested 1 C", "gcc-testsuite/libgomp.c/nested-1.c")
KSTAR_TEST("nested 2 C", "gcc-testsuite/libgomp.c/nested-2.c", [], 4)
SF_KSTAR_TEST("nested 3 C", "gcc-testsuite/libgomp.c/nested-3.c")
KSTAR_TEST("omp nested 1 C", "gcc-testsuite/libgomp.c/omp-nested-1.c")


AT_BANNER("GCC testsuite - depend")

KSTAR_TEST("depend 1 C", "gcc-testsuite/libgomp.c/depend-1.c")
KF_KSTAR_TEST("depend 2 C", "gcc-testsuite/libgomp.c/depend-2.c")
KF_KSTAR_TEST("depend 3 C", "gcc-testsuite/libgomp.c/depend-3.c")
KSTAR_TEST("depend 4 C", "gcc-testsuite/libgomp.c/depend-4.c")
KF_KSTAR_TEST("depend 5 C", "gcc-testsuite/libgomp.c/depend-5.c")

AT_BANNER("GCC testsuite - misc")

KSTAR_TEST("matvec C", "gcc-testsuite/libgomp.c/omp_matvec.c")
KF_KSTAR_TEST("sort C", "gcc-testsuite/libgomp.c/sort-1.c")
KSTAR_TEST("debug C", "gcc-testsuite/libgomp.c/debug-1.c")
KSTAR_TEST("omp parallel if C", "gcc-testsuite/libgomp.c/omp-parallel-if.c", [], 10)
KSTAR_TEST("omp parallel if C++", "gcc-testsuite/libgomp.c++/parallel-1.C", [], 10)

AT_BANNER("GCC testsuite - single")

KSTAR_TEST("single 2 C", "gcc-testsuite/libgomp.c/single-2.c")
KSTAR_TEST("omp single 1 C", "gcc-testsuite/libgomp.c/omp-single-1.c")
KSTAR_TEST("omp single 2 C", "gcc-testsuite/libgomp.c/omp-single-2.c")
KSTAR_TEST("omp single 3 C", "gcc-testsuite/libgomp.c/omp-single-3.c")
SKF_KSTAR_TEST("omp master 1 C++", "gcc-testsuite/libgomp.c++/master-1.C")
KSTAR_TEST("single 1 C++", "gcc-testsuite/libgomp.c++/single-1.C")
KSTAR_TEST("single 2 C++", "gcc-testsuite/libgomp.c++/single-2.C")
KSTAR_TEST("single 3 C++", "gcc-testsuite/libgomp.c++/single-3.C")

AT_BANNER("GCC testsuite - pr")

KSTAR_TEST("pr24455 C", "gcc-testsuite/libgomp.c/pr24455.c")
KSTAR_TEST("pr24455 C++", "gcc-testsuite/libgomp.c++/pr24455.C", [${abs_top_srcdir}/tests/gcc-testsuite/libgomp.c++/pr24455-1.C -std=c++11])
KSTAR_TEST("pr26171 C", "gcc-testsuite/libgomp.c/pr26171.c")
KSTAR_TEST("pr26691 C++", "gcc-testsuite/libgomp.c++/pr26691.C")
SKF_KSTAR_TEST("pr26943 C++", "gcc-testsuite/libgomp.c++/pr26943.C")
KSTAR_TEST("pr26943-1 C", "gcc-testsuite/libgomp.c/pr26943-1.c", [], 16)
KSTAR_TEST("pr27337 C++", "gcc-testsuite/libgomp.c++/pr27337.C")
KSTAR_TEST("pr29947-1 C", "gcc-testsuite/libgomp.c/pr29947-1.c")
KSTAR_TEST("pr29947-2 C", "gcc-testsuite/libgomp.c/pr29947-2.c")
KSTAR_TEST("pr30494 C", "gcc-testsuite/libgomp.c/pr30494.c")
KSTAR_TEST("pr30703 C++", "gcc-testsuite/libgomp.c++/pr30703.C", [], 5)
dnl two following tests rely on num_threads support which is not yet available for omp tasks on StarPU
dnl SKF_KSTAR_TEST("pr32362-2 C", "gcc-testsuite/libgomp.c/pr32362-2.c")
dnl SKF_KSTAR_TEST("pr32362-3 C", "gcc-testsuite/libgomp.c/pr32362-3.c")
KSTAR_TEST("pr32468 C", "gcc-testsuite/libgomp.c/pr32468.c", [], 4)
KSTAR_TEST("pr34513 C", "gcc-testsuite/libgomp.c/pr34513.c", [], 4)
KSTAR_TEST("pr34513 C++", "gcc-testsuite/libgomp.c++/pr34513.C", [], 4)
KSTAR_TEST("pr35185 C++", "gcc-testsuite/libgomp.c++/pr35185.C")
KSTAR_TEST("pr35196 C", "gcc-testsuite/libgomp.c/pr35196.c")
KSTAR_TEST("pr35549 C", "gcc-testsuite/libgomp.c/pr35549.c")
KSTAR_TEST("pr35625 C", "gcc-testsuite/libgomp.c/pr35625.c")
KSTAR_TEST("pr36802-1 C", "gcc-testsuite/libgomp.c/pr36802-1.c")
KSTAR_TEST("pr36802-2 C", "gcc-testsuite/libgomp.c/pr36802-2.c")
KSTAR_TEST("pr36802-3 C", "gcc-testsuite/libgomp.c/pr36802-3.c")
KSTAR_TEST("pr38650 C", "gcc-testsuite/libgomp.c/pr38650.c")
KSTAR_TEST("pr38650 C++", "gcc-testsuite/libgomp.c++/pr38650.C")
KSTAR_TEST("pr39154 C", "gcc-testsuite/libgomp.c/pr39154.c")
KSTAR_TEST("pr39573.C++", "gcc-testsuite/libgomp.c++/pr39573.C")
KF_KSTAR_TEST("pr56217.C++", "gcc-testsuite/libgomp.c++/pr56217.C", [-std=c++0x])
SKF_KSTAR_TEST("pr63248.C++", "gcc-testsuite/libgomp.c++/pr63248.C")
KF_KSTAR_TEST("pr39591-1 C", "gcc-testsuite/libgomp.c/pr39591-1.c")
KF_KSTAR_TEST("pr39591-2 C", "gcc-testsuite/libgomp.c/pr39591-2.c")
KF_KSTAR_TEST("pr39591-3 C", "gcc-testsuite/libgomp.c/pr39591-3.c")
KSTAR_TEST("pr42029 C", "gcc-testsuite/libgomp.c/pr42029.c")
SF_KSTAR_TEST("pr42942 C", "gcc-testsuite/libgomp.c/pr42942.c")
KSTAR_TEST("pr43893 C", "gcc-testsuite/libgomp.c/pr43893.c")
KSTAR_TEST("pr43893 C++", "gcc-testsuite/libgomp.c++/pr43893.C")
KSTAR_TEST("pr49043 C++", "gcc-testsuite/libgomp.c++/pr49043.C", [-std=c++11])
KSTAR_TEST("pr49897-1 C", "gcc-testsuite/libgomp.c/pr49897-1.c")
KSTAR_TEST("pr49897-2 C", "gcc-testsuite/libgomp.c/pr49897-2.c")
KSTAR_TEST("pr49898-1 C", "gcc-testsuite/libgomp.c/pr49898-1.c")
KSTAR_TEST("pr49898-2 C", "gcc-testsuite/libgomp.c/pr49898-2.c")

AT_BANNER("GCC testsuite - data sharing")

KSTAR_TEST("private C", "gcc-testsuite/libgomp.c/private-1.c")
KSTAR_TEST("share 1 C", "gcc-testsuite/libgomp.c/shared-1.c", [], 5)
KSTAR_TEST("share 2 C", "gcc-testsuite/libgomp.c/shared-2.c")
KSTAR_TEST("share 3 C", "gcc-testsuite/libgomp.c/shared-3.c")
KSTAR_TEST("share 1 C++", "gcc-testsuite/libgomp.c++/shared-1.C", [], 5)
KSTAR_TEST("share 2 C++", "gcc-testsuite/libgomp.c++/shared-2.C")

AT_BANNER("GCC testsuite - reduction")

KSTAR_TEST("reduction 1 C", "gcc-testsuite/libgomp.c/reduction-1.c")
KSTAR_TEST("reduction 2 C", "gcc-testsuite/libgomp.c/reduction-2.c")
KSTAR_TEST("reduction 3 C", "gcc-testsuite/libgomp.c/reduction-3.c")
KSTAR_TEST("reduction 4 C", "gcc-testsuite/libgomp.c/reduction-4.c")
KSTAR_TEST("reduction 5 C", "gcc-testsuite/libgomp.c/reduction-5.c")
KSTAR_TEST("reduction 6 C", "gcc-testsuite/libgomp.c/reduction-6.c")
KSTAR_TEST("omp reduction C", "gcc-testsuite/libgomp.c/omp_reduction.c")
KSTAR_TEST("omp orphan C", "gcc-testsuite/libgomp.c/omp_orphan.c")
KSTAR_TEST("reduction 1 C++", "gcc-testsuite/libgomp.c++/reduction-1.C")
KSTAR_TEST("reduction 2 C++", "gcc-testsuite/libgomp.c++/reduction-2.C")
KSTAR_TEST("reduction 3 C++", "gcc-testsuite/libgomp.c++/reduction-3.C")
KSTAR_TEST("reduction 4 C++", "gcc-testsuite/libgomp.c++/reduction-4.C")

AT_BANNER("GCC testsuite - task")

dnl first six task tests rely on num_threads support which is not yet available for omp tasks on StarPU
dnl SF_KSTAR_TEST("task 1 C", "gcc-testsuite/libgomp.c/task-1.c")
dnl SF_KSTAR_TEST("task 3 C", "gcc-testsuite/libgomp.c/task-3.c")
dnl SF_KSTAR_TEST("task 1 C++", "gcc-testsuite/libgomp.c++/task-1.C")
dnl SF_KSTAR_TEST("task 2 C++", "gcc-testsuite/libgomp.c++/task-2.C")
dnl SF_KSTAR_TEST("task 3 C++", "gcc-testsuite/libgomp.c++/task-3.C")
dnl SF_KSTAR_TEST("task 5 C++", "gcc-testsuite/libgomp.c++/task-5.C")
KSTAR_TEST("task 7 C++", "gcc-testsuite/libgomp.c++/task-7.C")
KF_KSTAR_TEST("task 8 C++", "gcc-testsuite/libgomp.c++/task-8.C")

AT_BANNER("GCC testsuite - taskgroup")

SKF_KSTAR_TEST("task group 1 C", "gcc-testsuite/libgomp.c/taskgroup-1.c")

AT_BANNER("GCC testsuite - work sharing")

KSTAR_TEST("workshare 1 C", "gcc-testsuite/libgomp.c/omp_workshare1.c")
KSTAR_TEST("workshare 2 C", "gcc-testsuite/libgomp.c/omp_workshare2.c")
KSTAR_TEST("workshare 4 C", "gcc-testsuite/libgomp.c/omp_workshare4.c")

AT_BANNER("GCC testsuite - section")

KSTAR_TEST("sections 1 C++", "gcc-testsuite/libgomp.c++/sections-1.C")

AT_BANNER("GCC testsuite - copyin")

KSTAR_TEST("copyin 1 C", "gcc-testsuite/libgomp.c/copyin-1.c")
SKF_KSTAR_TEST("copyin 2 C", "gcc-testsuite/libgomp.c/copyin-2.c")
KSTAR_TEST("copyin 3 C", "gcc-testsuite/libgomp.c/copyin-3.c")
KSTAR_TEST("copyin 1 C++", "gcc-testsuite/libgomp.c++/copyin-1.C", [-std=c++11])
KSTAR_TEST("copyin 2 C++", "gcc-testsuite/libgomp.c++/copyin-2.C", [-std=c++11])

AT_BANNER("GCC testsuite - ctor")

KSTAR_TEST("ctor 1 C++", "gcc-testsuite/libgomp.c++/ctor-1.C")
KSTAR_TEST("ctor 2 C++", "gcc-testsuite/libgomp.c++/ctor-2.C")
KSTAR_TEST("ctor 3 C++", "gcc-testsuite/libgomp.c++/ctor-3.C")
KSTAR_TEST("ctor 4 C++", "gcc-testsuite/libgomp.c++/ctor-4.C")
KSTAR_TEST("ctor 5 C++", "gcc-testsuite/libgomp.c++/ctor-5.C", [-std=c++11])
KSTAR_TEST("ctor 6 C++", "gcc-testsuite/libgomp.c++/ctor-6.C")
KSTAR_TEST("ctor 7 C++", "gcc-testsuite/libgomp.c++/ctor-7.C")
KSTAR_TEST("ctor 8 C++", "gcc-testsuite/libgomp.c++/ctor-8.C", [-std=c++11], 4)
KSTAR_TEST("ctor 9 C++", "gcc-testsuite/libgomp.c++/ctor-9.C", [-std=c++11], 4)
KSTAR_TEST("ctor 10 C++", "gcc-testsuite/libgomp.c++/ctor-10.C", [-std=c++11], 4)
KSTAR_TEST("ctor 11 C++", "gcc-testsuite/libgomp.c++/ctor-11.C")
KSTAR_TEST("ctor 12 C++", "gcc-testsuite/libgomp.c++/ctor-12.C")

AT_BANNER("GCC testsuite - lib")

SKF_KSTAR_TEST("lib 1 C", "gcc-testsuite/libgomp.c/lib-1.c")
SF_KSTAR_TEST("lib 2 C", "gcc-testsuite/libgomp.c/lib-2.c")

AT_BANNER("GCC testsuite - lock")

KF_KSTAR_TEST("lock 1 C", "gcc-testsuite/libgomp.c/lock-1.c")

AT_BANNER("GCC testsuite - ordered")

KSTAR_TEST("ordered 3 C", "gcc-testsuite/libgomp.c/ordered-3.c")

AT_BANNER("GCC testsuite - priority")

KF_KSTAR_TEST("priority C", "gcc-testsuite/libgomp.c/priority.c", [], 1)
