#ifndef KSTAR_ANALYSES_VARIABLESVISITOR_H
#define KSTAR_ANALYSES_VARIABLESVISITOR_H

#include <clang/AST/RecursiveASTVisitor.h>

#include <set>
#include <vector>

class GlobalVariablesVisitor
    : public clang::RecursiveASTVisitor<GlobalVariablesVisitor> {
public:
  GlobalVariablesVisitor(clang::Stmt const *S);

  bool VisitDeclRefExpr(clang::DeclRefExpr const *S);

  const std::set<clang::VarDecl const *> &getCapturedVars() const {
    return __CapturedVars;
  }

private:
  std::set<clang::VarDecl const *> __CapturedVars;
};

class LocalVariablesVisitor
    : public clang::RecursiveASTVisitor<LocalVariablesVisitor> {
public:
  LocalVariablesVisitor(clang::Stmt const *S);

  bool VisitDeclRefExpr(clang::DeclRefExpr const *S);

  const std::set<clang::VarDecl const *> &getCapturedVars() const {
    return __CapturedVars;
  }

private:
  std::set<clang::VarDecl const *> __CapturedVars;
};

class StaticLocalVariablesVisitor
    : public clang::RecursiveASTVisitor<StaticLocalVariablesVisitor> {
public:
  StaticLocalVariablesVisitor(clang::Stmt const *S);

  bool VisitDeclRefExpr(clang::DeclRefExpr const *S);

  const std::set<clang::VarDecl const *> &getCapturedVars() const {
    return __CapturedVars;
  }

private:
  std::set<clang::VarDecl const *> __CapturedVars;
};

class CapturedVariablesVisitor
    : public clang::RecursiveASTVisitor<CapturedVariablesVisitor> {
public:
  CapturedVariablesVisitor(clang::Decl *D, clang::OMPExecutableDirective *OMPD);

  bool VisitOMPExecutableDirective(clang::OMPExecutableDirective *OMPD);

  const std::vector<clang::VarDecl const *> &getCapturedVars() const {
    return __CapturedVars;
  }

private:
  clang::OMPExecutableDirective *Directive;
  std::vector<clang::VarDecl const *> __CapturedVars;
};

#endif
