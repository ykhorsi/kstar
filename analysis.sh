#!/bin/sh

WD=`pwd`

# SonarQube
mkdir -p $WD/SonarScan

# Collect coverage data
mv build/lib/.libs/*.gcda build/lib/ && mv build/lib/.libs/*.gcno build/lib/
mv build/src/*/*.gcda build/src/ && mv build/src/*/*.gcno build/src/
lcov --capture --directory $WD/build --output-file $WD/SonarScan/kstar.lcov
lcov --remove $WD/SonarScan/kstar.lcov "/usr*" "*/third_party/*" "*Kaapi*" --output-file $WD/SonarScan/kstar.lcov
ln -s /opt/lcov_cobertura-1.6/lcov_cobertura/lcov_cobertura.py $WD/SonarScan/lcov_cobertura.py
python $WD/SonarScan/lcov_cobertura.py $WD/SonarScan/kstar.lcov --output $WD/SonarScan/kstar-coverage.xml

export CPPCHECK_INCLUDES="-Iinclude -Itests/kstar-testsuite/c -Itests/kstar-testsuite/c++"
export SOURCES_TO_ANALYZE="include lib src tests/kstar-testsuite"
cppcheck  -v -f --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${CPPCHECK_INCLUDES} ${SOURCES_TO_ANALYZE} 2> $WD/SonarScan/kstar-cppcheck.xml

export SOURCES_TO_ANALYZE="include lib src tests/kstar-testsuite"
rats -w 3 --xml ${SOURCES_TO_ANALYZE} > $WD/SonarScan/kstar-rats.xml

export SOURCES_TO_ANALYZE="include lib src tests"
ln -s /opt/SonarCXX/sonar-cxx-plugin/src/tools/vera++Report2checkstyleReport.perl $WD/SonarScan/vera++Report2checkstyleReport.perl
find src/ include/ -regex ".*\.cpp\|.*\.h" | vera++ --show-rule --no-duplicate |& $WD/SonarScan/vera++Report2checkstyleReport.perl > $WD/SonarScan/kstar-vera.xml

# Create the config for sonar-scanner
export major=1
export minor=1
export version_kstar=${major}.${minor}
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.bordeaux.inria.fr/
sonar.projectKey=storm:kstar:gitlab:master

sonar.links.homepage=http://kstar.gforge.inria.fr/
sonar.projectDescription=CeCILL-C
sonar.projectVersion=$version_kstar
sonar.language=c++
sonar.sources=include, lib, src, tests/kstar-testsuite
sonar.sourceEncoding=UTF-8

sonar.cxx.includeDirectories= include/, tests/kstar-testsuite/c/nested_include_omp/
sonar.cxx.compiler.charset=UTF-8
sonar.cxx.errorRecoveryEnabled=true
sonar.cxx.compiler.parser=GCC
sonar.cxx.compiler.reportPath=build/kstar-build.log
sonar.cxx.coverage.reportPath=SonarScan/kstar-coverage.xml
sonar.cxx.cppcheck.reportPath=SonarScan/kstar-cppcheck.xml
sonar.cxx.rats.reportPath=SonarScan/kstar-rats.xml
sonar.cxx.vera.reportPath=SonarScan/kstar-vera.xml
EOF

/opt/SonarQube/3.0.3.778/bin/sonar-scanner -X -Dsonar.login=`cat $HOME/SonarQube/token.txt` > $WD/SonarScan/sonar.log
