#include <stdio.h>
#include <assert.h>

int main()
{
    int a = 4;
    int b = 8;
    #pragma omp parallel shared(a) num_threads(1)
    #pragma omp for firstprivate(a,b) lastprivate(a)
      for (int i=a|b; i>0; --i)
      {
        if (a==4) 
          a = i; 
      }
    assert( a == 12 );
    return 0;
}
