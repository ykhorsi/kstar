#include <stdio.h>
#include <omp.h>

int main()
{
    typedef int INT;
    INT a = 12;
    INT b = 16;
    int res = 0;
    #pragma omp parallel default(shared)
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((a - b) != -4);
    }
    return 0;
}
