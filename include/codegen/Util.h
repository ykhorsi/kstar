#ifndef KSTAR_CODEGEN_UTIL_H
#define KSTAR_CODEGEN_UTIL_H

#include <clang/AST/TemplateBase.h>
#include <clang/Basic/Specifiers.h>
#include <llvm/ADT/ArrayRef.h>

#include <string>
#include <vector>

namespace CodeGen {

/**
 * Generate a sizeof() call.
 */
const std::string GenSizeof(const std::string &ptr);

/**
 * Generate a memcpy() call.
 */
const std::string GenMemcpy(const std::string &dest, const std::string &src,
                            const std::string &n);

/**
 * Generate a malloc() call.
 */
const std::string GenMalloc(const std::string &size);

/**
 * Generate a thread storage class specifier.
 */
const std::string genTSCSpec(const clang::ThreadStorageClassSpecifier TCSC);

/// \brief Generate the "injected" template arguments that correspond to
/// the template parameters of a function template.
const std::string
genInjectedTemplateArgs(const llvm::ArrayRef<clang::TemplateArgument> &Args,
                        const clang::PrintingPolicy &Policy,
                        bool withArgKind = false);
}

#endif
