#include <stdlib.h>
#include <omp.h>

static void
local_struct()
{
    struct foo {
        int a;
        int b;
    };
    foo *const bar = new foo[2];
    bar[0].a = 12;
    bar[0].b = 16;
    int res = 0;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar[0].a - bar[0].b) != -4);
    }
    if (res != 0)
        abort();
}
int main()
{
    local_struct();
    return 0;
}
