#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeTarget::RuntimeTarget(OMPTargetDirective *t, ASTContext &c,
                             DataSharingInfo &dsi)
    : RuntimeScopeDirective(t, c, dsi) {

  for (VarDecl const *VD : dsi.getCapturedVars(t)) {
    if (!(to_.find(VD) != to_.end() || from_.find(VD) != from_.end() ||
          alloc_.find(VD) != alloc_.end() ||
          toFrom_.find(VD) != toFrom_.end())) {
      toFrom_[VD] = std::set<Expr const *>();
    }
  }

  if (!isa<CallExpr>(captured_->getCapturedStmt()))
    assert(!"Target directives can only contain function call!");

  // Hook to handle by value variables
  for (Expr *expr : cast<CallExpr>(captured_->getCapturedStmt())->arguments()) {
    if ((expr->getType()->isPointerType() || expr->getType()->isArrayType()) &&
        isa<DeclRefExpr>(expr->IgnoreImpCasts()))
      by_ref.push_back(cast<VarDecl>(cast<DeclRefExpr>(expr->IgnoreImpCasts())
                                         ->getDecl())->getName());
  }
}
