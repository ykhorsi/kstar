#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeSections::RuntimeSections(OMPSectionsDirective *t, ASTContext &c,
                                 DataSharingInfo &dsi)
    : RuntimeScopeDirective(t, c, dsi) {
}

std::string RuntimeSections::genReplacePragma(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  CompoundStmt *body = cast<CompoundStmt>(captured_->getCapturedStmt());
  int numSec = std::distance(body->body_begin(), body->body_end());

  std::string structVar = "params_" + std::to_string(getId());
  CodeGen::StructInit structVarAddr =
      genFillArgStruct(structVar, argStruct_);
  rso << structVarAddr.str();

  OpenMPScheduleClauseKind sched = OpenMPScheduleClauseKind::OMPC_SCHEDULE_auto;
  rso << newFunction_ << addInjectedTemplateArgs() << "(" << numSec << ", "
      << structVarAddr.getaddr() << ", 1, 1, " << getRuntime()->EmitScheduleType(sched)
      << ", 0, " << nowait_ << ");\n";

  return rso.str();
}

std::string RuntimeSections::genBeforeFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string cl_arg = "params";
  rso << "for(unsigned long long __kstar__i__ = __kstar__begin__; __kstar__i__ "
         "< __kstar__j__; __kstar__i__++){\n";
  rso << "switch(__kstar__i__){\n";
  // OUTLINE EACH SECTION IN SECTIONS
  int num_sec = 0;
  CompoundStmt *body = cast<CompoundStmt>(captured_->getCapturedStmt());
  for (Stmt **b = body->body_begin(); b != body->body_end(); b++) {
    rso << "case " << num_sec++ << ":\n";
    // Write outlined function body changing for loop iteration space
    KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi,
                       getOMPDirective());
    if (!isa<OMPSectionDirective>(*b)) {
      // Each structured block in the sections construct is preceded by a
      // section directive except possibly the first block, for which a
      // preceding section directive is optional.
      do {
        p.PrintStmt(*b++);
      } while (b != body->body_end() and not isa<OMPSectionDirective>(*b));
      if (b == body->body_end())
        b--;
    } else {
      p.PrintStmt(
          cast<CapturedStmt>(cast<OMPSectionDirective>(*b)->getAssociatedStmt())
              ->getCapturedStmt());
    }
    rso << ";";
    // LAST PRIVATE
    if (num_sec == std::distance(body->body_begin(), body->body_end())) {
      for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
        if (dsi.isLastPrivate(getOMPDirective(), param)) {
          rso << "*((struct " << argStruct_ << "*)" << cl_arg
              << ")->__kstar__fp_" << param->getName();
          rso << " = " << param->getName() << ";\n";
        }
      }
    }
    rso << "break;\n";
  }
  rso << "}\n";
  rso << "}\n";
  return genDeclareArgStruct(argStruct_) +
         genForOutlinedFunction(newFunction_, argStruct_, 1, rso.str(), false);
}
