#ifndef KSTAR_CODEGEN_STRUCTINIT_H
#define KSTAR_CODEGEN_STRUCTINIT_H

#include <string>
#include <unordered_map>
#include <vector>

#include <llvm/Support/raw_ostream.h>

namespace CodeGen {

class StructInit {

public:
  StructInit(const std::string &I, const std::string &N, bool inline_ = false,
             const std::string &T = std::string()
  );

  void add(const std::string &Type, const std::string &Name);
  void add_memcopy(const std::string &field, const std::string &src,
                   const std::string &len);
  void add_depend(const std::string &Type, const std::string &Name);
  std::string getaddr() const;
  std::string get(std::string const &attr) const;
  void setDynalloc(std::string const &dynalloc);
  std::string getType() const;
  std::string str() const;

private:
  std::vector<std::pair<std::string, std::string>> Members;
  // field name -> (src, len)
  std::unordered_map<std::string, std::pair<std::string, std::string>> Memcopy;
  std::vector<std::pair<std::string, std::string>> DependMembers;
  std::string Typename;
  std::string Name;
  std::string Template;
  std::string Dynalloc;
  bool Inline;
};

} // end namespace CodeGen

#endif // KSTAR_CODEGEN_STRUCTINIT_H
