#include "Kaapi.h"
#include "KaapiPragma.h"

using namespace clang;

RuntimeDirective *Kaapi::createDirective(OMPExecutableDirective *S,
                                         ASTContext &C,
                                         DataSharingInfo &DSI) {
#define GENERATE(PRAGMA)                                                       \
  if (isa<OMP##PRAGMA##Directive>(S)) {                                        \
    return new Kaapi##PRAGMA(cast<OMP##PRAGMA##Directive>(S), C, DSI);         \
  }
#include <pragma.def>
  assert(!"Unsupported OpenMP executable directive!");
  return nullptr;
}

std::string Kaapi::EmitBeginCritical(std::string const &Name, bool Unused) {
  return "komp_atomic_lock(&__kstar__mutex__" + Name + ");";
}

std::string Kaapi::EmitEndCritical(std::string const &Name, bool Unused) {
  return "komp_atomic_unlock(&__kstar__mutex__" + Name + ");";
}

std::string Kaapi::EmitBarrier() {
  return "komp_barrier(" + EmitSelfTeamCall() + ");";
}

std::string Kaapi::EmitTaskwait() {
  return "komp_sched_sync(" + EmitSelfThreadCall() + ");";
}

std::string Kaapi::EmitSingleCond() {
  return "komp_single(" + EmitSelfTeamCall() + ")";
}

std::string Kaapi::EmitBeginCopyprivate(const std::string &Data) {
  return "komp_single_copy_start(" + EmitSelfTeamCall() + ")";
}

std::string Kaapi::EmitEndCopyprivate(const std::string &Data) {
  return "komp_single_copy_end(" + EmitSelfTeamCall() + ", " + Data + ")";
}

std::string Kaapi::EmitCopyprivateBarrier() {
  return EmitBarrier();
}

std::string Kaapi::EmitMasterCond() {
  return "komp_master(" + EmitSelfTeamCall() + ")";
}

std::string Kaapi::EmitBeginOrdered() {
  return "komp_ordered_start()";
}

std::string Kaapi::EmitEndOrdered() {
  return "komp_ordered_end()";
}

std::string Kaapi::EmitBeginTaskgroup() {
  return "komp_taskgroup_begin()";
}

std::string Kaapi::EmitEndTaskgroup() {
  return "komp_taskgroup_end()";
}

std::string Kaapi::EmitFlush() {
  return "komp_mem_barrier();";
}

std::string Kaapi::EmitScheduleType(OpenMPScheduleClauseKind &Kind) {
  switch (Kind) {
  case OMPC_SCHEDULE_static:
    return "KOMP_FOREACH_SCHED_STATIC";
  case OMPC_SCHEDULE_adaptive:
  case OMPC_SCHEDULE_auto:
    return "KOMP_FOREACH_SCHED_ADAPTIVE";
  case OMPC_SCHEDULE_dynamic:
  case OMPC_SCHEDULE_guided:
    return "KOMP_FOREACH_SCHED_STEAL";
  case OMPC_SCHEDULE_runtime:
    return "KOMP_FOREACH_SCHED_DEFAULT";
  case OMPC_SCHEDULE_unknown:
    assert(!"Unknown OpenMP schedule kind!");
    break;
  }
  return "KOMP_FOREACH_SCHED_DEFAULT";
}

std::string Kaapi::EmitDependType(OpenMPDependClauseKind &Kind,
                                  bool IsCPlusPlus) {
  switch (Kind) {
  case OMPC_DEPEND_in:
    return "KOMP_ACCESS_MODE_R";
  case OMPC_DEPEND_out:
    return "KOMP_ACCESS_MODE_W";
  case OMPC_DEPEND_inout:
    return "KOMP_ACCESS_MODE_RW";
  case OMPC_DEPEND_cw:
    return "KOMP_ACCESS_MODE_ICW";
  case OMPC_DEPEND_commute:
    return "KOMP_ACCESS_MODE_ICW";
  case OMPC_DEPEND_sink:
  case OMPC_DEPEND_source:
    assert(!"Unsupported OpenMP depend kind!");
    break;
  case OMPC_DEPEND_unknown:
    assert(!"Unknown OpenMP depend kind!");
  }
  return "";
}

std::string Kaapi::EmitMapType(OpenMPMapClauseKind &Kind) {
  return "";
}

std::string Kaapi::EmitProcBind(OpenMPProcBindClauseKind &Kind) {
  switch (Kind) {
  case OMPC_PROC_BIND_master:
    return "komp_proc_bind_master";
  case OMPC_PROC_BIND_spread:
    return "komp_proc_bind_spread";
  case OMPC_PROC_BIND_close:
    return "komp_proc_bind_close";
  default:
    break;
  }
  return "komp_proc_bind_default";
}

std::string Kaapi::EmitAlloc(std::string const &Size) {
  return "komp_data_push(" + EmitSelfThreadCall() + ", " + Size + ")";
}

std::string Kaapi::EmitFree(std::string const &Name) {
  return "";
}

std::string Kaapi::EmitForInit(const std::string &Begin, const std::string &End,
                               const std::string &Chunk,
                               const std::string &Pargrain,
                               const std::string &Schedule,
                               const std::string &Ordered) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  std::string Attr = "__kstar__attr__";

  rso << "\tkomp_foreach_attr_t " << Attr << ";\n";
  rso << "\tkomp_foreach_attr_init( &" << Attr << ");\n";
  if (Chunk != "0") {
    // TODO grains may be adapted with knowledge about number of arithmetic
    // operation
    rso << "\tkomp_foreach_attr_set_grains(&" << Attr << ", " << Chunk
        << ", " << Pargrain << ");\n";
  }
  // Handle schedule clause
  rso << "komp_foreach_attr_set_policy(&" << Attr << ", " << Schedule
      << ");\n";

  return rso.str() + "if(komp_loop_policy_start_ull(komp_self_thread(), &" +
         Attr + ", 1, 0, " + End + ", 1, &" + Begin + ", &" + End + "))";
}

std::string Kaapi::EmitForNext(const std::string &Len, const std::string &Chunk,
                               const std::string &Schedule,
                               const std::string &Ordered,
                               const std::string &Begin,
                               const std::string &End) {
  std::string Args = EmitSelfThreadCall() + ", &" + Begin + ", &" + End;
  return "komp_loop_worknext_ull(" + Args + ") != 0";
}

std::string Kaapi::EmitForEnd(const std::string &Nowait) {
  return "komp_loop_workfini(" + EmitSelfThreadCall() + ");";
}

std::string Kaapi::EmitParallelRegion(const std::string &,
                                      const std::string &NumThreads,
                                      OpenMPProcBindClauseKind &ProcBind,
                                      const std::string &Func,
                                      const std::string &Arg) {
  std::string Args = NumThreads + ", " + EmitProcBind(ProcBind) + ", "
    + Func + ", " + Arg;
  return "komp_fork_call(" + Args + ");";
}

QualType Kaapi::getScheduleType(ASTContext &Context) {
  EnumDecl *ED =
    EnumDecl::Create(Context, Context.getTranslationUnitDecl(),
                     SourceLocation(), SourceLocation(),
                     &Context.Idents.get(""), nullptr, false, false, false);

  TypedefDecl *TD =
    Context.buildImplicitTypedef(Context.getEnumType(ED),
                                 "komp_foreach_attr_policy_t");

  return Context.getTypedefType(TD, QualType());
}

/**/
std::string Kaapi::genTaskSignature(ASTContext &Context,
                                         std::string funcName,
                                         std::string templateArgs) {
  FunctionDecl *FD = CreateWrapperFunction(Context, funcName);
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (!templateArgs.empty())
    rso << "template " << templateArgs;
  FD->print(rso);

  return rso.str();
}

std::string Kaapi::genRegister(Expr const *b, VarDecl const *,
                                    DataSharingInfo &, OMPExecutableDirective *,
                                    const int &bufInd, ASTContext &,
                                    bool alloc) {
  return "";
}

FunctionDecl *
Kaapi::CreateWrapperFunction(ASTContext &Context,
                                  const std::string &Name) {
  // Create function declaration.
  QualType PtrVoidTy = Context.getPointerType(Context.VoidTy);

  SmallVector<QualType, 1> FnArgTypes;
  FnArgTypes.push_back(PtrVoidTy);

  QualType FnTy =
    Context.getFunctionType(Context.VoidTy, FnArgTypes,
                                 FunctionProtoType::ExtProtoInfo());
  TypeSourceInfo *FnTI =
    Context.getTrivialTypeSourceInfo(FnTy, SourceLocation());

  FunctionDecl *FD =
    FunctionDecl::Create(Context, Context.getTranslationUnitDecl(),
                         SourceLocation(), SourceLocation(),
                         &Context.Idents.get(Name), FnTy, FnTI,
                         SC_Static, false, true, false);

  // Create function argument.
  TypeSourceInfo *PtrVoidTI =
    Context.getTrivialTypeSourceInfo(PtrVoidTy, SourceLocation());

  ParmVarDecl *Arg =
    ParmVarDecl::Create(Context, FD, SourceLocation(), SourceLocation(),
                        &Context.Idents.get("kstar_params"),
                        PtrVoidTy, PtrVoidTI, SC_None, 0);

  // Set function arguments.
  ParmVarDecl *Params[] = {Arg};
  FD->setParams(Params);

  return FD;
}
/**/

std::string Kaapi::EmitSelfThreadCall() const {
  return "komp_self_thread()";
}

std::string Kaapi::EmitSelfTeamCall() const {
  return "komp_self_team()";
}

std::string Kaapi::EmitDeclMutex(const std::string &Name) const {
  return "komp_mutex_t __kstar__mutex__" + Name + ";";
}

std::string Kaapi::EmitInitMutex(const std::string &Name) const {
  return "komp_atomic_initlock(&__kstar__mutex__" + Name + ");";
}

std::string Kaapi::EmitDestroyMutex(const std::string &Name) const {
  return "komp_atomic_destroylock(&__kstar__mutex__" + Name + ");";
}

std::string Kaapi::EmitConstructor(ASTContext &Context,
                                   const std::set<std::string> &MutexNames) const {
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (MutexNames.size() > 1 ||
      (MutexNames.size() == 1 && *MutexNames.begin() != "")) {
    // Create function declaration.
    QualType FnTy =
      Context.getFunctionType(Context.VoidTy, SmallVector<QualType, 0>(),
                              FunctionProtoType::ExtProtoInfo());
    TypeSourceInfo *FnTI =
      Context.getTrivialTypeSourceInfo(FnTy, SourceLocation());

    FunctionDecl *FD =
      FunctionDecl::Create(Context, Context.getTranslationUnitDecl(),
                           SourceLocation(), SourceLocation(),
                           &Context.Idents.get("runtim_abi_constructor"),
                           FnTy, FnTI, SC_Static, false, true, false);

    rso << "\n__attribute__ ((constructor))\n";
    FD->print(rso);

    rso << "{";
    for (const std::string &Name : MutexNames) {
      if (!Name.empty())
        rso << EmitInitMutex(Name);
    }
    rso << "}";
  }

  return rso.str();
}

std::string Kaapi::EmitDestructor(ASTContext &Context,
                                  const std::set<std::string> &MutexNames) const {
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (MutexNames.size() > 1 ||
      (MutexNames.size() == 1 && *MutexNames.begin() != "")) {
    // Create function declaration.
    QualType FnTy =
      Context.getFunctionType(Context.VoidTy, SmallVector<QualType, 0>(),
                              FunctionProtoType::ExtProtoInfo());
    TypeSourceInfo *FnTI =
      Context.getTrivialTypeSourceInfo(FnTy, SourceLocation());

    FunctionDecl *FD =
      FunctionDecl::Create(Context, Context.getTranslationUnitDecl(),
                           SourceLocation(), SourceLocation(),
                           &Context.Idents.get("runtim_abi_destructor"),
                           FnTy, FnTI, SC_Static, false, true, false);

    rso << "\n__attribute__ ((destructor))\n";
    FD->print(rso);

    rso << "{";
    for (const std::string &Name : MutexNames) {
      if (!Name.empty())
        rso << EmitDestroyMutex(Name);
    }
    rso << "}";
  }

  return rso.str();
}
