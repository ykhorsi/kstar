#include "analyses/FullSourceRange.h"

#include <clang/Basic/SourceManager.h>

using namespace clang;

FullSourceRange::FullSourceRange(Stmt const *S, Rewriter *R)
    : result(S->getSourceRange()), rewriter_(R) {
  TraverseStmt(const_cast<Stmt *>(S));
}

FullSourceRange::FullSourceRange(Decl const *S, Rewriter *R)
    : result(S->getSourceRange()), rewriter_(R) {
  TraverseDecl(const_cast<Decl *>(S));
}

bool FullSourceRange::VisitStmt(Stmt *S) {
  SourceRange SR = S->getSourceRange();
  if (SR.isValid())
    setEndLocation(SR.getEnd());

  if (isa<OMPExecutableDirective>(S)) {
    findBeginOfOMPDirective(SR);
    if (SR.isValid())
      setBeginLocation(SR.getBegin());
  }

  return true;
}

bool FullSourceRange::VisitDecl(Decl *D) {
  SourceRange SR = D->getSourceRange();
  if (SR.isValid())
    setEndLocation(SR.getEnd());

  if (isa<OMPThreadPrivateDecl>(D)) {
    findBeginOfOMPDirective(SR);
    findEndOfOMPThreadPrivateDecl(SR);
    if (SR.isValid()) {
      setBeginLocation(SR.getBegin());
      setEndLocation(SR.getEnd());
    }
  }
  return true;
}

void FullSourceRange::setBeginLocation(const SourceLocation &LHS) {
  SourceLocation RHS = result.getBegin();
  if (rewriter_->getSourceMgr().isBeforeInTranslationUnit(LHS, RHS))
    result.setBegin(LHS);
}

void FullSourceRange::setEndLocation(const SourceLocation &RHS) {
  SourceLocation LHS = result.getEnd();
  if (rewriter_->getSourceMgr().isBeforeInTranslationUnit(LHS, RHS))
    result.setEnd(RHS);
}

void FullSourceRange::findBeginOfOMPDirective(SourceRange &SR) {
  /* Find beginning of '#pragma omp [directive]' because the start location
   * of an OpenMP directive doesn't include '#pragma'. */
  int offset = -1;
  while (true) {
    SourceLocation L = SR.getBegin().getLocWithOffset(offset);
    std::string tok =
        std::string(rewriter_->getSourceMgr().getCharacterData(L), 1);
    if (tok == "#") {
      SR.setBegin(L);
      break;
    }
    offset--;
  }
}

void FullSourceRange::findEndOfOMPThreadPrivateDecl(SourceRange &SR) {
  int offset = 1;
  while (true) {
    SourceLocation L = SR.getEnd().getLocWithOffset(offset);
    std::string tok =
        std::string(rewriter_->getSourceMgr().getCharacterData(L), 1);
    if (tok == ")") {
      SR.setEnd(L);
      break;
    }
    offset++;
  }
}
