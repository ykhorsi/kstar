#ifndef KSTAR_GENUTILS_H
#define KSTAR_GENUTILS_H

#include <llvm/Support/raw_ostream.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/Decl.h>
#include <clang/Lex/Lexer.h>
#include <clang/Lex/PPCallbacks.h>
#include <clang/Lex/Preprocessor.h>
#include <clang/Rewrite/Frontend/Rewriters.h>
#include <clang/Rewrite/Core/Rewriter.h>

#include <string>

#include "analyses/DSA.h"

namespace GenUtils {
/*
 * Get declaration for the iterate variable of a For Loop
 */
clang::VarDecl *getIterDecl(clang::ForStmt *);

/// \brief Return the number of collapsed loops for loop directives.
unsigned getCollapsedNumber(clang::OMPExecutableDirective *);

/*
 * Return the adresse of the VarDecl in the current pragma scope
 * (not in outlined function)
 */
std::string getAddrVar(clang::VarDecl const *, DataSharingInfo &,
                       clang::OMPExecutableDirective *);
/*
 * Return the variable as it is requiered in the current pragma depending
 * on DataSharing information
 */
std::string getVar(clang::VarDecl const *, DataSharingInfo &,
                   clang::OMPExecutableDirective *, bool isLastPrivate = false);

/// \brief Return the downcasted type to be used as parameter.
clang::QualType getDowncastedType(clang::VarDecl const *VD,
                                  DataSharingInfo &DSI,
                                  clang::OMPExecutableDirective *S,
                                  clang::ASTContext &Context,
                                  bool isLastPrivate = false);
/*
 * Return downcasted type with variable name as for declaration
 * Downcast is because array may need to be downcast to pointer to be pass
 * as parameters or in structures
 */
std::string asParamDownType(clang::VarDecl const *, DataSharingInfo &,
                            clang::OMPExecutableDirective *,
                            clang::ASTContext &, bool varName = true,
                            bool isLastPrivate = false);
/*
 * Same as asParamDownType but variable name doesn't apprear (for casting..)
 */
std::string asCastDownType(clang::VarDecl const *VD, DataSharingInfo &,
                           clang::OMPExecutableDirective *, clang::ASTContext &,
                           bool isLastPrivate = false);
/*
 * Return declaration for the given Variable declaration.
 * Same as asParamDownType but with real type
 */
std::string asParamType(clang::VarDecl const *VD, DataSharingInfo &,
                        clang::OMPExecutableDirective *, clang::ASTContext &,
                        bool varName = true);
/*
 * Same as asParamType but variable name doesn't appear
 */
std::string asCastType(clang::VarDecl const *VD, DataSharingInfo &,
                       clang::OMPExecutableDirective *, clang::ASTContext &);

/// \brief Return a variable declaration from an expr.
///
/// \return This method returns nullptr when the variable is not found.
const clang::VarDecl *getVarDeclFromExpr(clang::Expr *E);

/*
 * Get element type from type, ptr or array
 */
clang::Type const *getRealElementType(clang::QualType const &);
/*
 * Check if the variable is pointer Type
 */
bool isPointerType(clang::VarDecl const *);
/*
 * Hacky way to replace SourceRange as it is buggy in clang
 */
void ReplaceSrcRange(clang::Rewriter *, clang::SourceRange &,
                     std::string const &);
/*
 * Get the function declaration that contains the given Statement
 */
clang::FunctionDecl const *getTopFunctionDecl(clang::Stmt const *,
                                              clang::ASTContext &);

/// \brief Get the function template decl of a stmt.
///
/// This method returns the function template declaration of the given
/// stmt if exists, otherwise nullptr is returned.
clang::FunctionTemplateDecl *getTopFunctionTemplateDecl(clang::Stmt const *S,
                                                        clang::ASTContext &C);

/// \brief Get the class template decl of a stmt.
///
/// This method returns the class template declaration of the given
/// stmt if exists, otherwise nullptr is returned.
clang::ClassTemplateDecl *getTopClassTemplateDecl(clang::Stmt const *S,
                                                  clang::ASTContext &C);

/*
 * Get the top declaration that contains the given Statement (upper than
 * class)
 */
clang::Decl const *getTopDecl(clang::Stmt const *, clang::ASTContext &);

/*
 * get declarations for all variables declare in the collapsed for construct
 */
std::string getForDecls(clang::Stmt *, clang::ASTContext &ctx, int depth);

/*
 * Get type for current class
 */
clang::QualType getThisType(clang::OMPExecutableDirective *S,
                            clang::ASTContext &C,
                            DataSharingInfo &dsi);

/*
 * Get a unique task identifier with file and line number.
 */
std::string getTaskName(clang::OMPExecutableDirective *ompd,
                        clang::ASTContext &ctx);

/*
 * Return TRUE when the given declaration is a local struct declaration.
 */
bool isLocalStructDecl(clang::VarDecl const *D);

/*
 * Return TRUE when the given declaration is located inside a CXX method.
 */
clang::CXXMethodDecl const *getTopCXXMethodDecl(clang::Stmt const *S,
                                                clang::ASTContext &C);

/// \brief Print a struct definition to a string.
const std::string RecordDeclToStr(clang::RecordDecl const *RD,
                                  const clang::PrintingPolicy &Policy);

const std::string ExprToStr(clang::Expr const *E);

/// \brief Return captured variables of the given OpenMP pragma.
const std::vector<clang::VarDecl const *>
getCapturedVars(clang::OMPExecutableDirective *OMPD);

/// \brief Return the code located in the given SourceRange object
/// as string.
const std::string SourceRangeToString(const clang::SourceRange &SR,
                                      clang::ASTContext &C);

/// \brief Return true if the variable declaration is referenced in this
/// threadprivate pragma.
bool IsThreadPrivateVarDecl(clang::OMPThreadPrivateDecl *OMPD,
                            clang::VarDecl *VD);
};

#endif
