#include "RuntimePragma.h"
#include "Printer.h"

using namespace clang;

RuntimeCritical::RuntimeCritical(OMPCriticalDirective *t, ASTContext &c,
                                 DataSharingInfo &dsi)
    : RuntimeScopeDirective(t, c, dsi) {}

std::string RuntimeCritical::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << getRuntime()->EmitBeginCritical("", getContext().getLangOpts().CPlusPlus11);
  KStarStmtPrinter P(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective(),
                     true, dsi.isInner(getOMPDirective()));
  P.PrintStmt(captured_->getCapturedStmt());
  rso << getRuntime()->EmitEndCritical("", getContext().getLangOpts().CPlusPlus11);

  return rso.str();
}
