#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    int a = 10;
    int b = 2*a;
    #pragma omp parallel
    #pragma omp for lastprivate(a) firstprivate(b)
      for (int i=0; i<b; ++i)
      {
        a = i;
      }
    assert( a == b - 1 );
    return 0;
}
