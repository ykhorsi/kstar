#Welcome to the Inria KSTAR Action website

##Introduction

The Inria KSTAR Action hosts the development of the Klang-Omp compiler and the Kastors benchmark suite.

##Klang-Omp
Klang-Omp is a C and C++ source-to-source OpenMP compiler based on LLVM framework and on Intel’s Clang-OMP front-end. It translates OpenMP directives into calls to task-based runtime system APIs. Klang-Omp currently targets the StarPU runtime and the Kaapi runtime.
The compiler supports independent tasks as defined by the OpenMP specification revision 3.1 as well as dependent tasks introduced with OpenMP 4. The two runtime systems currently targeted by Klang-Omp, [StarPU](http://starpu.gforge.inria.fr/) and [Kaapi](http://kaapi.gforge.inria.fr), both natively implement dependent tasks. Klang-Omp also supports long established OpenMP constructs such as parallel loops and sections.


###Download
The Klang-Omp compiler is is available for download [here](https://gforge.inria.fr/frs/?group_id=5411)


##Kastors Benchmarks
The KSTAR project also supports the development of the [KASTORS benchmark suite](http://kastors.gforge.inria.fr). The KASTORS suite is a series of popular computing kernels that we have been ported on the OpenMP 4 depend task model, to explore the performance of compiler/runtime system pairs.

###Download
The KASTORS Benchmark Suite is is available for download [here](https://gforge.inria.fr/frs/?group_id=6074)