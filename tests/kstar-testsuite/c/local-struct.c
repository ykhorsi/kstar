#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

struct bar {
    int a;
    int b;
};

static void
global_struct()
{
    struct bar foo = { .a = 12, .b = 16 };
    int res = 0;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((foo.a - foo.b) != -4);
    }
    if (res != 0)
        abort();
}

static void
local_struct_1()
{
    struct foo {
        int a;
        int b;
    };
    struct foo bar = { .a = 12, .b = 16 };
    int res = 0;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar.a - bar.b) != -4);
    }
    if (res != 0)
        abort();
}

static void
local_struct_2()
{
    typedef int INT;
    struct foo {
        INT a;
        INT b;
    };
    struct foo *bar = (struct foo *)malloc(sizeof(*bar));
    bar->a = 12;
    bar->b = 16;
    int res = 0;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar->a - bar->b) != -4);
    }
    if (res != 0)
        abort();
}

static void
local_struct_3()
{
    struct foo {
        int a;
        int b;
    };
    struct foo bar[2];
    bar[0].a = 12;
    bar[0].b = 16;
    int res = 0;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar[0].a - bar[0].b) != -4);
    }
    if (res != 0)
        abort();
}

static void
local_struct_4()
{
    struct foo {
        int a;
        int b;
    };
    int res = 0;
    #pragma omp parallel
    {
        struct foo bar[2];
        bar[0].a = 12;
        bar[0].b = 16;

        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar[0].a - bar[0].b) != -4);
    }
    if (res != 0)
        abort();
}

static void
local_struct_5()
{
    typedef struct {
        int a;
        int b;
    } foo;
    int res = 0;
    #pragma omp parallel
    {
        foo bar[2];
        bar[0].a = 12;
        bar[0].b = 16;

        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((bar[0].a - bar[0].b) != -4);
    }
    if (res != 0)
        abort();
}

int main()
{
    global_struct();
    local_struct_1();
    local_struct_2();
    local_struct_3();
    local_struct_4();
    local_struct_5();
    return 0;
}
