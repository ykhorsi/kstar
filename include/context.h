#ifndef KSTAR_CONTEXT_H
#define KSTAR_CONTEXT_H

#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Rewrite/Frontend/Rewriters.h>
#include <llvm/Support/raw_ostream.h>

struct ActionContext {
  ActionContext()
      : buffer(), buffer2(), resultStream(buffer), resultStream2(buffer2) {}
  std::string buffer;
  std::string buffer2;
  clang::Rewriter *rewriter_;
  llvm::raw_string_ostream resultStream;
  llvm::raw_string_ostream resultStream2;
  std::string expandedFile;
  std::set<std::string> IncludeFiles;
};

extern ActionContext Acontext;

#endif
