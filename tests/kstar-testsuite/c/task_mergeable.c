#include <stdio.h>
#include <stdlib.h>

int main()
{
  int x = 2;

#pragma omp parallel
#pragma omp single
  // Example 16.11c, correct usage of mergeable clause.
#pragma omp task shared(x) mergeable
  {
    x++;
  }
#pragma omp taskwait

  if (x != 3)
    abort();

  // Example 16.12c, incorrect usage, two different values can be returned
  // for x, it depends on the decisions taken by the implementation.
  x = 2;
#pragma omp parallel
#pragma omp single
#pragma omp task mergeable
  {
    x++;
  }
#pragma omp taskwait

  if (x != 2 && x != 3)
    abort();

  return 0;
}
