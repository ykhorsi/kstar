#include <stdlib.h>
#include <omp.h>

static int c;
int d;

int main()
{
    static int res = 0;
    int a = 12, b = 16;
    #pragma omp parallel
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((a - b) != -4);
        c = a + b;
        d = a - b;
    }

    if (res != 0 || c != 28 || d != -4)
      abort();
    return 0;
}
