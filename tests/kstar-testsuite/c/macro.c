#include <stdlib.h>
#include <assert.h>
#include <omp.h>

#define OPENMP_CODE_IN_MACRO                                                  \
{                                                                             \
    res |= (omp_get_thread_num() >= omp_get_num_threads());                   \
    res |= ((a - b) != -4);                                                   \
}

static void
test_macro()
{
    int a = 12, b = 16, res = 0;
    #pragma omp parallel firstprivate(a, b)
    OPENMP_CODE_IN_MACRO
}

static void
test_ifdef()
{
    int a = 12, b = 16, res = 0;

#ifdef USE_OPENMP
    #pragma omp parallel firstprivate(a, b)
#endif
    {
        res |= (omp_get_thread_num() >= omp_get_num_threads());
        res |= ((a - b) != -4);
    }
}

int main()
{
    test_macro();
    test_ifdef();
    return 0;
}
