bin_PROGRAMS=kstar-omp
kstardir=$(bindir)
kstar_SCRIPTS=\
	kstar \
	kstar++ \
	kstar-starpu \
	kstar-starpu++ \
	kstar-kaapi \
	kstar-kaapi++

kstar_omp_CPPFLAGS=$(AM_CPPFLAGS) $(LLVM_CONFIG_CPPFLAGS) -I$(top_srcdir)/include
kstar_omp_CXXFLAGS=$(AM_CXXFLAGS) 
kstar_omp_LDFLAGS=$(LLVM_CONFIG_LDFLAGS)
kstar_omp_LDADD= $(LIBS) \
	-lclangFrontendTool\
	-lclangFrontend\
	-lclangDriver\
	-lclangSerialization\
	-lclangCodeGen\
	-lclangParse\
	-lclangSema\
	-lclangStaticAnalyzerFrontend\
	-lclangStaticAnalyzerCheckers\
	-lclangStaticAnalyzerCore\
	-lclangAnalysis\
	-lclangARCMigrate\
	-lclangRewriteFrontend\
	-lclangRewrite\
	-lclangEdit\
	-lclangAST\
	-lclangLex\
	-lclangBasic\
	-lclangTooling\
	$(LLVM_CONFIG_LIBS)

kstar_omp_SOURCES=\
	GenUtils.cpp\
	MultiDimArray.cpp\
	Printer.cpp\
	analyses/DSA.cpp\
	analyses/FullSourceRange.cpp\
	analyses/OMPGraph.cpp\
	analyses/VariablesVisitor.cpp\
	codegen/StructInit.cpp\
	codegen/StructExpand.cpp\
	codegen/Util.cpp\
	frontend/FrontendActions.cpp \
	frontend/PrintPreprocessedOutput.cpp \
	frontend/InclusionRewriter.cpp\
	kaapi/Kaapi.cpp\
	kaapi/KaapiFor.cpp\
	kaapi/KaapiTarget.cpp\
	kaapi/KaapiTask.cpp\
	rewriter.cpp\
	runtime/OpenMPRuntimeFactory.cpp\
	runtime/RuntimeBarrier.cpp\
	runtime/RuntimeCritical.cpp\
	runtime/RuntimeDirective.cpp\
	runtime/RuntimeFlush.cpp\
	runtime/RuntimeFor.cpp\
	runtime/RuntimeLoop.cpp\
	runtime/RuntimeMaster.cpp\
	runtime/RuntimeOrdered.cpp\
	runtime/RuntimeParallel.cpp\
	runtime/RuntimeParallelFor.cpp\
	runtime/RuntimeParallelSections.cpp\
	runtime/RuntimeScopeDirective.cpp\
	runtime/RuntimeSections.cpp\
	runtime/RuntimeSingle.cpp\
	runtime/RuntimeTarget.cpp\
	runtime/RuntimeTask.cpp\
	runtime/RuntimeTaskgroup.cpp\
	runtime/RuntimeTaskwait.cpp\
	runtime/RuntimeTaskyield.cpp\
	runtime/RuntimeVisitor.cpp\
	starpu/StarPU.cpp\
	starpu/StarPUTarget.cpp\
	starpu/StarPUTask.cpp

dist_noinst_DATA=\
	pragma.def

do_subst = $(SED) -e 's,[@]libdir[@],$(libdir),g' \
			   -e 's,[@]bindir[@],$(bindir),g' \
			   -e 's,[@]abs_top_builddir[@],$(abs_top_builddir),g' \
			   -e 's,[@]abs_top_srcdir[@],$(abs_top_srcdir),g' \
			   -e 's,[@]kstarincludedir[@],$(kstarincludedir),g' \
			   -e 's,[@]pkglibdir[@],$(pkglibdir),g' \
			   -e 's,[@]LLVM_VERSION[@],$(LLVM_VERSION),g' \
			   -e 's,[@]LLVM_PREFIX[@],$(LLVM_PREFIX),g' \
			   -e 's,[@]KSTAR_LLVM[@],$(KSTAR_LLVM),g' \
			   -e 's,[@]CCNATIVE[@],$(CCNATIVE),g' \
			   -e 's,[@]CXXNATIVE[@],$(CXXNATIVE),g' \
			   -e 's,[@]CCNATIVE_OMP_FLAG[@],$(CCNATIVE_OMP_FLAG),g' \
			   -e 's|[@]STARPU_PKG_CONFIG_PATH[@]|$(STARPU_PKG_CONFIG_PATH)|g' \
			   -e 's|[@]KAAPI_PKG_CONFIG_PATH[@]|$(KAAPI_PKG_CONFIG_PATH)|g' \
			   -e 's|[@]STARPU_INTREE_PKG_CONFIG_PATH[@]|$(STARPU_INTREE_PKG_CONFIG_PATH)|g' \
			   -e 's|[@]KAAPI_INTREE_PKG_CONFIG_PATH[@]|$(KAAPI_INTREE_PKG_CONFIG_PATH)|g'

EXTRA_DIST=kstar.in \
	kstar++.in \
	kstar-starpu \
	kstar-starpu++ \
	kstar-kaapi \
	kstar-kaapi++

kstar: kstar.in Makefile
	$(AM_V_GEN)$(do_subst) < $(srcdir)/kstar.in > kstar
	$(AM_V_at)chmod +x $@
kstar++: kstar++.in Makefile
	$(AM_V_GEN)$(do_subst) < $(srcdir)/kstar++.in > kstar++
	$(AM_V_at)chmod +x $@
MOSTLYCLEANFILES=kstar kstar++

