#include <clang/Frontend/CompilerInstance.h>

#include "frontend/FrontendActions.h"
#include "frontend/Utils.h"

#include "context.h"

using namespace clang;

namespace kstar {

//===----------------------------------------------------------------------===//
// Preprocessor Actions
//===----------------------------------------------------------------------===//

void PrintPreprocessedAction::ExecuteAction() {
  CompilerInstance &CI = getCompilerInstance();

  kstar::DoPrintPreprocessedInput(CI.getPreprocessor(), &Acontext.resultStream,
                                  CI.getPreprocessorOutputOpts());
}

void IncludeExpanderAction::ExecuteAction() {
  CompilerInstance &CI = getCompilerInstance();

  kstar::DoRewriteIncludesInInput(CI.getPreprocessor(), &Acontext.resultStream2,
                                  CI.getPreprocessorOutputOpts());
}

} // end kstar namespace
