#include <stdio.h>
#include <stdlib.h>

static int compare_double(const double f1, const double f2)
{
    double precision = 0.00001;
    return ((f1 - precision < f2) && (f1 + precision) > f2);
}

int main()
{
    int n = 100;
    float a[n];
    for(int i=0; i<n; i++)
        a[i] = 1;

    float scale_factor = 3.4;

#pragma omp parallel
#pragma omp single
    for(int i = 0; i < n; i += 5)
#pragma omp task depend(inout: a[i:5]) shared(a)
        for(int j = i; j<i + 5; j++)
            a[j] = a[j] * scale_factor;

    if (!compare_double(a[9], 3.4))
        abort();

    return 0;
}
