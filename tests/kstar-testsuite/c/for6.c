#include <stdio.h>
#include <assert.h>

int main()
{
    int a = 4;
    int b = 8;
    #pragma omp parallel for firstprivate(a,b) lastprivate(a) num_threads(1)
      for (int i=a|b; i>0; --i)
      {
        if (a==4)
          a = i;
      }
    assert( a == 12 );
    return 0;
}
