#include "OpenMPRuntimeFactory.h"
#include "config.h"

#ifdef USE_KAAPI
#include "Kaapi.h"
#endif
#ifdef USE_STARPU
#include "StarPU.h"
#endif

OpenMPRuntime *OpenMPRuntimeFactory::Instance = nullptr;

void OpenMPRuntimeFactory::build(enum OpenMPRuntimeType T) {
  if (Instance)
    return;

  switch (T) {
#ifdef USE_STARPU
  case STARPU:
    Instance = new StarPU();
    break;
#endif
#ifdef USE_KAAPI
  case KAAPI:
    Instance = new Kaapi();
    break;
#endif
  default:
    assert("Unknown runtime!\n");
  }
}
