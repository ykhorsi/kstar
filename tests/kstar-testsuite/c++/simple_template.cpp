#include <iostream>

#include <stdlib.h>
#include <omp.h>

static int compare_double(const double f1, const double f2)
{
    double precision = 0.00001;
    return ((f1 - precision < f2) && (f1 + precision) > f2);
}

template<class T>
double get_average(T array[], int n)
{
    T sum = T();

#pragma omp parallel for
    for (int i = 0; i < n; ++i) {
#pragma omp critical
        sum += array[i];
    }
    return double(sum) / n;
}

int main()
{
    int array_int[5] = {100, 200, 400, 500, 1000};
    float array_float[3] = { 1.55f, 5.44f, 12.36f};

    if (get_average(array_int, 5) != 440)
        abort();
    if (!compare_double(get_average(array_float, 3), 6.45))
        abort();

    return 0;
}
