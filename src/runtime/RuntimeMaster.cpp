#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeMaster::RuntimeMaster(OMPMasterDirective *OMPD, ASTContext &C,
                             DataSharingInfo &DSI)
    : RuntimeScopeDirective(OMPD, C, DSI) {}

/// \brief Replace MASTER construct.
///
/// #pragma omp master
///     block
///
/// is replaced by:
///
/// if (master_cond() == TRUE)
///     block
std::string RuntimeMaster::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << "if (" << getRuntime()->EmitMasterCond() << ")";
  rso << "{";
  KStarStmtPrinter P(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective(),
                     true, dsi.isInner(getOMPDirective()));
  P.PrintStmt(captured_->getCapturedStmt());
  rso << "}";

  return rso.str();
}
