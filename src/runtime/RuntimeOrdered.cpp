#include "RuntimePragma.h"
#include "Printer.h"

using namespace clang;

RuntimeOrdered::RuntimeOrdered(OMPOrderedDirective *OMPD, ASTContext &C,
                               DataSharingInfo &DSI)
    : RuntimeScopeDirective(OMPD, C, DSI) {}

/// \brief Replace ORDERED construct.
std::string RuntimeOrdered::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << getRuntime()->EmitBeginOrdered() << ";";
  KStarStmtPrinter P(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective(),
                     true, dsi.isInner(getOMPDirective()));
  P.PrintStmt(captured_->getCapturedStmt());
  rso << getRuntime()->EmitEndOrdered() << ";";

  return rso.str();
}
