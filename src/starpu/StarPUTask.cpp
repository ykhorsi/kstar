#include <llvm/Support/raw_ostream.h>
#include "MultiDimArray.h"

#include "codegen/Util.h"
#include "StarPUPragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

StarPUTask::StarPUTask(OMPTaskDirective *t, ASTContext &c, DataSharingInfo &dsi)
    : RuntimeTask(t, c, dsi) {}

std::string StarPUTask::genBeforeFunction(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genDeclareArgStruct(argStruct_);

  if (!GenUtils::getTopCXXMethodDecl(getOMPDirective()->getAssociatedStmt(),
                                     getContext())) {
    // Only generate a signature when the given OpenMP pragma is
    // not located inside a CXX method.
    rso << getRuntime()->genTaskSignature(getContext(), newFunction_,
                                        addInjectedTemplateArgs(true)) << ";\n";
  }

  return rso.str();
}

std::string StarPUTask::genAfterFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << getRuntime()->genTaskSignature(getContext(), newFunction_,
                                      addInjectedTemplateArgs(true)) << "{\n";
  CodeGen::StructExpand argStruct = expandArgStruct("kstar_params", argStruct_);
#ifdef USE_STARPU_MANAGED_DATA
  argStruct.except(by_ref);
#endif /* USE_STARPU_MANAGED_DATA */
  rso << argStruct.str();

#ifdef USE_STARPU_MANAGED_DATA
  int j = 0;
  for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (VD == nullptr)
      continue;
    if (std::find(by_ref.begin(), by_ref.end(), VD->getName()) ==
        by_ref.end()) // Not in by_ref
      continue;

    QualType Ty = VD->getType().getNonReferenceType().withoutLocalFastQualifiers();
    std::string castType = GenUtils::asCastType(VD, dsi, getOMPDirective(), getContext());
    std::string paramType = GenUtils::asParamType(VD, dsi, getOMPDirective(), getContext());

    if (Ty->isPointerType()) {
      rso << VD->getType().getAsString() << "_kstar_ptr_"  << VD->getNameAsString()
        << " = (" << VD->getType().getAsString() << ")"
        << "STARPU_VECTOR_GET_PTR(kstar_buffers[" << j++ << "]);\n";

      rso << paramType
        << " = (" << castType << ")" << "&" << "_kstar_ptr_"  << VD->getNameAsString() << ";\n";
    } else {
      rso << paramType
        << " = (" << castType << ")"
        << "STARPU_VECTOR_GET_PTR(kstar_buffers[" << j++ << "]);\n";
    }
  }
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective()), false, false, by_ref);
#else /* USE_STARPU_MANAGED_DATA */
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective());
#endif /* USE_STARPU_MANAGED_DATA */

  p.PrintStmt(captured_->getCapturedStmt());
  rso << freeFirstPrivateVLA();
  rso << argStruct.free();
  rso << "}\n";

  return rso.str();
}

std::string StarPUTask::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string cond = "1";
  if (ifClause_) {
    OMPExecutableDirective * const ompd = getOMPDirective();
    OMPExecutableDirective * Parent = dsi.getParent(ompd);
    while (Parent && isa<OMPTaskgroupDirective>(Parent)) {
      Parent = dsi.getParent(Parent);
    }
    std::string dummy;
    llvm::raw_string_ostream dummys(dummy);
    if (Parent) {
      KStarStmtPrinter p(dummys, getContext().getPrintingPolicy(), q, &dsi, Parent);
      p.PrintExpr(ifClause_);
    } else {
      StmtPrinter p(dummys, 0, getContext().getPrintingPolicy(), 0);
      p.PrintExpr(ifClause_);
    }
    cond = dummys.str();
  }

  CodeGen::StructInit Task("starpu_omp_task_region_attr",
                           "_kstar_task_region_" + std::to_string(getId()));
  if (where_ == OMPC_WHERE_cpu || where_ == OMPC_WHERE_all) {
    Task.add("cl.cpu_funcs[0]", newFunction_ + addInjectedTemplateArgs());
  }
  if (where_ == OMPC_WHERE_gpu || where_ == OMPC_WHERE_all) {
    Task.add("cl.cuda_funcs[0]", newFunction_ + addInjectedTemplateArgs());
    Task.add("cl.cuda_flags[0]", "STARPU_CUDA_ASYNC");
  }
  Task.add("if_clause", cond);
  if (untiedCond_)
    Task.add("untied_clause", "1");
  if (finalCond_)
    Task.add("final_clause", GenUtils::ExprToStr(finalCond_));
  if (mergeCond_)
    Task.add("mergeable_clause", "1");
  Task.add("cl.name",
           "\"" + GenUtils::getTaskName(getOMPDirective(), getContext()) + "\"");

  if (priority_) {
    std::string prio;

    // Get the priority number of this task.
    llvm::APSInt Result;
    if (priority_->EvaluateAsInt(Result, getContext())) {
      prio = Result.toString(10);
    } else {
       std::string dummy;
      llvm::raw_string_ostream dummys(dummy);
      StmtPrinter P(dummys, 0, getContext().getPrintingPolicy());
      P.PrintExpr(priority_);
      prio = dummys.str();
    }

    Task.add("priority", prio);
  }

  bool IsCPlusPlus = getContext().getLangOpts().CPlusPlus;
  int nbrBuffer_ = 0;
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    for (unsigned i = 0; i < inDeps_[param].size(); i++) {
      OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_in;
      Task.add("cl.modes[" + std::to_string(nbrBuffer_++) + "]",
               getRuntime()->EmitDependType(Kind, IsCPlusPlus));
    }
    for (unsigned i = 0; i < outDeps_[param].size(); i++) {
      OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_inout;
      Task.add("cl.modes[" + std::to_string(nbrBuffer_++) + "]",
               getRuntime()->EmitDependType(Kind, IsCPlusPlus));
    }
    for (unsigned i = 0; i < cwDeps_[param].size(); i++) {
      OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_cw;
      Task.add("cl.modes[" + std::to_string(nbrBuffer_++) + "]",
               getRuntime()->EmitDependType(Kind, IsCPlusPlus));
    }
    for (unsigned i = 0; i < commuteDeps_[param].size(); i++) {
      OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_commute;
      Task.add("cl.modes[" + std::to_string(nbrBuffer_++) + "]",
               getRuntime()->EmitDependType(Kind, IsCPlusPlus));
    }
  }

  if (nbrBuffer_) {
    rso << "starpu_data_handle_t handles[" << nbrBuffer_ << "];";

    Task.add("cl.nbuffers", std::to_string(nbrBuffer_));
    Task.add("handles", "&handles[0]");
  }

  if (!dsi.emptyOrOnlyPrivate(getOMPDirective())) {
    // FILL CL_ARG
    CodeGen::StructInit argStruct = genFillArgStruct(
        "_kstar_args_" + std::to_string(getId()), argStruct_);

    std::string argStructName = argStruct_ + addInjectedTemplateArgs();
    argStruct.setDynalloc(
        CodeGen::GenMalloc(CodeGen::GenSizeof("struct " + argStructName)));
    rso << argStruct.str();

    Task.add("cl_arg_free", "1");
    Task.add("cl_arg_size", CodeGen::GenSizeof("struct " + argStructName));
    Task.add("cl_arg", argStruct.getaddr());

    int bufInd = 0;
    // FILL BUFFERS
    for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective()))
      for (Expr const *b : getDepends(param)) {
        rso << getRuntime()->genRegister(b, param, dsi, getOMPDirective(), bufInd,
                                       getContext());
        bufInd++;
      }
    assert(bufInd == nbrBuffer_);
  }

  rso << Task.str();
  rso << "starpu_omp_task_region(" << Task.getaddr() << ");";
  return rso.str();
}
