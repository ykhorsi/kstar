#ifndef KSTAR_RUNTIMEPRAGMA_H
#define KSTAR_RUNTIMEPRAGMA_H

#include <clang/AST/StmtOpenMP.h>
#include <clang/AST/Decl.h>
#include <clang/AST/ASTContext.h>
#include <clang/Basic/OpenMPKinds.h>

#include <map>
#include <string>
#include <set>
#include <iterator>

#include "RuntimeDirective.h"
#include "RuntimeScopeDirective.h"

class RuntimeSections : public RuntimeScopeDirective {
public:
  RuntimeSections(clang::OMPSectionsDirective *t, clang::ASTContext &c,
                  DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
  virtual std::string genBeforeFunction(std::queue<std::string> &q);
};

class RuntimeParallelSections : public RuntimeScopeDirective {
public:
  RuntimeParallelSections(clang::OMPParallelSectionsDirective *t,
                          clang::ASTContext &c, DataSharingInfo &dsi);
  virtual std::string genBeforeFunction(std::queue<std::string> &q);
  virtual std::string genReplacePragma(std::queue<std::string> &q);

protected:
  std::string wrapperFunction_;
};

class RuntimeLoop : public RuntimeScopeDirective {
public:
  RuntimeLoop(clang::OMPExecutableDirective *S, clang::ASTContext &C,
              DataSharingInfo &DSI);
  virtual std::string genBeforeFunction(std::queue<std::string> &q);
  virtual std::string genReplacePragma(std::queue<std::string> &q);

protected:
  int collapse_;
  std::string wrapperStructType_;
  std::string wrapperFunction_;

  /// \brief Generate outlined for body.
  std::string genForBody(std::queue<std::string> &,
                         std::string const &argStruct);

  /// \brief Generate the length of the compacted iteration space from a for
  /// statement and check if upper bound is greater than the lower one.
  std::string genUpperBound(clang::ForStmt *For, bool parent = false) const;

  /// \brief Generate the length of the compacted iteration space from a for
  /// statement.
  std::string genUpperBoundNoCheck(clang::ForStmt *For, bool parent = false,
                                   bool is_outer = false) const;

  // \brief Generate the step for a ForLoop for a given depth.
  std::string genStep(clang::ForStmt *For, bool parent = false) const;

  /// \brief Generate the last value of the iteration space from a for
  /// statement.
  std::string genEnd(clang::ForStmt *For, bool parent = false,
                     bool is_outer = false) const;

  /// \brief Generate the first value of the iteration space from a for
  /// statement.
  std::string genBegin(clang::ForStmt *For, bool parent = false,
                       bool is_outer = false) const;

  /// \brief Return the for statement attached to the given statement.
  clang::ForStmt *getForStmt(clang::Stmt *S, int Depth) const;

  /// \brief Return the init expression of the given for statement.
  clang::Expr *getPreCond(clang::ForStmt *For) const;

  /// \brief Return the iteration variable of the given for statement.
  const clang::VarDecl *getIterationVariable(clang::ForStmt *For) const;

  /// \brief Return true if the for statement will increase on each iteration.
  bool isIncr(clang::ForStmt *For) const;
};

class RuntimeParallelFor : public RuntimeLoop {
public:
  RuntimeParallelFor(clang::OMPParallelForDirective *S, clang::ASTContext &C,
                     DataSharingInfo &DSI);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeFor : public RuntimeLoop {
public:
  RuntimeFor(clang::OMPForDirective *S, clang::ASTContext &C,
             DataSharingInfo &DSI);

  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeCritical : public RuntimeScopeDirective {
public:
  RuntimeCritical(clang::OMPCriticalDirective *t, clang::ASTContext &c,
                  DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeOrdered : public RuntimeScopeDirective {
public:
  RuntimeOrdered(clang::OMPOrderedDirective *OMPD, clang::ASTContext &C,
                 DataSharingInfo &DSI);

  /// \brief Replace ORDERED construct.
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeTaskgroup : public RuntimeScopeDirective {
public:
  RuntimeTaskgroup(clang::OMPTaskgroupDirective *OMPD, clang::ASTContext &C,
                   DataSharingInfo &DSI);

  // \brief Replace TASKGROUP construct.
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeMaster : public RuntimeScopeDirective {
public:
  RuntimeMaster(clang::OMPMasterDirective *OMPD, clang::ASTContext &C,
                DataSharingInfo &DSI);

  /// \brief Replace MASTER construct.
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeSingle : public RuntimeScopeDirective {
public:
  RuntimeSingle(clang::OMPSingleDirective *OMPD, clang::ASTContext &C,
                DataSharingInfo &DSI);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
  virtual std::string genBeforeFunction(std::queue<std::string> &q);
};

class RuntimeParallel : public RuntimeScopeDirective {
public:
  RuntimeParallel(clang::OMPParallelDirective *t, clang::ASTContext &c,
                  DataSharingInfo &dsi);
  virtual std::string genBeforeFunction(std::queue<std::string> &q);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
  virtual std::string genFunctionBody(std::queue<std::string> &q,
                                      std::string const &);
  std::string genAfterFunction(std::queue<std::string> &q) override;
};

class RuntimeTarget : public RuntimeScopeDirective {
public:
  RuntimeTarget(clang::OMPTargetDirective *t, clang::ASTContext &c,
                DataSharingInfo &dsi);

protected:
  std::vector<std::string> by_ref;
};

class RuntimeTask : public RuntimeScopeDirective {
public:
  RuntimeTask(clang::OMPTaskDirective *t, clang::ASTContext &c,
              DataSharingInfo &dsi);

  /// \brief Return the number of depends of a variable declaration.
  ///
  /// This method counts the number of in, out and cw depends.
  const int getNumberOfDepends(clang::VarDecl const *VD);

  /// \brief Return all depends of a variable declaration.
  const std::vector<clang::Expr const *> getDepends(clang::VarDecl const *VD);

  /// \brief Generate code for freeing allocated memory for arrays.
  std::string freeFirstPrivateVLA() const;

protected:
  std::vector<std::string> by_ref;
};

class RuntimeTaskwait : public RuntimeDirective {
public:
  RuntimeTaskwait(clang::OMPTaskwaitDirective *t, clang::ASTContext &c,
                  DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeBarrier : public RuntimeDirective {
public:
  RuntimeBarrier(clang::OMPBarrierDirective *t, clang::ASTContext &c,
                 DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeFlush : public RuntimeDirective {
public:
  RuntimeFlush(clang::OMPFlushDirective *t, clang::ASTContext &c,
               DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};

class RuntimeTaskyield : public RuntimeDirective {
public:
  RuntimeTaskyield(clang::OMPTaskyieldDirective *t, clang::ASTContext &c,
                   DataSharingInfo &dsi);
  virtual std::string genReplacePragma(std::queue<std::string> &q);
};
#endif
