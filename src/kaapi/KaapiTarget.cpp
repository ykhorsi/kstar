#include "KaapiPragma.h"

using namespace clang;

KaapiTarget::KaapiTarget(OMPTargetDirective *t, ASTContext &c,
                         DataSharingInfo &dsi)
    : RuntimeTarget(t, c, dsi) {
  assert(!"KAAPI doesn't support targets yet!");
}

std::string KaapiTarget::genBeforeFunction(std::queue<std::string> &) {
  return "";
}

std::string KaapiTarget::genAfterFunction(std::queue<std::string> &) {
  return "";
}

std::string KaapiTarget::genReplacePragma(std::queue<std::string> &) {
  return "";
}
