#!/bin/bash

# Check for lcov support.
which lcov &> /dev/null
if [ $? != 0 ]; then
  echo "lcov needs to be installed!"
  exit 1
fi

# Clean previous build.
if [ -d lcov_report ]; then
  rm -rf lcov_report
fi
make distclean

# Enable code coverage.
CXXFLAGS="-g -O0 --coverage" ./configure

# Build source.
make -j$(nproc)

# Initialize lcov.
lcov --base-directory . --directory . --zerocounters -q

# Run unit tests.
make -j$(nproc) check

# Generate lcov report.
cd src
lcov --base-directory . --directory . -c -o kstar_lcov.info
lcov --remove kstar_lcov.info "/usr*" "third_party/*" -o kstar_lcov.info

# Generate HTML report.
genhtml -o ../lcov_report -t "KSTAR code coverage analysis" --num-spaces 4 kstar_lcov.info
