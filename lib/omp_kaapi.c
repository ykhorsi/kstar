/*
** xkaapi
**
**
** Copyright 2009,2010,2011,2012, 2017 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
**
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
**
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
**
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
**
*/
#include "libkomp.h"

komp_mutex_t __kstar__mutex__;

static komp_mutex_t theatomic;

__attribute__((constructor))
static void runtime_abi_constructor(void)
{
    komp_init();
    komp_atomic_initlock(&__kstar__mutex__);
    komp_atomic_initlock(&theatomic);
}

__attribute__((destructor))
static void runtime_abi_destructor(void)
{
    komp_finalize();
    komp_atomic_destroylock(&__kstar__mutex__);
    komp_atomic_destroylock(&theatomic);
}


/*
*/
void GOMP_atomic_start (void)
{
  komp_atomic_lock(&theatomic);
}

/*
*/
void GOMP_atomic_end (void)
{
  komp_atomic_unlock(&theatomic);
}


// Make sur library is linked even when as-needed is used (to call constructor/destructor)
int __kstar_extern_link_forced(void)
{
  return 0;
}

void __kmpc_begin(void *__unused_loc, int32_t __unused_flags)
{
}

void __kmpc_end(void *__unused_loc)
{
}
