#ifndef KSTAR_STARPU_H
#define KSTAR_STARPU_H

#include <starpu.h>

/* Check if StarPU supports commute dependence type. */
#if (STARPU_MAJOR_VERSION >= 1) && (STARPU_MINOR_VERSION >= 2)
  #define STARPU_COMMUTE_IF_SUPPORTED STARPU_COMMUTE
#else
  #define STARPU_COMMUTE_IF_SUPPORTED STARPU_NONE
#endif

extern int __kstar_extern_link_forced;
static int *__force_link = &__kstar_extern_link_forced;

#ifdef __cplusplus
#include <new>

extern "C" {
#endif
  void *kstar_starpu_malloc(size_t size);
#ifdef __cplusplus
}
#endif

#endif /* __KSTAR_STARPU_H */
