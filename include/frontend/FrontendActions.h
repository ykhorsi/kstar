#ifndef KSTAR_FRONTEND_ACTIONS_H
#define KSTAR_FRONTEND_ACTIONS_H

#include <clang/Frontend/FrontendActions.h>

namespace kstar {

class PrintPreprocessedAction : public clang::PreprocessorFrontendAction {
protected:
  void ExecuteAction() override;
};

class IncludeExpanderAction : public clang::PreprocessorFrontendAction {
protected:
  void ExecuteAction() override;
};

} // end kstar namespace

#endif
