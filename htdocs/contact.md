#Contact

* Website [http://kstar.gforge.inria.fr/](http://kstar.gforge.inria.fr)
* Klang-Omp compiler source code [here](https://gforge.inria.fr/frs/?group_id=5411)
* Mailing-List [kstar-public@lists.gforge.inria.fr](mailto://kstar-public@lists.gforge.inria.fr)
