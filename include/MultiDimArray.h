#ifndef KSTAR_MULTIDIMARRAY_H
#define KSTAR_MULTIDIMARRAY_H

#include <clang/AST/Decl.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/Expr.h>

#include <deque>

class MultiDimArray {
public:
  MultiDimArray(clang::Expr const *);

  int getDim();
  /*
   *Returns the array base
   *Warning : this is a DeclRefExpr (clang-s implementation)
   */
  clang::DeclRefExpr const *getBase();
  clang::Expr const *getLowerBound(int dim);
  clang::Expr const *getLen(int dim);
  /*Returns the LDA, or null if ConstantArrayType*/
  clang::Expr const *getLDA();
  llvm::APInt &getConstantLDA();

private:
  int dimension_ = 0;
  clang::DeclRefExpr const *base_ = nullptr;
  std::deque<clang::Expr const *> lowerBounds_;
  std::deque<clang::Expr const *> lengths_;
  clang::Expr const *lda_ = nullptr;
  llvm::APInt constantLda_;
};

#endif
