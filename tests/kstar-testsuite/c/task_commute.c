#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * This test is only supported by KSTAR/StarPU.
 *
 * The commute dependence type is not described by the official spec of
 * OpenMP 4, it is an extension added in our compiler.
 */

void foo(int *a)
{
    *a = 12;
}

void bar(int *a)
{
    sleep(1);
    *a = 14;
}

void foobar(int *a)
{
    sleep(1);
    *a = 14 + *a;
}

int main()
{
    int a = 8;
#pragma omp parallel
#pragma omp single
    {
#pragma omp task depend(out: a) shared(a) taskname("mytaskname")
        foo(&a);
#pragma omp task depend(out: a) shared(a)
        bar(&a);
#pragma omp task depend(in: a) shared(a)
        if (a != 14)
            abort();
    }

#pragma omp parallel
#pragma omp single
    {
#pragma omp task depend(out: a) shared(a) priority(15)
        foo(&a);
#pragma omp task depend(commute: a) shared(a)
        foobar(&a);
#pragma omp task depend(in: a) shared(a)
        if (a != 26)
            abort();
    }

    return 0;
}
