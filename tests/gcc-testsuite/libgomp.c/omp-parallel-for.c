extern void abort (void);

int main()
{
  int i, a;

  a = 30;

#pragma omp parallel num_threads (2)
#pragma omp for firstprivate (a) lastprivate (a) \
	schedule(static)
  for (i = 0; i < 10; i++)
    a = a + i;

  /* The thread that owns the last iteration will have computed
     30 + 5 + 6 + 7 + 8 + 9 = 65.  */
  if (a != 65)
    abort ();

  return 0;
}
