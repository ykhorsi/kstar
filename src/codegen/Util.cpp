#include "codegen/Util.h"

#include <clang/AST/DeclTemplate.h>
#include <clang/AST/Expr.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang;

namespace CodeGen {

const std::string GenSizeof(const std::string &ptr) {
  return "sizeof(" + ptr + ")";
}

const std::string GenMemcpy(const std::string &dest, const std::string &src,
                            const std::string &n) {
  return "memcpy(" + dest + ", " + src + ", " + n + ")";
}

const std::string GenMalloc(const std::string &size) {
  return "malloc(" + size + ")";
}

const std::string genTSCSpec(const ThreadStorageClassSpecifier TSCS) {
  switch (TSCS) {
  case TSCS_unspecified:
    break;
  case TSCS___thread:
    return "__thread";
    break;
  case TSCS__Thread_local:
    return "_Thread_local";
    break;
  case TSCS_thread_local:
    return "thread_local";
    break;
  }
  return std::string();
}

static void printTemplateParameterList(const TemplateParameterList *List,
                                       const PrintingPolicy &Policy,
                                       llvm::raw_string_ostream &rso,
                                       bool withArgKind) {
  const llvm::ArrayRef<const NamedDecl *> &Params = List->asArray();

  for (unsigned i = 0; i < Params.size(); i++) {
    const NamedDecl *ND = Params.vec()[i];
    if (i)
      rso << ", ";
    if (withArgKind)
      rso << "class ";
    ND->printQualifiedName(rso, Policy);
  }
}

const std::string
genInjectedTemplateArgs(const llvm::ArrayRef<TemplateArgument> &Args,
                        const PrintingPolicy &Policy, bool withArgKind) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (Args.empty())
    return rso.str();

  rso << " < ";
  for (unsigned i = 0; i < Args.size(); i++) {
    const TemplateArgument Arg = Args.vec()[i];
    if (i)
      rso << ", ";

    switch (Arg.getKind()) {
    case TemplateArgument::ArgKind::Type:
      if (withArgKind)
        rso << "class ";
      break;
    case TemplateArgument::ArgKind::Template: {
      TemplateName TN = Arg.getAsTemplate();

      if (withArgKind) {
        rso << "template ";
        rso << " < ";
        switch (TN.getKind()) {
        case TemplateName::NameKind::Template: {
          TemplateDecl *TD = TN.getAsTemplateDecl();
          printTemplateParameterList(TD->getTemplateParameters(), Policy, rso,
                                     withArgKind);
          break;
        }
        default:
          assert(!"Unsupported template name kind!\n");
          break;
        }
        rso << " > class ";
      }
      break;
    }
    case TemplateArgument::ArgKind::Expression:
      if (withArgKind) {
        Arg.getAsExpr()->getType().print(rso, Policy);
        rso << " ";
      }
      break;
    default:
      assert(!"Unsupported template argument kind!\n");
      break;
    }
    Arg.print(Policy, rso);
  }
  rso << " > ";

  return rso.str();
}
};
