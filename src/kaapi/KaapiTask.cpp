#include <llvm/Support/raw_ostream.h>

#include "GenUtils.h"
#include "KaapiPragma.h"
#include "MultiDimArray.h"
#include "Printer.h"
#include "Kaapi.h"

using namespace clang;

MapFormat KaapiTask::formatRegistered_;

KaapiTask::KaapiTask(OMPTaskDirective *S, ASTContext &C, DataSharingInfo &DSI)
    : RuntimeTask(S, C, DSI) {
  argStruct_ = "argstruct_" + std::to_string(getId());

  for (VarDecl const *VD : dsi.getCapturedVars(S, true)) {
    if (!VD)
      continue;

    const Type *Ty = GenUtils::getRealElementType(VD->getType());
    if (isBuiltinFormatType(Ty))
      continue;

    SourceRange SR = getOMPDirective()->getSourceRange();
    if (!isBuiltinFormatType(Ty)) {
      auto found = formatRegistered_.find(Ty);
      if (found == formatRegistered_.end()) {
        std::pair<Type const *, SourceLocation> p(Ty, SR.getBegin());
        formatRegistered_.insert(p);
      } else {
        SourceLocation oldSL = found->second;
        SourceLocation curSL = SR.getBegin();
        SourceManager &SM = getContext().getSourceManager();

        while (oldSL.isMacroID())
          oldSL = SM.getImmediateMacroCallerLoc(oldSL);
        while (curSL.isMacroID())
          curSL = SM.getImmediateMacroCallerLoc(curSL);

        if (SM.isWrittenInSameFile(curSL, oldSL)) {
          if (SM.getPresumedLineNumber(curSL) <
              SM.getPresumedLineNumber(oldSL))
            found->second = curSL;
        }
      }
    }
  }
}

std::string KaapiTask::genDeclareExtraArgStruct() {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << "komp_dataenv_icv_t* _k_icv;\n";
  for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective())) {
    int NumDepends = getNumberOfDepends(VD);
    for (int i = 0; i < NumDepends; i++) {
      rso << "komp_access_t ";
      rso << VD->getNameAsString() + std::to_string(i);
      rso << ";\n";
    }
  }

  return rso.str();
}

std::string KaapiTask::genBeforeFunction(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // Emit all user formats.
  SourceManager &SM = getContext().getSourceManager();
  SourceRange SR = getOMPDirective()->getSourceRange();
  unsigned int curLN = SM.getPresumedLineNumber(SR.getBegin());

  for (auto it = formatRegistered_.begin(), itEnd = formatRegistered_.end();
       it != itEnd; ++it) {
    if (SM.getPresumedLineNumber(it->second) == curLN)
      rso << EmitUserFormat(it->first);
  }

  rso << "\n/***********************************/\n";
  rso << "/* Begin arg struct for \"" << std::to_string(getId()) << "\" */\n";
  rso << "/***********************************/\n";
  rso << genDeclareArgStruct(argStruct_);

  if (!GenUtils::getTopCXXMethodDecl(getOMPDirective()->getAssociatedStmt(),
                                     getContext())) {
    // Create record declarations for this function.
    RecordDecl *KompTask = getContext().buildImplicitRecord("komp_task_t");
    RecordDecl *KompThread = getContext().buildImplicitRecord("komp_thread_t");

    // Create function declaration.
    QualType KompTaskTy =
      getContext().getPointerType(getContext().getRecordType(KompTask));
    QualType KompThreadTy =
      getContext().getPointerType(getContext().getRecordType(KompThread));

    SmallVector<QualType, 2> FnArgTypes;
    FnArgTypes.push_back(KompTaskTy);
    FnArgTypes.push_back(KompThreadTy);

    QualType FnTy =
      getContext().getFunctionType(getContext().VoidTy, FnArgTypes,
                                   FunctionProtoType::ExtProtoInfo());
    TypeSourceInfo *FnTI =
      getContext().getTrivialTypeSourceInfo(FnTy, SourceLocation());

    FunctionDecl *FD =
      FunctionDecl::Create(getContext(), getContext().getTranslationUnitDecl(),
                           SourceLocation(), SourceLocation(),
                           &getContext().Idents.get(newFunction_), FnTy, FnTI,
                           SC_Static, false, true, false);

    // Create function argument.
    TypeSourceInfo *KompThreadTI =
      getContext().getTrivialTypeSourceInfo(KompThreadTy, SourceLocation());
    TypeSourceInfo *KompTaskTI =
      getContext().getTrivialTypeSourceInfo(KompTaskTy, SourceLocation());

    ParmVarDecl *Arg1 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("_k_task"),
                        KompTaskTy, KompTaskTI, SC_None, 0);

    ParmVarDecl *Arg2 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("_k_thread"),
                        KompThreadTy, KompThreadTI, SC_None, 0);

    // Set function arguments.
    ParmVarDecl *Params[] = {Arg1, Arg2};
    FD->setParams(Params);

    // Create and print a template function declaration if needed.
    FunctionTemplateDecl *FTD = CreateFunctionTemplate(FD);
    if (FTD) {
      FTD->print(rso);
    } else {
      FD->print(rso);
    }

    rso << ";"; // end of function prototype
  }
  rso << "/***********************************/\n";
  rso << "/* End arg struct for \"" << argStruct_ << "\" */\n";
  rso << "/***********************************/\n\n";
  return rso.str();
}

std::string KaapiTask::genAfterFunction(std::queue<std::string> &q) {
  return EmitTask() + EmitTaskWrapper(q);
}

std::string KaapiTask::EmitUserFormat(Type const *Ty) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  std::string TypeFormatName = EmitUserFormatType(Ty);
  std::string typeName = Ty->getCanonicalTypeInternal().getAsString();
  std::string GetPointerFuncName    = "get" + TypeFormatName + "_pointer";
  std::string CstorFuncName         = TypeFormatName + "_cstor";
  std::string DstorFuncName         = TypeFormatName + "_dstor";
  std::string CstorCopyFuncName     = TypeFormatName + "_cstorcopy";
  std::string CopyFuncName          = TypeFormatName + "_copy";
  std::string AssignFuncName        = TypeFormatName + "_assign";
  std::string RegisterCStorFuncName = TypeFormatName + "_registerCstor";

  rso << "\n/***********************************/\n";
  rso << "/* Begin register format for type \"" << typeName << "\" */\n";
  rso << "/***********************************/\n";
  rso << "static struct komp_format_t *" << TypeFormatName << ";\n";
  rso << "static struct komp_format_t *" << GetPointerFuncName << "(void)\n{\n";
  rso << "\t return " << TypeFormatName << ";\n}\n";

  rso << "static void " << CstorFuncName << "(void *dest)\n{\n";
  rso << typeName << " tmp;\n";
  rso << "*(" << typeName << " *)dest = tmp;\n";
  rso << "}\n";

  rso << "static void " << DstorFuncName << "(void *dest)\n{\n";
  rso << typeName << " tmp;\n";
  rso << "*(" << typeName << " *)dest = tmp;\n";
  rso << "}\n";

  rso << "static void " << CstorCopyFuncName << "(void *dest,"
      << "const void *src)\n{\n";
  rso << "*(" << typeName << " *)dest = "
      << "*(" << typeName << " *)src;\n";
  rso << "}\n";

  rso << "static void " << CopyFuncName << "(void *dest,"
      << "const void *src)\n{\n";
  rso << "*(" << typeName << " *)dest = "
      << "*(" << typeName << " *)src;\n";
  rso << "}\n";

  rso << "static void " << AssignFuncName << "(void* dest, "
      << "const komp_memory_view_t* dview, const void* src, "
      << "const komp_memory_view_t* sview)\n{\n";
  rso << "*(" << typeName << " *)dest = "
      << "*(" << typeName << " *)src;\n";
  rso << "}\n";

  rso << "__attribute__ ((constructor)) static void "
      << RegisterCStorFuncName << "(void)\n{\n";
  rso << TypeFormatName << " = komp_format_allocate();\n";
  rso << "\tkomp_format_structregister(\n" << GetPointerFuncName << "(),\n"
      << "\"" << typeName << "\",\n"
      << "sizeof(" << typeName << "),\n"
      << "&" << CstorFuncName << ",\n"
      << "&" << DstorFuncName << ",\n"
      << "&" << CstorCopyFuncName << ",\n"
      << "&" << CopyFuncName << ",\n"
      << "&" << AssignFuncName << ");\n";
  rso << "}\n";

  rso << "/***********************************/\n";
  rso << "/* End register format for type \"" << typeName << "\" */\n";
  rso << "/***********************************/\n\n";
  return rso.str();
}

std::string KaapiTask::EmitTaskWrapper(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << newFunction_
      << "(struct komp_task_t *_k_task, komp_thread_t *_k_thread)"
      << "\n{\n";

  rso << "\tstruct " << argStruct_ << addInjectedTemplateArgs()
      << " *theargs = (struct " << argStruct_ << addInjectedTemplateArgs()
      << " *)*(void**)_k_task;\n";
  rso << "\tkomp_dataenv_icv_t* _k_save_icv = "
         "komp_swap_dataenv_icvs(_k_thread, theargs->_k_icv);\n";

  CodeGen::StructExpand argStruct = expandArgStruct("theargs", argStruct_);
  rso << argStruct.str();
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective());
  p.PrintStmt(captured_->getCapturedStmt());

  rso << "\tkomp_swap_dataenv_icvs(_k_thread, _k_save_icv);\n";
  rso << freeFirstPrivateVLA();
  rso << argStruct.free();

  rso << "}\n";
  return rso.str();
}

/* --------- genCallRuntime ---------- */
/* Create and push a task to kaapi*/
std::string KaapiTask::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << "\n\n/***********************************/\n";
  rso << "/* Begin task spawn */\n";
  rso << "/***********************************/\n";

  /* name of arguments variable */
  std::string varArgName(argStruct_ + "_arg");

  /* thread, task, and wrapper name */
  std::string threadName("thread_" + std::to_string(getId()));

  // get thread
  Kaapi *K = static_cast<Kaapi *>(getRuntime());
  rso << "\tkomp_thread_t *" << threadName << " = "
      << K->EmitSelfThreadCall() << ";\n";

  /* pushdata */
  CodeGen::StructInit argStruct = genFillArgStruct(varArgName, argStruct_);
  argStruct.setDynalloc("komp_data_push(" + threadName + ", " +
                        "sizeof(struct " + argStruct_ +
                        addInjectedTemplateArgs() + ")" + ")");
   
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (not param)
      continue;

    int NumDepends = getNumberOfDepends(param);
    std::string VarName = param->getNameAsString();
    for (int i = 0; i < NumDepends; i++) 
    {
      argStruct.add_depend( VarName + std::to_string(i) /* + ".data_toto"*/, argStruct.get(VarName));
    }
  }
  argStruct.add("_k_icv", "komp_inittask_dataenv_icvs(" + threadName + ", " +
                              std::to_string(mergeCond_) + ")");
  rso << argStruct.str();

  std::string cond = "1";
  if (ifClause_) {
    std::string dummy;
    llvm::raw_string_ostream dummys(dummy);
    StmtPrinter p(dummys, 0, getContext().getPrintingPolicy(), 0);
    p.PrintExpr(ifClause_);
    cond = dummys.str();
  }
  rso << "\tkomp_task_push(" << threadName << ", " << newFunction_
      << addInjectedTemplateArgs() << ", " << argStruct.getaddr() << ", "
      << cond << ");\n";

  rso << "/***********************************/\n";
  rso << "/* End task spawn */\n";
  rso << "/***********************************/\n\n";
  return rso.str();
}

std::string KaapiTask::EmitTask() {
  std::string str;
  llvm::raw_string_ostream rso(str);

  unsigned int nParam = 1; /* for ICVS */
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (dsi.isPrivate(getOMPDirective(), param))
      continue;
    nParam++;
  }

  for (auto &deps : inDeps_)
    nParam += deps.second.size();
  for (auto &deps : outDeps_)
    nParam += deps.second.size();
  for (auto &deps : cwDeps_)
    nParam += deps.second.size();
  for (auto &deps : commuteDeps_)
    nParam += deps.second.size();

  // type and wrapper names
  std::string TaskName = argStruct_;
  std::string TaskNameWithTpl = TaskName + addInjectedTemplateArgs();

  // variables and functions names
  std::string TaskFormatName         = TaskName       + "_format";
  std::string GetSizeFuncName        = TaskFormatName + "_get_size";
  std::string TaskCopyFuncName       = TaskFormatName + "_task_copy";
  std::string GetCountParamsFuncName = TaskFormatName + "_get_count_params";
  std::string GetModeParamFuncName   = TaskFormatName + "_get_mode_param";
  std::string GetAccessParamFuncName = TaskFormatName + "_get_access_param";
  std::string SetAccessParamFuncName = TaskFormatName + "_set_access_param";
  std::string GetFmtParamFuncName    = TaskFormatName + "_get_fmt_param";
  std::string GetViewParamFuncName   = TaskFormatName + "_get_view_param";
  std::string SetViewParamFuncName   = TaskFormatName + "_set_view_param";
  std::string ReductorFuncNameg      = TaskFormatName + "_reductor";
  std::string RedinitFuncName        = TaskFormatName + "_redinit";
  std::string CtorFuncName           = TaskFormatName + "_constructor";

  rso << "\n/***********************************/\n";
  rso << "/* Begin register task for \"" << TaskName << "\" */\n";
  rso << "/***********************************/\n";

  // get_sizeof
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static unsigned long " << GetSizeFuncName;
  rso << "(const struct komp_format_t *fmt,\n";
  rso << "\t const void *sp)\n{\n";
  if (nParam > 0)
    rso << "\treturn sizeof(struct " << TaskNameWithTpl << ");\n";
  else
    rso << "\treturn 0;\n";
  rso << "}\n";

  // get_count_params
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static unsigned long " << GetCountParamsFuncName
      << "(const struct komp_format_t *fmt,\n";
  rso << "\t const void *sp)\n{\n";
  rso << "\treturn " << nParam << ";\n";
  rso << "}\n";

  // task_copy
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << TaskCopyFuncName
      << "(const struct komp_format_t *fmt,\n";
  rso << "\tvoid *sp_dest, const void *sp)\n{\n";

  // sp src
  rso << "\tconst struct " << TaskNameWithTpl
      << " *arg = (const struct " << TaskNameWithTpl << " *)sp;\n";
  // sp_dest
  rso << "\tstruct " << TaskNameWithTpl << " *arg_dest = (struct "
      << TaskNameWithTpl << " *)sp_dest;\n";

  rso << "\t*arg_dest = *arg;\n";
  rso << "}\n";

  /*get_mode_param*/
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static komp_access_mode_t " << GetModeParamFuncName;
  rso << "(const struct komp_format_t* fmt, unsigned int i,"
      << "const void *sp)\n{\n";

  rso << "\tstatic komp_access_mode_t mode_param[] = {\n";
  rso << "\n\t\tKOMP_ACCESS_MODE_V";
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    rso << EmitTaskDependsType(param);
  }
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (dsi.isPrivate(getOMPDirective(), param))
      continue;
    rso << ",\n\t\tKOMP_ACCESS_MODE_V";
  }
  rso << "\n\t};\n";
  rso << "\treturn mode_param[i];\n";
  rso << "}\n";

  // get_access_param
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static komp_access_t *" << GetAccessParamFuncName;
  rso << "(const struct komp_format_t* fmt, unsigned int __kaapi_i,"
      << "const void* sp)\n{\n";
  rso << "\tstruct " << TaskNameWithTpl << " *arg = (struct "
      << TaskNameWithTpl << " *)sp;\n";
  rso << "\tswitch(__kaapi_i) {\n";
  int index = 1;
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    int idDep = 0;
    for (Expr const *b : getDepends(param)) {
      (void)b;
      rso << "\t\tcase " << index++ << ": ";
      rso << "return &arg->" << param->getName();
      rso << idDep++;
      rso << ";\n";
    }
  }
  rso << "\t\tdefault: break;\n";
  rso << "\t}\n";
  rso << "\treturn 0;\n";
  rso << "}\n";

  // set_access_param
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << SetAccessParamFuncName
      << "(const struct komp_format_t* fmt, unsigned int i, void* sp"
      << ",\n";
  rso << "\tconst komp_access_t* a)\n{\n";
  rso << "\tstruct " << TaskNameWithTpl << " *arg = (struct "
      << TaskNameWithTpl << " *)sp;\n";
  rso << "\tswitch(i) {\n";
  index = 1;
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    int idDep = 0;
    for (Expr const *b : getDepends(param)) {
      (void)b;
      rso << "\t\tcase " << index++ << ": ";
      rso << "arg->" << param->getName();
      rso << idDep++;
      rso << " = *a;\n";
      rso << "\t\t\tbreak;\n";
    }
  }
  rso << "\t\tdefault: break;\n";
  rso << "\t}\n";
  rso << "}\n";

  // get_fmt_param
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static const struct komp_format_t* " << GetFmtParamFuncName;
  rso << "(const struct komp_format_t* fmt, unsigned int i,"
      << "const void* sp)\n{\n";
  rso << "\tstruct " << TaskNameWithTpl << " *arg = (struct "
      << TaskNameWithTpl << " *)sp;\n";
  rso << "\tswitch(i) {\n";
  int i = 0;
  rso << "\t\tcase " << i++ << ": return komp_voidp_format;\n";
  for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective())) {
    if (!VD)
      continue;

    for (Expr const *b : getDepends(VD)) {
      (void)b;
      rso << "\t\tcase " << i++ << ": return " << EmitFormatType(VD)
          << ";\n";
    }
  }

  rso << "\t}\n";
  rso << "\treturn 0;\n";
  rso << "}\n";

  // get_view_param
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << GetViewParamFuncName;
  rso << "(const struct komp_format_t* fmt,"
      << "unsigned int __kaapi_i, const void* sp, komp_memory_view_t *view)"
      << "\n{\n";
  CodeGen::StructExpand argStruct = expandArgStruct("sp", TaskName);
  rso << argStruct.str();
  rso << "\tswitch(__kaapi_i) {\n";
  i = 1;
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    rso << EmitMemoryViewParams(param, i);
  }
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (not param or dsi.isPrivate(getOMPDirective(), param))
      continue;
    rso << "\t\tcase " << i++ << ": ";
    rso << "komp_memory_view_make1d(view, 0, 1, sizeof(";
    GenUtils::getRealElementType(param->getType())
        ->getCanonicalTypeInternal()
        .print(rso, getContext().getPrintingPolicy());
    rso << "));break;\n";
  }
  rso << "\t}\n";
  rso << argStruct.free();
  rso << "}\n";

  // set_view_param
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << SetViewParamFuncName
      << "(const struct komp_format_t* fmt, unsigned int i,"
      << "void* sp, "
      << "const komp_memory_view_t* view)\n{\n";
  rso << "}\n";

  // reducor
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << ReductorFuncNameg
      << "(const struct komp_format_t* fmt, unsigned int i,"
      << "void* sp, "
      << "const void* v)\n{\n";
  rso << "\tstruct " << TaskNameWithTpl << " *arg = (struct "
      << TaskNameWithTpl << " *)sp;\n";
  rso << "}\n";

  // redinit
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "static void " << RedinitFuncName
      << "(const struct komp_format_t* fmt, unsigned int i,"
      << "const void* sp, void* v)\n{\n";
  rso << "\tstruct " << TaskNameWithTpl << " *arg = (struct "
      << TaskNameWithTpl << " *)sp;\n";
  rso << "}\n";

  // constructor
  if (hasInjectedTemplateArgs())
    rso << "template " << addInjectedTemplateArgs(true);
  rso << "__attribute__ ((constructor)) static void " << CtorFuncName
      << "(void)\n{\n";
  rso << "\tkomp_format_taskregister_func(\n";
  // fmt
  rso << "\t\tkomp_format_allocate(),\n";
  // body
  rso << "\t\t" << newFunction_ << addInjectedTemplateArgs() << ",\n";
  // bodywh
  rso << "\t\t"
      << "0"
      << ",\n";
  // name
  rso << "\t\t"
      << "\"" << GenUtils::getTaskName(getOMPDirective(), getContext()) << "/"
      << newFunction_ << "\""
      << ",\n";
  // get_name
  rso << "\t\t"
      << "0"
      << ",\n";
  // size
  rso << "\t\t" << GetSizeFuncName << addInjectedTemplateArgs() << ",\n";
  // task_copy
  rso << "\t\t" << TaskCopyFuncName << addInjectedTemplateArgs() << ",\n";
  // get_count_param
  rso << "\t\t" << GetCountParamsFuncName << addInjectedTemplateArgs() << ",\n";
  // get_mode_param
  rso << "\t\t" << GetModeParamFuncName << addInjectedTemplateArgs() << ",\n";
  // get_data_param
  rso << "\t\t"
      << "0"
      << ",\n";
  // get_access_param
  rso << "\t\t" << GetAccessParamFuncName << addInjectedTemplateArgs()
      << ",\n";
  // set_access_param
  rso << "\t\t" << SetAccessParamFuncName << addInjectedTemplateArgs()
      << ",\n";
  // get_fmt_param
  rso << "\t\t" << GetFmtParamFuncName << addInjectedTemplateArgs() << ",\n";
  // get_view_param
  rso << "\t\t" << GetViewParamFuncName << addInjectedTemplateArgs() << ",\n";
  // set_view_param
  rso << "\t\t" << SetViewParamFuncName << addInjectedTemplateArgs() << ",\n";
  // reducor
  rso << "\t\t" << ReductorFuncNameg << addInjectedTemplateArgs() << ",\n";
  // redinit
  rso << "\t\t" << RedinitFuncName << addInjectedTemplateArgs() << ",\n";
  // splitter adaptative
  rso << "\t\t"
      << "0"
      << "\n";
  rso << "\t);\n";
  rso << "}\n";

  rso << "/***********************************/\n";
  rso << "/* End register task for \"" << TaskName << "\" */\n";
  rso << "/***********************************/\n\n";
  return rso.str();
}

std::string KaapiTask::EmitTaskDependsType(VarDecl const *VD) {
  bool IsCPlusPlus = getContext().getLangOpts().CPlusPlus;
  std::string str;
  llvm::raw_string_ostream rso(str);

  for (unsigned i = 0; i < inDeps_[VD].size(); i++) {
    OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_in;
    rso << ", " << getRuntime()->EmitDependType(Kind, IsCPlusPlus);
  }
  for (unsigned i = 0; i < outDeps_[VD].size(); i++) {
    OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_inout;
    rso << ", " << getRuntime()->EmitDependType(Kind, IsCPlusPlus);
  }
  for (unsigned i = 0; i < cwDeps_[VD].size(); i++) {
    OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_cw;
    rso << ", " << getRuntime()->EmitDependType(Kind, IsCPlusPlus);
  }
  for (unsigned i = 0; i < commuteDeps_[VD].size(); i++) {
    OpenMPDependClauseKind Kind = OpenMPDependClauseKind::OMPC_DEPEND_commute;
    rso << ", " << getRuntime()->EmitDependType(Kind, IsCPlusPlus);
  }
  return rso.str();
}


std::string KaapiTask::EmitMemoryViewParams(VarDecl const *VD, int &paramIdx) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  std::string dummy1;
  std::queue<std::string> dummyq;
  llvm::raw_string_ostream streamFalse(dummy1);
  KStarStmtPrinter printerFalse(streamFalse, getContext().getPrintingPolicy(),
                                dummyq, &dsi, getOMPDirective(), false);

  for (Expr const *b : getDepends(VD)) {
    rso << "\t\tcase " << paramIdx << ": ";
    if (isa<DeclRefExpr>(b))
      rso << "komp_memory_view_make1d(view, 0, 1, sizeof(";
    else if (isa<ArraySubscriptExpr>(b) || isa<OMPArraySectionExpr>(b)) {
      MultiDimArray MDA(b);

      std::string len1 = "1";
      if (MDA.getLen(0)) {
        streamFalse.str().clear();
        printerFalse.PrintExpr(const_cast<Expr *>(MDA.getLen(0)));
        /*Use str again to have the stream flushed*/
        len1 = streamFalse.str();
      }

      streamFalse.str().clear();
      printerFalse.PrintExpr(const_cast<Expr *>(MDA.getLowerBound(0)));
      std::string idx = streamFalse.str();

      if (MDA.getDim() == 1)
        rso << "komp_memory_view_make1d(view, ";
      else
        rso << "komp_memory_view_make2d(view, ";
      if (MDA.getDim() > 1) {
        std::string len2 = "1";
        if (MDA.getLen(1)) {
          streamFalse.str().clear();
          printerFalse.PrintExpr(const_cast<Expr *>(MDA.getLen(1)));
          len2 = streamFalse.str();
        }
        streamFalse.str().clear();
        printerFalse.PrintExpr(const_cast<Expr *>(MDA.getLowerBound(1)));
        std::string idx2 = streamFalse.str();

        std::string ldas;
        if (MDA.getLDA()) {
          streamFalse.str().clear();
          printerFalse.PrintExpr(const_cast<Expr *>(MDA.getLDA()));
          ldas = streamFalse.str();
        } else {
          ldas = std::to_string(MDA.getConstantLDA().getLimitedValue());
        }

        // offset
        rso << "(unsigned long)(" << idx << ")*(" << ldas
            << ") + (unsigned long)(" << idx2 << ")";
        // len 1
        rso << ", ";
        rso << len1;

        rso << ", ";
        // len 2
        rso << len2;
        rso << ", ";
        rso << ldas;
      } else {
        // offset
        rso << "(unsigned long)(" << idx << "), ";
        // size
        rso << len1;
      }
      rso << ", sizeof(";
    }
    GenUtils::getRealElementType(VD->getType())
        ->getCanonicalTypeInternal()
        .print(rso, getContext().getPrintingPolicy());
    rso << "));break;\n";
    paramIdx++;
  }
  return rso.str();
}

std::string KaapiTask::EmitFormatType(VarDecl const *VD) {
  const Type *Ty = GenUtils::getRealElementType(VD->getType());
  std::string TypeName;

  if (isBuiltinFormatType(Ty)) {
    TypeName = EmitBuiltinFormatType(Ty);
  } else {
    TypeName = EmitUserFormatType(Ty);
  }
  return TypeName;
}

std::string KaapiTask::EmitUserFormatType(Type const *Ty) {
  std::string UserType;
  if (!Ty)
    return UserType;

  UserType = Ty->getCanonicalTypeInternal().getAsString();
  replace(UserType.begin(), UserType.end(), '<', '_');
  replace(UserType.begin(), UserType.end(), '>', '_');
  replace(UserType.begin(), UserType.end(), ' ', '_');
  replace(UserType.begin(), UserType.end(), '[', '_');
  replace(UserType.begin(), UserType.end(), ']', '_');

  return "komp_" + UserType + "_format";
}

std::string KaapiTask::EmitBuiltinFormatType(const Type *Ty) {

  if (Ty->isCharType())
    return "komp_char_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::Int))
    return "komp_int_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::Void))
    return "komp_voidp_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::Long))
    return "komp_long_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::LongLong))
    return "komp_llong_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::UInt))
    return "komp_uint_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::ULong))
    return "komp_ulong_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::ULongLong))
    return "komp_ullong_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::Double))
    return "komp_dbl_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::LongDouble))
    return "komp_ldbl_format";
  else if (Ty->isSpecificBuiltinType(BuiltinType::Float))
    return "komp_flt_format";

  assert(!"Unsupported format builtin type!");
  return std::string();
}

bool KaapiTask::isBuiltinFormatType(const Type *Ty) {
  return (Ty->isCharType()                                   ||
          Ty->isSpecificBuiltinType(BuiltinType::Int)        ||
          Ty->isSpecificBuiltinType(BuiltinType::Void)       ||
          Ty->isSpecificBuiltinType(BuiltinType::Long)       ||
          Ty->isSpecificBuiltinType(BuiltinType::LongLong)   ||
          Ty->isSpecificBuiltinType(BuiltinType::UInt)       ||
          Ty->isSpecificBuiltinType(BuiltinType::ULong)      ||
          Ty->isSpecificBuiltinType(BuiltinType::ULongLong)  ||
          Ty->isSpecificBuiltinType(BuiltinType::Double)     ||
          Ty->isSpecificBuiltinType(BuiltinType::LongDouble) ||
          Ty->isSpecificBuiltinType(BuiltinType::Float));
}

