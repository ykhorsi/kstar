#!/bin/sh

set -e

if [ ! -f third_party/clang/README.txt ]; then
	git submodule update --init
fi
if [ ! -f third_party/clang/README.txt ]; then
	echo "Error while getting submodules."
	echo "Aborting"
	exit 1
fi

autoreconf -fvi
