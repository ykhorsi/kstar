#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static void
matmul_depend(int N, int BS, float A[N][N], float B[N][N], float C[N][N])
{
  int i, j, k, ii, jj, kk;

  for (i = 0; i < N; i += BS) {
    for (j = 0; j < N; j += BS) {
      for (k = 0; k < N; k += BS) {
#pragma omp parallel
#pragma omp single
        {
#pragma omp task depend(in: A[i:BS][k:BS], B[k:BS][j:BS]) \
                 depend(inout: C[i:BS][j:BS])
          for (ii = i; ii < i+BS; ii++)
            for (jj = j; jj < j+BS; jj++)
              for (kk = k; kk < k+BS; kk++)
                C[ii][jj] = C[ii][jj] + A[ii][kk] * B[kk][jj];
        }
      }
    }
  }
}

int main()
{
  int N  = 100;
  int BS = 20;
  float A[N][N], B[N][N], C[N][N];
  int i, j;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      A[i][j] = B[i][j] = 2;
      C[i][j] = 0;
    }
  }

  matmul_depend(N, BS, A, B, C);

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      if (C[i][j] != 400)
        abort();
    }
  }
  return 0;
}

