#include "RuntimePragma.h"
#include "Printer.h"

using namespace clang;

RuntimeTaskgroup::RuntimeTaskgroup(OMPTaskgroupDirective *OMPD, ASTContext &C,
                                   DataSharingInfo &DSI)
    : RuntimeScopeDirective(OMPD, C, DSI) {}

/// \brief Replace TASKGROUP construct
///
/// #pragma omp taskgroup
///     block;
///
/// is replaced by:
///
/// task_group_begin();
/// block;
/// task_group_end();
std::string RuntimeTaskgroup::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << getRuntime()->EmitBeginTaskgroup() << ";";
  KStarStmtPrinter P(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective(),
                     true, dsi.isInner(getOMPDirective()));
  P.PrintStmt(captured_->getCapturedStmt());
  rso << getRuntime()->EmitEndTaskgroup() << ";";

  return rso.str();
}
