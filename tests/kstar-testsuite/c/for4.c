#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    do  {
      randomvalue = rand()%10000;
    } while (randomvalue == 0);

    int a = randomvalue;
    int b = 2;
    #pragma omp parallel 
    #pragma omp for lastprivate(a) 
      for (int i=0; i<randomvalue*b; ++i)
      {
        a =  i; 
      }
    assert( a == (randomvalue*b - 1) );
    return 0;
}
