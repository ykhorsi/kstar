#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    do  {
      randomvalue = rand()%10000;
    } while (randomvalue == 0);

    int a = randomvalue;
    #pragma omp parallel shared(a) num_threads(1)
    {
      assert(a == randomvalue);
      a = 12;
    }
    assert( a == 12 );
    return 0;
}
