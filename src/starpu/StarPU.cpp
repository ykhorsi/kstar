#include "StarPU.h"
#include "StarPUPragma.h"

#include "GenUtils.h"
#include "Printer.h"
#include "MultiDimArray.h"

using namespace clang;

RuntimeDirective *StarPU::createDirective(OMPExecutableDirective *S,
                                          ASTContext &C,
                                          DataSharingInfo &DSI) {
#define GENERATE(PRAGMA)                                                       \
  if (isa<OMP##PRAGMA##Directive>(S)) {                                        \
    return new StarPU##PRAGMA(cast<OMP##PRAGMA##Directive>(S), C, DSI);        \
  }
#include <pragma.def>
  assert(!"Unsupported OpenMP executable directive!");
  return nullptr;
}

std::string StarPU::EmitBeginCritical(std::string const &Name,
                                      bool IsCPlusPlus11) {
  std::string Arg;
  if (!Name.empty()) {
    Arg = "\"" + Name + "\"";
  } else {
    Arg = IsCPlusPlus11 ? "nullptr" : "NULL";
  }
  return "starpu_omp_critical_inline_begin(" + Arg + ");";
}

std::string StarPU::EmitEndCritical(std::string const &Name,
                                    bool IsCPlusPlus11) {
  std::string Arg;
  if (!Name.empty()) {
    Arg = "\"" + Name + "\"";
  } else {
    Arg = IsCPlusPlus11 ? "nullptr" : "NULL";
  }
  return "starpu_omp_critical_inline_end(" + Arg + ");";
}

std::string StarPU::EmitBarrier() {
  return "starpu_omp_barrier();";
}

std::string StarPU::EmitTaskwait() {
  return "starpu_omp_taskwait();";
}

std::string StarPU::EmitSingleCond() {
  return "starpu_omp_single_inline()";
}

std::string StarPU::EmitBeginCopyprivate(const std::string &Data) {
  return "starpu_omp_single_copyprivate_inline_begin(" + Data + ")";
}

std::string StarPU::EmitEndCopyprivate(const std::string &Data) {
  return "";
}

std::string StarPU::EmitCopyprivateBarrier() {
  return "starpu_omp_single_copyprivate_inline_end()";
}

std::string StarPU::EmitMasterCond() {
  return "starpu_omp_master_inline()";
}

std::string StarPU::EmitBeginOrdered() {
  return "starpu_omp_ordered_inline_begin()";
}

std::string StarPU::EmitEndOrdered() {
  return "starpu_omp_ordered_inline_end()";
}

std::string StarPU::EmitBeginTaskgroup() {
  return "starpu_omp_taskgroup_inline_begin()";
}

std::string StarPU::EmitEndTaskgroup() {
  return "starpu_omp_taskgroup_inline_end()";
}

std::string StarPU::EmitFlush() {
  return "__sync_synchronize();";
}

std::string StarPU::EmitScheduleType(OpenMPScheduleClauseKind &Kind) {
  switch (Kind) {
  case OMPC_SCHEDULE_static:
    return "starpu_omp_sched_static";
  case OMPC_SCHEDULE_adaptive:
  case OMPC_SCHEDULE_auto:
    return "starpu_omp_sched_auto";
  case OMPC_SCHEDULE_dynamic:
    return "starpu_omp_sched_dynamic";
  case OMPC_SCHEDULE_guided:
    return "starpu_omp_sched_guided";
  case OMPC_SCHEDULE_runtime:
    return "starpu_omp_sched_static";
  case OMPC_SCHEDULE_unknown:
    assert(!"Unknown OpenMP schedule kind!");
  }
  return "starpu_omp_sched_auto";
}

std::string StarPU::EmitDependType(OpenMPDependClauseKind &Kind,
                                   bool IsCPlusPlus) {
  std::string Mode = "STARPU_NONE";
  switch (Kind) {
  case OMPC_DEPEND_in:
    Mode = "STARPU_R";
    break;
  case OMPC_DEPEND_out:
    Mode = "STARPU_W";
    break;
  case OMPC_DEPEND_inout:
    Mode = "STARPU_RW";
    break;
  case OMPC_DEPEND_cw:
    Mode = "STARPU_REDUX";
    break;
  case OMPC_DEPEND_commute:
    Mode = "STARPU_RW | STARPU_COMMUTE_IF_SUPPORTED";
    break;
  case OMPC_DEPEND_sink:
  case OMPC_DEPEND_source:
    assert(!"Unsupported OpenMP depend kind!");
    break;
  case OMPC_DEPEND_unknown:
    assert(!"Unknown OpenMP depend kind!");
  }
  if (IsCPlusPlus)
    Mode = "starpu_data_access_mode(" + Mode + ")";
  return Mode;
}

std::string StarPU::EmitMapType(OpenMPMapClauseKind &Kind) {
  switch (Kind) {
  case OMPC_MAP_alloc:
    return "STARPU_SCRATCH";
  case OMPC_MAP_to:
    return "STARPU_R";
  case OMPC_MAP_from:
    return "STARPU_W";
  case OMPC_MAP_tofrom:
    return "STARPU_RW";
  case OMPC_MAP_delete:
  case OMPC_MAP_release:
  case OMPC_MAP_always:
    assert(!"Unsupported OpenMP map kind!");
  case OMPC_MAP_unknown:
    assert(!"Unknown OpenMP map kind!");
  }
  return "STARPU_NONE";
}

std::string StarPU::EmitProcBind(OpenMPProcBindClauseKind &Kind) {
  return "";
}

std::string StarPU::EmitAlloc(std::string const &Size) {
  return "kstar_starpu_malloc(" + Size + ")";
}

std::string StarPU::EmitFree(std::string const &Name) {
  return "starpu_free(" + Name + ")";
}

std::string StarPU::EmitForInit(const std::string &Begin,
                                const std::string &End,
                                const std::string &Chunk,
                                const std::string &Pargrain,
                                const std::string &Schedule,
                                const std::string &Ordered) {
  return "if (starpu_omp_for_inline_first_alt(" + End + ", " + Chunk + ", " +
         Schedule + ", " + Ordered + ", &" + Begin + ", &" + End + "))";
}

std::string StarPU::EmitForNext(const std::string &Len,
                                const std::string &Chunk,
                                const std::string &Schedule,
                                const std::string &Ordered,
                                const std::string &Begin,
                                const std::string &End) {
  return "starpu_omp_for_inline_next_alt(" + Len + ", " + Chunk + ", " +
         Schedule + ", " + Ordered + ", &" + Begin + ", &" + End + ")";
}

std::string StarPU::EmitForEnd(const std::string &Nowait) {
  return "if (!" + Nowait + ") " + EmitBarrier();
}

std::string StarPU::EmitParallelRegion(const std::string &Name,
                                       const std::string &NumThreads,
                                       OpenMPProcBindClauseKind &,
                                       const std::string &Func,
                                       const std::string &Arg) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  CodeGen::StructInit S("starpu_omp_parallel_region_attr", Name);
  S.add("cl.cpu_funcs[0]", Func);
  S.add("if_clause", "1");
  S.add("cl_arg", Arg);
  S.add("num_threads", NumThreads);

  rso << S.str()
      << "starpu_omp_parallel_region(" << S.getaddr() << ");";

  return rso.str();
}

QualType StarPU::getScheduleType(ASTContext &Context) {
  return Context.IntTy;
}

/*******************/
std::string StarPU::genTaskSignature(ASTContext &Context,
                                          std::string funcName,
                                          std::string templateArgs) {
  FunctionDecl *FD = CreateWrapperFunction(Context, funcName);
  std::string str;
  llvm::raw_string_ostream rso(str);

  if (!templateArgs.empty())
    rso << "template " << templateArgs;
  FD->print(rso);

  return rso.str();
}

// TODO: Rewrite this method.
std::string StarPU::genRegister(Expr const *b, VarDecl const *VD,
                                     DataSharingInfo &dsi,
                                     OMPExecutableDirective *S,
                                     const int &bufInd, ASTContext &context_,
                                     bool alloc) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string dummy1;
  std::queue<std::string> dummyq;
  llvm::raw_string_ostream streamTrue(dummy1);
  KStarStmtPrinter printerTrue(streamTrue, context_.getPrintingPolicy(), dummyq,
                               &dsi, S, true);

  if (isa<ArraySubscriptExpr>(b) || isa<OMPArraySectionExpr>(b)) {
    MultiDimArray MDA(b);

    streamTrue.str().clear();
    printerTrue.PrintExpr(const_cast<DeclRefExpr *>(MDA.getBase()));
    std::string base_str = streamTrue.str();

    std::string len1_str = "1";
    if (MDA.getLen(0)) {
      streamTrue.str().clear();
      printerTrue.PrintExpr(const_cast<Expr *>(MDA.getLen(0)));
      len1_str = streamTrue.str();
    }

    std::string access_value = "";
    for (int i = 0; i < MDA.getDim(); i++) {
      if (MDA.getLowerBound(i) != nullptr) {
        streamTrue.str().clear();
        printerTrue.PrintExpr(const_cast<Expr *>(MDA.getLowerBound(i)));
        access_value += "[" + streamTrue.str() + "]";
      } else {
        access_value += "[0]";
      }
    }
    base_str += access_value;

    if (!alloc) {
      /* only attempt to reuse a handle if no fresh allocation has been
       * requested */
      rso << "if(starpu_data_lookup(&" << base_str << "))\n";
      rso << "\thandles[" << bufInd << "] = "
          << "starpu_data_lookup(&" << base_str << ");\n";
      rso << "else\n";
    }
    if (MDA.getDim() == 1) {
      rso << "{\n\tstarpu_vector_data_register(&handles[" << bufInd << "], ";
      if (alloc)
        rso << "-1, 0";
      else
        rso << "0, (uintptr_t)&" << base_str;
      rso << ", " << len1_str << ", " << CodeGen::GenSizeof(base_str) << ");\n";

      // Assign current handles to the global arbiter instantiated by StarPU/OMP.
      // This allows to improve performance a lot when tasks are commuted.
      rso << "starpu_data_assign_arbiter(handles["
          << bufInd
          << "], starpu_omp_get_default_arbiter());\n";

      if (MDA.getLowerBound(0) != nullptr) {
        // Get access value for 1D arrays.
        streamTrue.str().clear();
        printerTrue.PrintExpr(const_cast<Expr *>(MDA.getLowerBound(0)));

        rso << "starpu_omp_vector_annotate(handles[" << bufInd << "] , "
          << streamTrue.str() << ");\n}\n";
      } else {
        rso << "starpu_omp_vector_annotate(handles[" << bufInd << "] , "
          << "0" << ");\n}\n";
      }
    } else {
      rso << "\tstarpu_matrix_data_register(&handles[" << bufInd
          << "], 0, (uintptr_t)&" << base_str << ", ";
      std::string ldas;
      if (MDA.getLDA()) {
        streamTrue.str().clear();
        printerTrue.PrintExpr(const_cast<Expr *>(MDA.getLDA()));
        ldas = streamTrue.str();
      } else {
        ldas = std::to_string(MDA.getConstantLDA().getLimitedValue());
      }
      std::string len2_str = "1";
      if (MDA.getLen(1)) {
        streamTrue.str().clear();
        printerTrue.PrintExpr(const_cast<Expr *>(MDA.getLen(1)));
        len2_str = streamTrue.str();
      }
      rso << ldas << ", " << len1_str << ", " << len2_str << ", "
          << CodeGen::GenSizeof(base_str) << ");\n";
    }
  } else {
    QualType Ty = VD->getType();
    std::string VarStr;

    if (!VD->getType()->isPointerType())
        VarStr = "&";
    VarStr += VD->getNameAsString();

    rso << "if(starpu_data_lookup(" << VarStr << "))\n";
    rso << "\thandles[" << bufInd << "] = "
        << "starpu_data_lookup(" << VarStr << ");\n";
    rso << "else\n";
    rso << "\tstarpu_variable_data_register(&handles[" << bufInd
        << "], 0, (uintptr_t)" << VarStr << ", "
        << CodeGen::GenSizeof(Ty.getAsString()) << ");\n";
  }
  return rso.str();
}

FunctionDecl *
StarPU::CreateWrapperFunction(ASTContext &Context,
                                   const std::string &Name) {
  // Create function declaration.
  QualType PtrVoidTy = Context.getPointerType(Context.VoidTy);
  QualType PtrVoidPtrTy = Context.getPointerType(Context.VoidPtrTy);

  SmallVector<QualType, 2> FnArgTypes;
  FnArgTypes.push_back(PtrVoidPtrTy);
  FnArgTypes.push_back(PtrVoidTy);

  QualType FnTy =
    Context.getFunctionType(Context.VoidTy, FnArgTypes,
                                 FunctionProtoType::ExtProtoInfo());
  TypeSourceInfo *FnTI =
    Context.getTrivialTypeSourceInfo(FnTy, SourceLocation());

  FunctionDecl *FD =
    FunctionDecl::Create(Context, Context.getTranslationUnitDecl(),
                         SourceLocation(), SourceLocation(),
                         &Context.Idents.get(Name), FnTy, FnTI,
                         SC_Static, false, true, false);

  // Create function argument.
  TypeSourceInfo *PtrVoidTI =
    Context.getTrivialTypeSourceInfo(PtrVoidTy, SourceLocation());
  TypeSourceInfo *PtrVoidPtrTI =
    Context.getTrivialTypeSourceInfo(PtrVoidPtrTy, SourceLocation());

  ParmVarDecl *Arg1 =
    ParmVarDecl::Create(Context, FD, SourceLocation(), SourceLocation(),
                        &Context.Idents.get("kstar_buffers"),
                        PtrVoidPtrTy, PtrVoidPtrTI, SC_None, 0);

  ParmVarDecl *Arg2 =
    ParmVarDecl::Create(Context, FD, SourceLocation(), SourceLocation(),
                        &Context.Idents.get("kstar_params"),
                        PtrVoidTy, PtrVoidTI, SC_None, 0);

  // Set function arguments.
  ParmVarDecl *Params[] = {Arg1, Arg2};
  FD->setParams(Params);

  return FD;
}
