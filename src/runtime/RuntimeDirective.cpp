#include "RuntimeDirective.h"
#include "OpenMPRuntimeFactory.h"

using namespace clang;

int RuntimeDirective::numDirective = 0;

RuntimeDirective::RuntimeDirective(OMPExecutableDirective *S, ASTContext &C)
    : OMPDirective(S), Context(C), pragmaId_(numDirective++) {}
