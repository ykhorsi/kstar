#include <clang/AST/ExprOpenMP.h>

#include "MultiDimArray.h"

using namespace clang;

MultiDimArray::MultiDimArray(Expr const *array) {
  Expr const *curExpr = array;
  dimension_ = 0;

  if (isa<ArraySubscriptExpr>(curExpr)) {
    /*Explore from right to left*/
    /*FIXME comment a bit...*/
    while (isa<ArraySubscriptExpr>(curExpr)) {
      ArraySubscriptExpr const *curArray = cast<ArraySubscriptExpr>(curExpr);
      Expr const *idx = curArray->getIdx()->IgnoreImplicit();
      if (isa<OMPArraySectionExpr>(idx)) {
        OMPArraySectionExpr const *cean = cast<OMPArraySectionExpr>(idx);
        lowerBounds_.push_front(cean->getLowerBound()->IgnoreImplicit());
        lengths_.push_front(cean->getLength()->IgnoreImplicit());
        // We get the "left" lda as we support only 2 dim arrays at most.

        // XXX: The patch which adds leading dimension has not been rebased
        // on top of Clang master. If someone need it, please do!
        //lda_ = cean->getLeadingDim()->IgnoreImplicit();
        assert(!"Leading dimension still not supported!");
      } else {
        lowerBounds_.push_front(idx);
        lengths_.push_front(nullptr);
        lda_ = nullptr;
      }
      curExpr = curArray->getBase()->IgnoreImplicit();
      dimension_++;
    }
  } else {
    while (isa<OMPArraySectionExpr>(curExpr)) {
      const OMPArraySectionExpr *ArrayExpr = cast<OMPArraySectionExpr>(curExpr);
      curExpr = ArrayExpr->getBase()->IgnoreImplicit();

      // XXX: add support for multi dimensions.
      dimension_++;
      if (ArrayExpr->getLowerBound() == nullptr) {
        lowerBounds_.push_front(nullptr);
      } else {
        lowerBounds_.push_front(ArrayExpr->getLowerBound()->IgnoreImplicit());
      }
      lengths_.push_front(ArrayExpr->getLength()->IgnoreImplicit());
    }
  }

  assert(isa<DeclRefExpr>(curExpr) && "Base is not a DeclRefExpr");
  assert((dimension_ == 1 || dimension_ == 2) && "Unsupported array size");
  base_ = cast<DeclRefExpr>(curExpr);
  if (dimension_ > 1) {
    Type const *baseType = base_->getType().getTypePtrOrNull();
    while (baseType->isPointerType() || isa<ParenType>(baseType)) {
      if (isa<ParenType>(baseType))
        baseType = &*cast<ParenType>(baseType)->getInnerType();
      else
        baseType = baseType->getPointeeType().getTypePtrOrNull();
    }
    if(lda_)
        return;

    assert(baseType->isArrayType() && "Base is not an array Type");

    if (isa<VariableArrayType>(baseType)) {
      VariableArrayType const *vat = cast<VariableArrayType>(baseType);
      lda_ = vat->getSizeExpr();
    } else if (isa<DependentSizedArrayType>(baseType)) {
      DependentSizedArrayType const *dsat =
          cast<DependentSizedArrayType>(baseType);
      lda_ = dsat->getSizeExpr();
    } else if (isa<ConstantArrayType>(baseType)) {
      ConstantArrayType const *cat = cast<ConstantArrayType>(baseType);
      constantLda_ = cat->getSize();
    } else {
      baseType->dump();
      assert(false && "Base has unsupported ArrayType");
    }
  }
}

int MultiDimArray::getDim() { return dimension_; }

DeclRefExpr const *MultiDimArray::getBase() { return base_; }

Expr const *MultiDimArray::getLen(int dim) {
  assert(dim < dimension_ && dim >= 0 && "Wrong dimension");
  return lengths_[dim];
}

Expr const *MultiDimArray::getLowerBound(int dim) {
  assert(dim < dimension_ && dim >= 0 && "Wrong dimension");
  return lowerBounds_[dim];
}

Expr const *MultiDimArray::getLDA() {
  return lda_;
  /*
   *assert(dim < dimension_ && dim >= 0 && "Wrong dimension");
   *return rowSizes_[dim];
   */
}

llvm::APInt &MultiDimArray::getConstantLDA() { return constantLda_; }
