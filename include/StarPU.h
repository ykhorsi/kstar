//===----------------------------------------------------------------------===//
//
// This provides a class for OpenMP code generation with StarPU runtime.
//
//===----------------------------------------------------------------------===//

#ifndef KSTAR_STARPU_H
#define KSTAR_STARPU_H

#include "OpenMPRuntime.h"

class StarPU : public OpenMPRuntime {
public:
  StarPU() : OpenMPRuntime() {}

  RuntimeDirective *createDirective(clang::OMPExecutableDirective *S,
                                    clang::ASTContext &C,
                                    DataSharingInfo &DSI) override;

  std::string EmitBeginCritical(const std::string &Name,
                                bool IsCPlusPlus11) override;
  std::string EmitEndCritical(const std::string &Name,
                              bool IsCPlusPlus11) override;
  std::string EmitBarrier() override;
  std::string EmitTaskwait() override;
  std::string EmitSingleCond() override;
  std::string EmitBeginCopyprivate(const std::string &Data) override;
  std::string EmitEndCopyprivate(const std::string &Data) override;
  std::string EmitCopyprivateBarrier() override;
  std::string EmitMasterCond() override;
  std::string EmitBeginOrdered() override;
  std::string EmitEndOrdered() override;
  std::string EmitBeginTaskgroup() override;
  std::string EmitEndTaskgroup() override;
  std::string EmitFlush() override;
  std::string EmitScheduleType(clang::OpenMPScheduleClauseKind &K) override;
  std::string EmitDependType(clang::OpenMPDependClauseKind &K,
                             bool IsCPlusPlus) override;
  std::string EmitMapType(clang::OpenMPMapClauseKind &K) override;
  std::string EmitProcBind(clang::OpenMPProcBindClauseKind &K) override;
  std::string EmitAlloc(std::string const &Size) override;
  std::string EmitFree(std::string const &Name) override;
  std::string EmitForInit(const std::string &Begin, const std::string &End,
                          const std::string &Chunk, const std::string &Pargrain,
                          const std::string &Schedule,
                          const std::string &Ordered) override;
  std::string EmitForNext(const std::string &Len, const std::string &Chunk,
                          const std::string &Schedule,
                          const std::string &Ordered, const std::string &Begin,
                          const std::string &End) override;
  std::string EmitForEnd(const std::string &Nowait) override;
  std::string EmitParallelRegion(const std::string &Name,
                                 const std::string &NumThreads,
                                 clang::OpenMPProcBindClauseKind &,
                                 const std::string &Func,
                                 const std::string &Arg) override;

  clang::QualType getScheduleType(clang::ASTContext &C) override;

  bool isKaapi() override { return false; }
  std::string genRegister(clang::Expr const *b, clang::VarDecl const *,
                          DataSharingInfo &, clang::OMPExecutableDirective *,
                          const int &bufInd, clang::ASTContext &,
                          bool alloc = false) override;
  std::string genTaskSignature(clang::ASTContext &Context,
                               std::string funcName,
                               std::string templateArgs) override;
  clang::FunctionDecl *
    CreateWrapperFunction(clang::ASTContext &Context,
                          const std::string &Name) override;
};

#endif
