#include <starpu.h>

static __global__ void
vector_mult_cuda(unsigned n, int *vec, int factor)
{
  unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n)
    vec[i] *= factor;
}

extern "C" void
gpu_scal_func(unsigned n, int *vec, int factor)
{
  int threads_per_block = 256;
  int blocks_per_grid = (n + threads_per_block - 1) / threads_per_block;
  vector_mult_cuda<<<blocks_per_grid, threads_per_block>>>(n, vec, factor);
}

